/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LCD_D5_Pin GPIO_PIN_2
#define LCD_D5_GPIO_Port GPIOE
#define LCD_D4_Pin GPIO_PIN_3
#define LCD_D4_GPIO_Port GPIOE
#define LCD_RS_Pin GPIO_PIN_4
#define LCD_RS_GPIO_Port GPIOE
#define LCD_RW_Pin GPIO_PIN_5
#define LCD_RW_GPIO_Port GPIOE
#define LCD_EN_Pin GPIO_PIN_6
#define LCD_EN_GPIO_Port GPIOE
#define PC14_OSC32_IN_Pin GPIO_PIN_14
#define PC14_OSC32_IN_GPIO_Port GPIOC
#define PC15_OSC32_OUT_Pin GPIO_PIN_15
#define PC15_OSC32_OUT_GPIO_Port GPIOC
#define osc_in_Pin GPIO_PIN_0
#define osc_in_GPIO_Port GPIOH
#define osc_out_Pin GPIO_PIN_1
#define osc_out_GPIO_Port GPIOH
#define Vbat_Pin GPIO_PIN_0
#define Vbat_GPIO_Port GPIOC
#define FT232_RX_Pin GPIO_PIN_0
#define FT232_RX_GPIO_Port GPIOA
#define FT232_TX_Pin GPIO_PIN_1
#define FT232_TX_GPIO_Port GPIOA
#define HC05_RX_Pin GPIO_PIN_2
#define HC05_RX_GPIO_Port GPIOA
#define HC05_TX_Pin GPIO_PIN_3
#define HC05_TX_GPIO_Port GPIOA
#define IR_Pin GPIO_PIN_5
#define IR_GPIO_Port GPIOC
#define transoptor_Pin GPIO_PIN_0
#define transoptor_GPIO_Port GPIOB
#define pompka_Pin GPIO_PIN_1
#define pompka_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define serwo4_Pin GPIO_PIN_11
#define serwo4_GPIO_Port GPIOE
#define LCD_D6_Pin GPIO_PIN_14
#define LCD_D6_GPIO_Port GPIOE
#define LCD_D7_Pin GPIO_PIN_15
#define LCD_D7_GPIO_Port GPIOE
#define serwo7_Pin GPIO_PIN_10
#define serwo7_GPIO_Port GPIOB
#define serwo6_Pin GPIO_PIN_11
#define serwo6_GPIO_Port GPIOB
#define serwo5_Pin GPIO_PIN_14
#define serwo5_GPIO_Port GPIOB
#define buzzer_Pin GPIO_PIN_15
#define buzzer_GPIO_Port GPIOB
#define led_zielony_Pin GPIO_PIN_12
#define led_zielony_GPIO_Port GPIOD
#define led_pom_Pin GPIO_PIN_13
#define led_pom_GPIO_Port GPIOD
#define led_czerwony_Pin GPIO_PIN_14
#define led_czerwony_GPIO_Port GPIOD
#define led_niebieski_Pin GPIO_PIN_15
#define led_niebieski_GPIO_Port GPIOD
#define SERWO1_Pin GPIO_PIN_6
#define SERWO1_GPIO_Port GPIOC
#define SERWO2_Pin GPIO_PIN_7
#define SERWO2_GPIO_Port GPIOC
#define SERWO3_Pin GPIO_PIN_8
#define SERWO3_GPIO_Port GPIOC
#define KROKOWY_PWM1_Pin GPIO_PIN_9
#define KROKOWY_PWM1_GPIO_Port GPIOC
#define KROKOWY_PWM2_Pin GPIO_PIN_8
#define KROKOWY_PWM2_GPIO_Port GPIOA
#define KROKOWY1_Pin GPIO_PIN_9
#define KROKOWY1_GPIO_Port GPIOA
#define KROKOWY2_Pin GPIO_PIN_10
#define KROKOWY2_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define KROKOWY3_Pin GPIO_PIN_10
#define KROKOWY3_GPIO_Port GPIOC
#define KROKOWY4_Pin GPIO_PIN_11
#define KROKOWY4_GPIO_Port GPIOC
#define Audio_RST_Pin GPIO_PIN_4
#define Audio_RST_GPIO_Port GPIOD
#define OTG_FS_OverCurrent_Pin GPIO_PIN_5
#define OTG_FS_OverCurrent_GPIO_Port GPIOD
#define MOTOR2A_Pin GPIO_PIN_6
#define MOTOR2A_GPIO_Port GPIOD
#define MOTOR1A_Pin GPIO_PIN_7
#define MOTOR1A_GPIO_Port GPIOD
#define MOTOR1B_Pin GPIO_PIN_5
#define MOTOR1B_GPIO_Port GPIOB
#define MOTOR2B_Pin GPIO_PIN_7
#define MOTOR2B_GPIO_Port GPIOB
#define MOTOR1_PWM_Pin GPIO_PIN_8
#define MOTOR1_PWM_GPIO_Port GPIOB
#define MOTOR2_PWM_Pin GPIO_PIN_9
#define MOTOR2_PWM_GPIO_Port GPIOB
#define MEMS_INT2_Pin GPIO_PIN_1
#define MEMS_INT2_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
