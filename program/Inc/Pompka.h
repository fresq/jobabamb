#pragma once


#define POMPKA_TIM TIM3
#define POMPKA_CCR CCR4
#define POMPKA_MAX_VALUE 20000


void PompkaWypelnienieP(float p)
{
	POMPKA_TIM->POMPKA_CCR = (int)(p * POMPKA_MAX_VALUE);
}

void PompkaWypelnienie(uint16_t w)
{
	POMPKA_TIM->POMPKA_CCR = w;
}

void PompkaWypelnienieMod(uint16_t w)
{
	POMPKA_TIM->POMPKA_CCR += w;
}
