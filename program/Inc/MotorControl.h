/**
  * @file MotorControl.h
  * @brief   Plik zawiera funkcje obslugi motorow
  */

#pragma once

#include "stm32f4xx_hal.h"

#define MOTOR_MAX 10000

//#define MOTOR1_GPIO GPIOD
//#define MOTOR2_GPIO GPIOB
//#define MOTOR3_GPIO GPIOD
//#define MOTOR4_GPIO GPIOB
//#define MOTOR_PWM_GPIO GPIOB
//
//#define MOTOR1_PIN GPIO_Pin_7
//#define MOTOR2_PIN GPIO_Pin_5
//#define MOTOR3_PIN GPIO_Pin_6
//#define MOTOR4_PIN GPIO_Pin_7
//#define MOTOR1_PWM_PIN GPIO_Pin_8
//#define MOTOR2_PWM_PIN GPIO_Pin_9
//
#define M1SET HAL_GPIO_WritePin(MOTOR1A_GPIO_Port, MOTOR1A_Pin, 1);
#define M1CLR HAL_GPIO_WritePin(MOTOR1A_GPIO_Port, MOTOR1A_Pin, 0);
#define M2SET HAL_GPIO_WritePin(MOTOR1B_GPIO_Port, MOTOR1B_Pin, 1);
#define M2CLR HAL_GPIO_WritePin(MOTOR1B_GPIO_Port, MOTOR1B_Pin, 0);
#define M3SET HAL_GPIO_WritePin(MOTOR2A_GPIO_Port, MOTOR2A_Pin, 1);
#define M3CLR HAL_GPIO_WritePin(MOTOR2A_GPIO_Port, MOTOR2A_Pin, 0);
#define M4SET HAL_GPIO_WritePin(MOTOR2B_GPIO_Port, MOTOR2B_Pin, 1);
#define M4CLR HAL_GPIO_WritePin(MOTOR2B_GPIO_Port, MOTOR2B_Pin, 0);



int lastLewy = 0, lastPrawy = 0;

/**
  * @brief   Ustawia kierunek i predkosc jazdy
  */
void Jazda(int lewy, int prawy)
{
	if(lewy >= 0)
	{
		if(lewy>MOTOR_MAX)
		lewy = MOTOR_MAX;
		M1SET;
		M2CLR;
	}
	else
	{
		if(lewy<-MOTOR_MAX)
		lewy = -MOTOR_MAX;
		M1CLR;
		M2SET;
	}

	if(prawy >= 0)
	{
		if(prawy>MOTOR_MAX)
		prawy = MOTOR_MAX;
		M3SET;
		M4CLR;
	}
	else
	{
		if(prawy<-MOTOR_MAX)
		prawy = -MOTOR_MAX;
		M3CLR;
		M4SET;
	}

	TIM4->CCR3 = abs(lewy);
	TIM4->CCR4 = abs(prawy);
}

int ileKrokow = 4;

/**
  * @brief   Ustawia kierunek i predkosc jazdy procentowo bez zrywow
  */
void JazdaP(float lewy, float prawy)
{
	//LCDOutString("jazda:  ");
	//LCDOutUFloat(lewy,1,5);
	//LCDOutUFloat(prawy,1,5);
	int deltaLewy =  (MOTOR_MAX * lewy - lastLewy) / ileKrokow;
	int deltaPrawy =  (MOTOR_MAX * prawy - lastPrawy) / ileKrokow;
	for(int i = 0; i < ileKrokow; i++)
	{
		lastLewy += deltaLewy;
		lastPrawy += deltaPrawy;
		Jazda(lastLewy, lastPrawy);
		HAL_Delay(20);
	}
	lastLewy = MOTOR_MAX * lewy;
	lastPrawy = MOTOR_MAX * prawy;

}

/**
  * @brief   Petla umozliwiajaca jazde sterowana z pilota
//  */
//void MenuJazda()
//{
//	int cmd;
//	LCDClear();
//	//int ile = 0;
//	while(1)
//	{
//		while((cmd = detect()) < 0);
//		if(cmd > 0)
//		{
//			cmd &= ~2048;
//			//LCDOutint16(cmd,5);
//			if(cmd == KEY_ESC)
//			{
//				Jazda(0,0);
//				return;
//			}
//			if(cmd == KEY_LEFT)
//			{
//				JazdaP(0.9f,-0.1f);
//			}
//			if(cmd == KEY_RIGHT)
//			{
//				JazdaP(-0.1f,0.9f);
//			}
//			if(cmd == KEY_UP)
//			{
//				JazdaP(0.8f,0.8f);
//			}
//			if(cmd == KEY_DOWN)
//			{
//				JazdaP(-0.8f,-0.8f);
//			}
//			if(cmd == KEY_1)
//			{
//				JazdaP(-0.8f,0);
//			}
//			if(cmd == KEY_2)
//			{
//				JazdaP(0,-0.8f);
//			}
//			if(cmd == KEY_3)
//			{
//				JazdaP(0.8f,0);
//			}
//			if(cmd == KEY_4)
//			{
//				JazdaP(0,0.8f);
//			}
//		}
//		//_delay_ms(100);
//	}
//}
