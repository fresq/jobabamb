#pragma once

/**
  * @file RC5.h
  * @brief   Plik zawierajacy konfiguracje i obsluge rc5
  */
#define KEY_ESC 12
#define KEY_LEFT 17
#define KEY_RIGHT 16
#define KEY_DOWN 33
#define KEY_UP 32
#define KEY_OK 59
#define KEY_1 1
#define KEY_2 2
#define KEY_3 3
#define KEY_4 4
#define KEY_5 5
#define KEY_6 6
#define KEY_7 7
#define KEY_8 8
#define KEY_9 9


volatile uint16_t timerL = 0, timerH = 0;
#define RC5_IN HAL_GPIO_ReadPin(IR_GPIO_Port, IR_Pin)


/**
  * @brief   funkcja odczytuje znak przychodzacy z rc5 ewentualnie zwraca blad jesli go nie bylo
  * funkcja nie blokujaca.
  */
uint detect()
{
   uint8_t temp;
   uint8_t ref1;
   uint8_t ref2;
   uint8_t bitcnt;
   uint command;

   timerH  = 0;
   timerL  = 0;

   // Czeka na okres ciszy na linii wej�cia uC trwaj�cy  3.5ms
   // Je�li nie wykryje takiego okresu ciszy w ci�gu 131ms,
   // to ko�czy dzia�anie funkcji z b��dem
   while( timerL<110)
   {
      if(timerH>=16)  return  command = -1;

      if(!RC5_IN) timerL = 0;
   }

   // Czeka na  pierwszy bit startowy.
   // Je�li nie wykryje bitu startowego w ci�gu 131ms,
   // to ko�czy dzia�anie funkcji z b��dem
   while(RC5_IN)
        if(timerH>=16)  return command = -2;


   // Pomiar czasu trwani niskiego poziom sygan�u
   // w pierwszym bicie startowym.
   // Je�li nie wykryje rosn�cego zbocza sygna�u w ci�gu
   // 1ms, to ko�czy dzia�anie funkcji z b��dem
   timerL = 0;
   while(!RC5_IN)
        if(timerL>34) return command = -3;

   //
   temp = timerL;
   timerL = 0;

   // ref1 - oblicza  3/4 czasu trwania bitu
   ref1 =temp+(temp>>1);

   // ref2 - oblicza 5/4 czasu trwania bitu
   ref2 =(temp<<1)+(temp>>1);


   // Oczekuje na zbocze opadaj�ce sygna�u w �rodku drugiego
   // bitu startowego.
   // Je�li nie wykryje zbocza w ci�gu 3/4 czasu trwania
   // bitu, to ko�czy dzia�anie funkcji z b��dem
   while(RC5_IN)
        if(timerL > ref1) return command = -4;

   // W momencie wykrycia zbocza sygna�u, synchronizuje
   // zmien� timerL dla pr�bkowania  bitu toggle
   timerL = 0;

   // Odczytuje dekoduje pozosta�e 12 bit�w polecenia rc5
   for(bitcnt=0, command = 0; bitcnt <12; bitcnt++)
   {
      // Czeka 3/4 czasu trwania bitu od momentu wykrycia
      // zbocza sygna�u w po�owie poprzedniego bitu
      while(timerL < ref1) {};

      // Pr�bkuje - odczytuje port we  uC
      if(!RC5_IN)
      {
         // Je�li odczytano 0, zapami�tuje w zmiennej
         // "command" bit o warto�ci 0
         command <<= 1 ;

         // Oczekuje na zbocze rosn�ce sygna�u w �rodku bitu.
         // Je�li nie wykryje zbocza w ci�gu 5/4 czasu trwania
         // bitu, to ko�czy dzia�anie funkcji z b��dem
         while(!RC5_IN)
            if(timerL > ref2) return command = -5;
      }
      else
      {
         // Je�li odczytano 1, zapami�tuje w zmiennej
         // "command" bit o warto�ci 1
         command = (command <<1 ) | 0x01;

         // Oczekuje na zbocze opadaj�ce sygna�u w �rodku bitu.
         // Je�li nie wykryje zbocza w ci�gu 5/4 czasu trwania
         // bitu, to ko�czy dzia�anie funkcji z b��dem
         while(RC5_IN)
            if(timerL > ref2) return command = -6;
      }

      // W momencie wykrycia zbocza sygna�u, synchronizuje
      // zmien� timerL dla pr�bkowania kolejnego bitu
      timerL = 0;
  }

  // Zwraca kod polecenia rc5
  // bity 0..5 numer przycisku
  // bity 6..10  kod systemu(urz�dzenia)
  // bit 11 toggle bit
  return command;
}


