#pragma once
#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "main.h"


volatile int8_t krok = 0;
volatile int16_t krok_poz = 180;
float t;
volatile int zerowa = 0;
#define MICRO_COUNT 50

//50 //int16_t values[MICRO_COUNT] = {0,320,640,960,1278,1595,1911,2225,2536,2845,3151,3453,3752,4047,4338,4625,4907,5183,5455,5721,5981,6234,6482,6723,6956,7183,7402,7614,7818,8014,8201,8380,8551,8713,8865,9009,9144,9269,9384,9490,9586,9672,9749,9815,9871,9917,9953,9979,9994,10000};
//int16_t values[MICRO_COUNT] = {0,1736,3420,4999,6427,7660,8660,9396,9848,10000}; //10
int16_t values[MICRO_COUNT] = { 0, 204, 408, 612, 816, 1020, 1224, 1428, 1632,
		1836, 2040, 2244, 2448, 2653, 2857, 3061, 3265, 3469, 3673, 3877, 4081,
		4285, 4489, 4693, 4897, 5102, 5306, 5510, 5714, 5918, 6122, 6326, 6530,
		6734, 6938, 7142, 7346, 7551, 7755, 7959, 8163, 8367, 8571, 8775, 8979,
		9183, 9387, 9591, 9795, 10000 };
//int16_t values[MICRO_COUNT] = {0,825,1645,2454,3246,4016,4759,5469,6142,6772,7357,7891,8371,8794,9157,9458,9694,9863,9965,10000};//20
volatile int mMicroKrok = 0;
volatile int mKrokPoz = 0;
volatile int mKrok = 0;

//Przerwanie pozycji zerowej

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == transoptor_Pin) {
		zerowa = 1;
	}
}

void PolKrok();

void MikroKrok8(int lewo) {
	if (lewo) {
		mMicroKrok++;
		mKrokPoz++;
		if (mMicroKrok == MICRO_COUNT) {
			mKrok++;
			mMicroKrok = 0;
			if (mKrok == 8) {
				mKrok = 0;
			}
		}
	} else {
		mMicroKrok--;
		mKrokPoz--;
		if (mMicroKrok == -1) {
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1;
			if (mKrok == -1) {
				mKrok = 7;
			}
		}
	}

	if (mKrok == 0) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
		TIM8->CCR4 = 9999;
		TIM1->CCR1 = values[mMicroKrok];
	} else if (mKrok == 1) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = 9999;
	} else if (mKrok == 2) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);

		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = 9999;
	} else if (mKrok == 3) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
		TIM8->CCR4 = 9999;
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	} else if (mKrok == 4) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = 9999;
		TIM1->CCR1 = values[mMicroKrok];
	} else if (mKrok == 5) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = 9999;
	} else if (mKrok == 6) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = 9999;
	} else if (mKrok == 7) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = 9999;
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	}
}

void MikroKrok2(int lewo) {
	if (lewo) {
		mMicroKrok++;
		mKrokPoz++;
		if (mMicroKrok == MICRO_COUNT) {
			mKrok++;
			mMicroKrok = 0;
			if (mKrok == 4) {
				mKrok = 0;
			}
		}
	} else {
		mMicroKrok--;
		mKrokPoz--;
		if (mMicroKrok == -1) {
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1;
			if (mKrok == -1) {
				mKrok = 3;
			}
		}
	}

	if (mKrok == 0) {

		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	} else if (mKrok == 1) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = values[mMicroKrok];
	} else if (mKrok == 2) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	} else if (mKrok == 3) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = values[mMicroKrok];
	}
}

void MikroKrokSC(int lewo) {
	if (lewo)
		krok_poz++;
	else
		krok_poz--;
	float t1 = sin(krok_poz * t);
	float t2 = cos(krok_poz * t);
	if (t1 > 0) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
	} else {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
	}
	t1 = (t1 > 0 ? t1 : -t1);
	t1 = t1 * 9999.0f;
	TIM8->CCR4 = (int) t1;

	if (t2 > 0) {
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else {
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
	}
	t2 = (t2 > 0 ? t2 : -t2);
	t2 = t2 * 9999.0f;
	TIM1->CCR1 = (int) (t2);
}

volatile int stepper_speed = 0;
volatile int stepper_dir = 0;

void SteperTimerHandler() {
	static volatile int licz = 0;
	licz++;
	if (stepper_speed != 0 && licz > stepper_speed) {
		licz = 0;
		MikroKrok2(stepper_dir);
	}
}

void Krocz(int krok, int dir) {
	for (int i = 0; i < krok; i++) {
		PolKrok(dir);
		//MikroKrokSC(dir);
		HAL_Delay(25);
		//for (volatile int j = 0; j < 1000000; j++)
			;
	}
}

void delay() {
	for (volatile uint32_t i = 100000; i > 0; i--)
		;
}

void PolKrok(int prawo) {
	if (prawo) {
		krok++;
		krok_poz++;
	} else {
		krok--;
		krok_poz--;
	}

	if (krok == -1)
		krok = 7;
	if (krok == 8)
		krok = 0;

	if (krok == 0) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else if (krok == 1) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else if (krok == 2) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else if (krok == 3) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else if (krok == 4) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
	} else if (krok == 5) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
	} else if (krok == 6) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
	} else if (krok == 7) {
		HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 1);
		HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
		HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 1);
	}
	delay();
}

void ZerujKrokowy() {
	zerowa = 0;
	while (!zerowa) {
		MikroKrok2(1);
		//for (volatile int i = 0; i < 10000; i++);
		HAL_Delay(10);
	}
	krok_poz = 180;
	for (int j = 0; j < 362; j++) {
		MikroKrok2(0);
		//for (volatile int i = 0; i < 10000; i++);
		HAL_Delay(10);
	}
}

void KrokowyStop() {
	HAL_GPIO_WritePin(KROKOWY1_GPIO_Port, KROKOWY1_Pin, 0);
	HAL_GPIO_WritePin(KROKOWY2_GPIO_Port, KROKOWY2_Pin, 0);
	HAL_GPIO_WritePin(KROKOWY3_GPIO_Port, KROKOWY3_Pin, 0);
	HAL_GPIO_WritePin(KROKOWY4_GPIO_Port, KROKOWY4_Pin, 0);
}
