/**
  *	@file    Blue.h
  * @brief   Plik zawiera funkcje obslugujace komunikacje poprzez interfejs Bluetooth
  */

#pragma once
#include "main.h"
#include "Queue.h"
#include "SimpleKinematic.h"
#include "MotorControl.h"
#include "Pompka.h"
#include "ReversKinematic.h"
#include "hd44780.h"
#include "StepperControl.h"
#include "buzzer.h"

#define RIGHT_PAD 0x7e //126
#define GYRO 0x7d //125
#define POMPKA 0x7c
#define PODSTAWA 0x7b
#define S1 0x7a
#define S2 0x79
#define S3 0x78
#define S4 0x77
#define CHW_OBR 0x76
#define CHW 0x75
#define LEFT_PAD 0x74
#define VBAR 0x73
#define DEFAULT 0x72
uint8_t BTData;
#define ABS(x) (((x < 0) ? (-x) : (x)))


/**
  * @brief   Przerwanie odebrania danych z Bluetooth-a wrzuca wszystkie dane do kolejki
  */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	qAdd(&droidQ, BTData);
	HAL_UART_Receive_DMA(&huart2, &BTData, 1);
}



/**
  * @brief   Czeka na nastepna dana w kolejce
  */
int8_t GetNextData()
{
	while(qEmpty(&droidQ)) {HAL_Delay(10);}
	return qGet(&droidQ);
}


/**
  * @brief   obsluga informacji z prawego pada
  */
void RightPad(int x, int y)
{
	float l = 0;
	float r = 0;
	l += (-y / 100.0f) * 0.8f;
	r += (-y / 100.0f) * 0.8f;
	//int znak = (y < 0) ? -1 : 1;

	if(y < 38)
	{
		l += (x / 100.0f) * 0.5f;
		r -= (x / 100.0f) * 0.5f;
	}
	else
	{
		l -= (x / 100.0f) * 0.5f;
		r += (x / 100.0f) * 0.5f;
	}
	if(l <= -1) l = -0.99;
	if(l >= 1) l = 0.99;
	if(r <= -1) r = -0.99;
	if(r >= 1) r = 0.99;
	hd44780_clear();
	hd44780_print("RP: ");
	hd44780_print_int((int)(l*100));
	hd44780_print(", ");
	hd44780_print_int((int)r*100);
	JazdaP(l, r);
}

/**
  * @brief  obsluga informacji z �yroskopu(z komorki)
  */
void Gyro(int x, int y, int z)
{
	hd44780_clear();
	hd44780_print("Zyro: ");
	hd44780_print_int(x);
	hd44780_print(", ");
	hd44780_print_int(y);
	hd44780_print(", ");
	hd44780_print_int(z);
}

/**
  * @brief Glowna petla odbierania komunikatow z telefonu
  */
void BluetoothComunication(arm* a)
{
	droidQ.head = 0;
	droidQ.tail = 0;
	int baseDir = 0;
	int baseMax = 50;

//	hd44780_clear();
//	hd44780_print("Blue1!");
	KrokowyStop();

	while(1)
	{

		if(!qEmpty(&droidQ))
		{

			int8_t data = qGet(&droidQ);
//			if(data == 112)
//			{
//
//				HAL_GPIO_TogglePin(LED_zielony_GPIO_Port, LED_zielony_Pin);
//				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, 111);
//				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[0].ang);
//				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[1].ang);
//				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[2].ang);
//				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[3].ang);
//			}
//			else
			if(data == RIGHT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				RightPad(x, y);
			}
			else if(data == DEFAULT)
			{
				SetSlowAngle(a, 0, 30, 30, 30, 0, 0, 500);
			}
			else if(data == LEFT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				float yy = y / 33.333f;
				hd44780_clear();

				BuzzerSet();
				if(ABS(yy)>0.2)
					rkMoveArm(a, yy, 0);
				BuzzerReset();

				baseDir = (x < 0) ? 0 : 1;
				baseMax = 50 - abs(x / 2);


				stepper_speed = baseMax;
				if(ABS(x) < 15) stepper_speed = 0;
				if(ABS(x) == 50) stepper_speed = 1;

				stepper_dir = baseDir;


				hd44780_print("LP: ");
				hd44780_print_int(x);
				hd44780_print(", ");
				hd44780_print_int(yy*100);
			}
			else if(data == VBAR)
			{
				int8_t h = GetNextData();
				float step_down = h / 16.666f;
				int res = rkMoveArm(a, 0.0f, step_down);

				hd44780_clear();
				hd44780_print("rkRes: ");
				hd44780_print_int(ABS(res));
			}
			else if(data == GYRO)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				int8_t z = GetNextData();
				Gyro(x, y, z);
			}
			else if(data == S1)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 0, angle,300);
			}
			else if(data == S2)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 1, angle,300);
			}
			else if(data == S3)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 2, angle,300);
			}
			else if(data == S4)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 3, angle,300);
			}
			else if(data == CHW)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 5, angle,300);
			}
			else if(data == CHW_OBR)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 4, angle,300);
			}
			else if(data == POMPKA)
			{
				int8_t v = GetNextData();
				PompkaWypelnienieP( v / 100.0f);
				hd44780_clear();
				hd44780_print("Pompka: ");
				hd44780_print_int(v);
				hd44780_print("%");
			}
			else if(data == PODSTAWA)
			{
				int8_t v = GetNextData();
				baseDir = (v < 0) ? 0 : 1;
				baseMax = 50 - ABS(v);
				stepper_speed = baseMax;
				if(v == 0) stepper_speed = 0;
				if(ABS(v) == 50) stepper_speed = 1;
				stepper_dir = baseDir;
				hd44780_clear();
				hd44780_print("ob: ");
				hd44780_print_int(baseMax);
				//hd44780_print(", ");
				//hd44780_print_int(baseDir);
			}
		}
		//if(baseMax != 50) baseCount++;
		//if(baseCount >= baseMax)
		//{
			//Krocz(1, baseDir);
			//MikroKrok2(baseDir);
			//baseCount = 0;
			//hd44780_print(".");
		//}
		HAL_Delay(10);
	}
}
