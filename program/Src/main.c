/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "hd44780.h"
#include "RC5.h"
//#include "menu.h"
#include "Pompka.h"
#include "ServoControl.h"
#include "Blue.h"
#include "StepperControl.h"
#include "MotorControl.h"
#include "Queue.h"
#include "ReversKinematic.h"
#include "SimpleKinematic.h"
#include "buzzer.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
arm a; //manipulator
Memory m;
volatile int licznik = 0;
int wybrane = 0;
int useconds = 1500 / 2;
int dt = 10;
int cmd, toggle;
uint16_t adc;
int16_t i = -50, j = -17, k = 16, l = 49;
uint8_t rosnij0 = 0, rosnij1 = 0, rosnij2 = 0, rosnij3 = 0;
char udg[] = { 0x00, 0x00, 0x0a, 0x00, 0x11, 0x0e, 0x00, 0x00 };
#define UDG	0
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void BujajSie();
void ShowAllAngle(arm* a);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM14) {
		//BujajSie();
	}
	if (htim->Instance == TIM8) {
		SteperTimerHandler();


	}
	if (htim->Instance == TIM13) {
		volatile static uint8_t inttemp;
				// zmienna timerL zwi�ksza si� co 32us
				timerL++;
				// zmienna timerH  zwi�ksza si� co 8.192ms (32us*256)
				inttemp++;
				if (!inttemp)
					timerH++;
	}
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM8_Init();
  MX_TIM12_Init();
  MX_UART4_Init();
  MX_USART2_UART_Init();
  MX_TIM14_Init();
  MX_TIM6_Init();
  MX_TIM13_Init();

  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_DMA(&huart2, &BTData, 1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);
	HAL_TIM_Base_Start_IT(&htim14);		//10hz
	HAL_TIM_Base_Start_IT(&htim13);		//co 32us do obslugi RC5
	HAL_TIM_Base_Start_IT(&htim8);		//50hz
	HAL_ADC_Start_DMA(&hadc1, (uint32_t) adc, 1);

	rkInit();
	InitMemory(&m);
	hd44780_init(GPIOE, GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_3,
	GPIO_PIN_2, GPIO_PIN_14, GPIO_PIN_15, HD44780_LINES_2, HD44780_FONT_5x8);
	hd44780_position(0, 1);
	hd44780_display(true, false, false);
	Init(&a);
	Move(a);
	SetSlowAngle(&a, 0, 0, 0, 0, 0, 50, 500);

//		hd44780_cgram(UDG, udg);
	hd44780_position(0, 1);
	hd44780_print("KoNaR");
//		hd44780_put(UDG);

	//ZerujKrokowy();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
//		BluetoothComunication(&a);
		while ((cmd = detect()) < 0)
			;
		toggle = cmd & 2048;
		cmd &= ~2048;
		hd44780_print_int(cmd);
		if (cmd == KEY_1)
			wybrane = 0;
		if (cmd == KEY_2)
			wybrane = 1;
		if (cmd == KEY_3)
			wybrane = 2;
		if (cmd == KEY_4)
			wybrane = 3;
		if (cmd == KEY_5)
			wybrane = 4;
		if (cmd == KEY_6)
			wybrane = 5;
		if (cmd == KEY_UP) {
			useconds += dt;
			servoSet(wybrane, useconds);
		}
		if (cmd == KEY_DOWN) {
			useconds -= dt;
			servoSet(wybrane, useconds);
		}
		if (cmd == KEY_LEFT) {
			dt -= 1;
		}
		if (cmd == KEY_RIGHT) {
			dt += 1;
		}
		hd44780_clear();
		hd44780_print("Wybrane: ");
		hd44780_print_int(wybrane + 1);
		hd44780_print(", dt: ");
		hd44780_print_int(dt);
		hd44780_position(0, 1);
		hd44780_print("Wartosc: ");
		hd44780_print_int(useconds);

		servoSet(wybrane, useconds);

		HAL_Delay(50);

//		rkMoveArm(&a, -1.0, 0.0);
//		HAL_Delay(2000);
//		rkMoveArm(&a, -5.0, 0.0);
//		HAL_Delay(2000);
//		rkMoveArm(&a, 50.0, 50.0);
//		HAL_Delay(2000);
//		int wynik = MainMenu();
//
//		if (wynik == 0) {
//			//NalejWodki(&m, &a);
//		}
//		//		else if (wynik == 1)
//		//			Zapamietane(&m, &a);
//		else if (wynik == 2)
//			KalibracjaServ();
//		else if (wynik == 3)
//			RecznaManipulacja(&a);
//		//		else if (wynik == 4)
//		//			Odwrotna(&a);
//		//		else if (wynik == 5)
//		//			Zwierzeta(&a);
//		else if (wynik == 6)
//			Pompka();
//		else if (wynik == 7)
//			ZerujKrokowy();
//		//		else if(wynik == 8)
//		//			Komunikacja(&a);
//		else if (wynik == 9)
//			Napiecie();
//		else if (wynik == 10)
//			MenuJazda();
//		else if (wynik == 11)
//			BluetoothComunication(&a);
//
//	}
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	}
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void BujajSie() {
	if (i <= -50)
		rosnij0 = 1;
	if (i >= 50)
		rosnij0 = 0;
	if (rosnij0)
		i = i + 1;
	else
		i = i - 1;
	if (j <= -50)
		rosnij1 = 1;
	if (j >= 50)
		rosnij1 = 0;
	if (rosnij1)
		j = j + 1;
	else
		j = j - 1;
	if (k <= -50)
		rosnij2 = 1;
	if (k >= 50)
		rosnij2 = 0;
	if (rosnij2)
		k = k + 1;
	else
		k = k - 1;
	if (l <= -50)
		rosnij3 = 1;
	if (l >= 50)
		rosnij3 = 0;
	if (rosnij3)
		l = l + 1;
	else
		l = l - 1;
	servoAngle0(i);
	servoAngle1(j);
	servoAngle2(k);
	servoAngle3(l);
}

void ShowAllAngle(arm* a) {
	int i, b;
	hd44780_position(0, 0);
	for (i = 0; i < COUNT; i++) {
		b = (int) (a->tab[i].ang);
		hd44780_print_int(b);
		hd44780_print(" ");
	}
	hd44780_print("K(");
	//hd44780_print_int(mKrok);
	hd44780_print(", ");
	//hd44780_print_int(mMicroKrok);
	hd44780_print(")  ");
}
/**
 * @brief Wyswietla w petli napiecie baterii na wyswietlaczu
 */
//void Napiecie() {
//int cmd;
//LCD_Clear();
//while (1) {
//	cmd = detect();
//	if (cmd > 0) {
//		cmd &= ~2048;
//		if (cmd == KEY_ESC)
//			return;
//	}
//
//	ADC_SoftwareStartConv(ADC1);
//	int adc = ADC_GetConversionValue(ADC1);
//	float V = (3.99) * adc * 3.31 / 4095;
//
//	LCD_GoTo(0, 0);
//	LCD_WriteText("Napiecie :");
//	LCDOutFloat(V, 2, 5);
//	LCD_GoTo(0, 1);
//	LCD_WriteText("Raw :");
//	LCDOutint16(adc, 5);
//
//}
//}
/**
 * @brief wyswietla katy ramienai na wyswietlaczu
 */
//void ShowAllAngle(arm* a) {
//int i, b;
//LCD_GoTo(0, 0);
//for (i = 0; i < COUNT; i++) {
//	b = (int) (a->tab[i].ang);
//	LCD_WriteInt(b);
//	LCD_WriteText(" ");
//}
//LCD_WriteText("K(");
////LCD_WriteInt(mKrok);
//LCD_WriteText(", ");
////LCD_WriteInt(mMicroKrok);
//LCD_WriteText(")  ");
//}
//
///**
// * @brief funkcja sterowania ramieniem z pilota
// */
//void RecznaManipulacja(arm* a) {
//int cmd, toogle;
//int wybrane = 0;
//int dt = 1;
//LCD_Clear();
////for (int i = 0; i < 4; i++)
////{
////a->tab[i].ang = 0;
////}
//a->tab[5].ang = 30;
//ShowAllAngle(a);
//_delay_ms(2000);
//Move(*a);
//a->tab[5].ang = 50;
//ShowAllAngle(a);
//LCD_GoTo(0, 2);
//LCD_WriteText("Wybrane: ");
//LCD_WriteInt(wybrane + 1);
//int zmiana = 0;
//while (1) {
//	zmiana = 0;
//	while ((cmd = detect()) < 0)
//		;
//	toogle = cmd & 2048;
//	cmd &= ~2048;
//	zmiana = 0;
//	if (cmd == KEY_ESC)
//		return;
//	if (cmd == KEY_1) {
//		wybrane = 0;
//		zmiana = 1;
//	}
//	if (cmd == KEY_2) {
//		wybrane = 1;
//		zmiana = 1;
//	}
//	if (cmd == KEY_3) {
//		wybrane = 2;
//		zmiana = 1;
//	}
//	if (cmd == KEY_4) {
//		wybrane = 3;
//		zmiana = 1;
//	}
//	if (cmd == KEY_5) {
//		wybrane = 4;
//		zmiana = 1;
//	}
//	if (cmd == KEY_6) {
//		wybrane = 5;
//		zmiana = 1;
//	}
//	if (cmd == KEY_UP) {
//		a->tab[wybrane].ang -= dt;
//		zmiana = 1;
//	}
//	if (cmd == KEY_DOWN) {
//		a->tab[wybrane].ang += dt;
//		zmiana = 1;
//	}
//	if (cmd == KEY_LEFT) {
//		Krocz(1, 1);
//	}
//	if (cmd == KEY_RIGHT) {
//		Krocz(1, 0);
//	}
//	//if(cmd == KEY_UP) { dt += 1; ShowAngle(dt);}
//	//if(cmd == KEY_DOWN) { dt -= 1; ShowAngle(dt);}
//	//if(a->tab[0].ang > 60) {a->tab[0].ang = 60;}
//	//if(a->tab[0].ang < -60) {a->tab[0].ang = -60;}
//
//	//Set(a);
//	if (zmiana) {
//		Move(*a);
//		LCD_GoTo(0, 2);
//		LCD_WriteText("Wybrane: ");
//		LCD_WriteInt(wybrane + 1);
//		//Show3Angle(a->tab[0].ang, a->tab[1].ang, a->tab[2].ang,a->tab[3].ang);
//		ShowAllAngle(a);
//	}
//}
//}
//
///**
// * @brief funkcja umozliwia precyzyjne zmiany wypelnienia na serwomechanizmach
// */
//void KalibracjaServ() {
//int cmd, toogle;
//int wybrane = 0;
//int useconds = 1500;
//int dt = 10;
//while (1) {
//	while ((cmd = detect()) < 0)
//		;
//	toogle = cmd & 2048;
//	cmd &= ~2048;
//	if (cmd == KEY_ESC)
//		return;
//	if (cmd == KEY_1)
//		wybrane = 0;
//	if (cmd == KEY_2)
//		wybrane = 1;
//	if (cmd == KEY_3)
//		wybrane = 2;
//	if (cmd == KEY_4)
//		wybrane = 3;
//	if (cmd == KEY_5)
//		wybrane = 4;
//	if (cmd == KEY_6)
//		wybrane = 5;
//	if (cmd == KEY_UP) {
//		useconds += dt;
//		servoSet(wybrane, useconds);
//	}
//	if (cmd == KEY_DOWN) {
//		useconds -= dt;
//		servoSet(wybrane, useconds);
//	}
//	if (cmd == KEY_LEFT) {
//		dt -= 1;
//	}
//	if (cmd == KEY_RIGHT) {
//		dt += 1;
//	}
//
//	LCD_Clear();
//	LCD_WriteText("Wybrane: ");
//	LCD_WriteInt(wybrane + 1);
//	LCD_WriteText(", dt: ");
//	LCD_WriteInt(dt);
//	LCD_GoTo(0, 1);
//	LCD_WriteText("wartosc: ");
//	LCD_WriteInt(useconds);
//
//	servoSet(wybrane, useconds);
//
//	_delay_ms(50);
//}
//
//}
//
///**
// * @brief funkcja umozliwia sterowanie pompka z pilota
// */
//void Pompka() {
//int cmd;
//LCD_Clear();
//while (1) {
//	while ((cmd = detect()) < 0)
//		;
//	cmd &= ~2048;
//	if (cmd == KEY_ESC)
//		return;
//	if (cmd == KEY_LEFT) {
//		PompkaWypelnienieMod(-200);
//	}
//	if (cmd == KEY_RIGHT) {
//		PompkaWypelnienieMod(200);
//	}
//	if (cmd == KEY_8) {
//		PompkaWypelnienie(0);
//	}
//	if (cmd == KEY_9) {
//		PompkaWypelnienie(POMPKA_MAX_VALUE);
//	}
//	LCD_GoTo(0, 0);
//	LCDOutFloat((POMPKA_TIM->POMPKA_CCR) / 10000.0f, 1, 5);
//	LCD_WriteText("    ");
//	_delay_ms(100);
//}
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
