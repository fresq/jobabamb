Partlist

Exported from untitled1.sch at 2013-11-14 02:11:48

EAGLE Version 6.5.0 Copyright (c) 1988-2013 CadSoft

Assembly variant: 

Part        Value           Device          Package         Library         Sheet

3.3V        7805TV          7805TV          TO220V          linear          1
5.0V        7805TV          7805TV          TO220V          linear          1
C1                          C-EUC1206       C1206           rcl             1
C2                          C-EUC1206       C1206           rcl             1
C3                          CPOL-EUE5-6     E5-6            rcl             1
C4                          CPOL-EUE5-6     E5-6            rcl             1
C5                          C-EUC1206       C1206           rcl             1
C6                          C-EUC1206       C1206           rcl             1
C7                          C-EUC1206       C1206           rcl             1
C8                          C-EUC1206       C1206           rcl             1
C9                          C-EUC1206       C1206           rcl             1
C10                         C-EUC1206       C1206           rcl             1
C11                         C-EUC1206       C1206           rcl             1
C12                         CPOL-EUE5-6     E5-6            rcl             1
C13                         CPOL-EUE5-6     E5-6            rcl             1
C14                         C-EUC1206       C1206           rcl             1
IC2         FT232RL         FT232RL         SSOP28          ftdichip        1
IC4         MAX6520         MAX6520         SOT23           maxim           1
JP1                         PINHD-2X2       2X02            pinhead         1
JP2                         PINHD-1X1       1X01            pinhead         1
JP3                         PINHD-1X1       1X01            pinhead         1
JP4                         PINHD-1X1       1X01            pinhead         1
JP5                         PINHD-1X1       1X01            pinhead         1
JP6                         PINHD-1X1       1X01            pinhead         1
KROKOWY1    TB6612FNG       TB6612FNG       SSOP24          tb6612          1
KROKOWY2    TB6612FNG       TB6612FNG       SSOP24          tb6612          1
L1                          L-EUL2825P      L2825P          rcl             1
L2                          L-EUL2825P      L2825P          rcl             1
LED1                        LEDSML1206      SML1206         led             1
PROGRAMATOR                 PINHD-2X3       2X03            pinhead         1
Q1          CSM-7X-DU       CSM-7X-DU       CSM-7X-DU       crystal         1
R1                          R-EU_R1210      R1210           rcl             1
R2                          R-EU_R1206      R1206           rcl             1
R3                          R-EU_R1206      R1206           rcl             1
R4                          R-EU_R1206      R1206           rcl             1
S1                          10-XX           B3F-10XX        switch-omron    1
SHAR                        PINHD-1X3       1X03            pinhead         1
SV1                         MA03-2          MA03-2          con-lstb        1
U1          ATXMEGA64D3-AU  ATXMEGA64D3-AU  TQFP-64A-64     atmega-atxmega  1
USB                         PINHD-1X4       1X04            pinhead         1
X1          MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 con-phoenix-508 1
X3          MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 con-phoenix-508 1
X4          MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 con-phoenix-508 1
X5          MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 con-phoenix-508 1
X6          MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 MKDSN1,5/2-5,08 con-phoenix-508 1
