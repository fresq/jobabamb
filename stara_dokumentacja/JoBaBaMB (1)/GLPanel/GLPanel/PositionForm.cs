﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GLPanel
{
	public partial class PositionForm : Form
	{
		Pos pos;
		public PositionForm(Pos p)
		{
			InitializeComponent();
			this.pos = p;
			textBox1.Text = p.nazwa;
			numericUpDown1.Minimum = -100;
			numericUpDown2.Minimum = -100;
			numericUpDown3.Minimum = -100;
			numericUpDown4.Minimum = -100;
			numericUpDown5.Minimum = -100; 
			numericUpDown1.Value = p.a1;
			numericUpDown2.Value = p.a2;
			numericUpDown3.Value = p.a3;
			numericUpDown4.Value = p.a4;
			numericUpDown5.Value = p.ch;
			textBox1.SelectAll();
			textBox1.Focus();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			pos.a1 = (int)numericUpDown1.Value;
			pos.a2 = (int)numericUpDown2.Value;
			pos.a3 = (int)numericUpDown3.Value;
			pos.a4 = (int)numericUpDown4.Value;
			pos.ch = (int)numericUpDown5.Value;
			pos.nazwa = textBox1.Text;
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				pos.a1 = (int)numericUpDown1.Value;
				pos.a2 = (int)numericUpDown2.Value;
				pos.a3 = (int)numericUpDown3.Value;
				pos.a4 = (int)numericUpDown4.Value;
				pos.ch = (int)numericUpDown5.Value;
				pos.nazwa = textBox1.Text;
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
		}
	}
}
