﻿namespace GLPanel
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.opengl = new System.Windows.Forms.Button();
			this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button9 = new System.Windows.Forms.Button();
			this.posName = new System.Windows.Forms.TextBox();
			this.save = new System.Windows.Forms.Button();
			this.push = new System.Windows.Forms.Button();
			this.realTime = new System.Windows.Forms.CheckBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.hScrollBar6 = new System.Windows.Forms.HScrollBar();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
			this.label3 = new System.Windows.Forms.Label();
			this.hScrollBar3 = new System.Windows.Forms.HScrollBar();
			this.label2 = new System.Windows.Forms.Label();
			this.hScrollBar2 = new System.Windows.Forms.HScrollBar();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.connect = new System.Windows.Forms.Button();
			this.hScrollBar7 = new System.Windows.Forms.HScrollBar();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.wypelnienieCzas = new System.Windows.Forms.TextBox();
			this.button10 = new System.Windows.Forms.Button();
			this.pompkaJazda = new System.Windows.Forms.Button();
			this.wypelnieniPompka = new System.Windows.Forms.NumericUpDown();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.TextWypelnienie = new System.Windows.Forms.TextBox();
			this.numerServa = new System.Windows.Forms.ComboBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.c11 = new System.Windows.Forms.CheckBox();
			this.zatwiedzCewki = new System.Windows.Forms.Button();
			this.c12 = new System.Windows.Forms.CheckBox();
			this.c22 = new System.Windows.Forms.CheckBox();
			this.c21 = new System.Windows.Forms.CheckBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.execute = new System.Windows.Forms.Button();
			this.executeText = new System.Windows.Forms.RichTextBox();
			this.ruszaj = new System.Windows.Forms.Button();
			this.moves = new System.Windows.Forms.ComboBox();
			this.button4 = new System.Windows.Forms.Button();
			this.rb = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.ustaw = new System.Windows.Forms.Button();
			this.poses = new System.Windows.Forms.ComboBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.button11 = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.comNumber = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.wypelnieniPompka)).BeginInit();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
			this.tabPage5.SuspendLayout();
			this.SuspendLayout();
			// 
			// opengl
			// 
			this.opengl.Location = new System.Drawing.Point(21, 12);
			this.opengl.Name = "opengl";
			this.opengl.Size = new System.Drawing.Size(103, 32);
			this.opengl.TabIndex = 0;
			this.opengl.Text = "OpenGL";
			this.opengl.UseVisualStyleBackColor = true;
			this.opengl.Click += new System.EventHandler(this.button1_Click);
			// 
			// hScrollBar1
			// 
			this.hScrollBar1.Location = new System.Drawing.Point(59, 31);
			this.hScrollBar1.Minimum = -100;
			this.hScrollBar1.Name = "hScrollBar1";
			this.hScrollBar1.Size = new System.Drawing.Size(218, 20);
			this.hScrollBar1.TabIndex = 1;
			this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button9);
			this.groupBox1.Controls.Add(this.posName);
			this.groupBox1.Controls.Add(this.save);
			this.groupBox1.Controls.Add(this.push);
			this.groupBox1.Controls.Add(this.realTime);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.numericUpDown5);
			this.groupBox1.Controls.Add(this.numericUpDown4);
			this.groupBox1.Controls.Add(this.numericUpDown3);
			this.groupBox1.Controls.Add(this.numericUpDown2);
			this.groupBox1.Controls.Add(this.numericUpDown1);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.hScrollBar6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.hScrollBar4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.hScrollBar3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.hScrollBar2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.hScrollBar1);
			this.groupBox1.Enabled = false;
			this.groupBox1.Location = new System.Drawing.Point(17, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(333, 276);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Kąty";
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(165, 192);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(52, 23);
			this.button9.TabIndex = 29;
			this.button9.Text = "Zerowa";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// posName
			// 
			this.posName.Location = new System.Drawing.Point(108, 235);
			this.posName.Name = "posName";
			this.posName.Size = new System.Drawing.Size(138, 20);
			this.posName.TabIndex = 28;
			// 
			// save
			// 
			this.save.Location = new System.Drawing.Point(252, 233);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(75, 23);
			this.save.TabIndex = 27;
			this.save.Text = "Zapisz";
			this.save.UseVisualStyleBackColor = true;
			this.save.Click += new System.EventHandler(this.save_Click);
			// 
			// push
			// 
			this.push.Location = new System.Drawing.Point(3, 244);
			this.push.Name = "push";
			this.push.Size = new System.Drawing.Size(75, 23);
			this.push.TabIndex = 26;
			this.push.Text = "Push";
			this.push.UseVisualStyleBackColor = true;
			this.push.Click += new System.EventHandler(this.push_Click);
			// 
			// realTime
			// 
			this.realTime.AutoSize = true;
			this.realTime.Location = new System.Drawing.Point(6, 221);
			this.realTime.Name = "realTime";
			this.realTime.Size = new System.Drawing.Size(74, 17);
			this.realTime.TabIndex = 25;
			this.realTime.Text = "Real Time";
			this.realTime.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(223, 192);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(104, 23);
			this.button2.TabIndex = 24;
			this.button2.Text = "Prawo  --->";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click_1);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(60, 192);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(99, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "<---   Lewo";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// numericUpDown5
			// 
			this.numericUpDown5.Location = new System.Drawing.Point(280, 159);
			this.numericUpDown5.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.numericUpDown5.Name = "numericUpDown5";
			this.numericUpDown5.Size = new System.Drawing.Size(47, 20);
			this.numericUpDown5.TabIndex = 23;
			this.numericUpDown5.Value = new decimal(new int[] {
            55,
            0,
            0,
            -2147483648});
			this.numericUpDown5.ValueChanged += new System.EventHandler(this.numericUpDown5_ValueChanged);
			// 
			// numericUpDown4
			// 
			this.numericUpDown4.Location = new System.Drawing.Point(280, 126);
			this.numericUpDown4.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.numericUpDown4.Name = "numericUpDown4";
			this.numericUpDown4.Size = new System.Drawing.Size(47, 20);
			this.numericUpDown4.TabIndex = 22;
			this.numericUpDown4.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Location = new System.Drawing.Point(280, 95);
			this.numericUpDown3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(47, 20);
			this.numericUpDown3.TabIndex = 21;
			this.numericUpDown3.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(280, 63);
			this.numericUpDown2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(47, 20);
			this.numericUpDown2.TabIndex = 20;
			this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(280, 31);
			this.numericUpDown1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(47, 20);
			this.numericUpDown1.TabIndex = 19;
			this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 162);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 13);
			this.label6.TabIndex = 18;
			this.label6.Text = "Chwytak";
			// 
			// hScrollBar6
			// 
			this.hScrollBar6.Location = new System.Drawing.Point(59, 159);
			this.hScrollBar6.Minimum = -100;
			this.hScrollBar6.Name = "hScrollBar6";
			this.hScrollBar6.Size = new System.Drawing.Size(218, 20);
			this.hScrollBar6.TabIndex = 16;
			this.hScrollBar6.Value = -55;
			this.hScrollBar6.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar6_Scroll);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(0, 197);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(54, 13);
			this.label5.TabIndex = 15;
			this.label5.Text = "Podstawa";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 129);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(46, 13);
			this.label4.TabIndex = 12;
			this.label4.Text = "Ramie 4";
			// 
			// hScrollBar4
			// 
			this.hScrollBar4.Location = new System.Drawing.Point(59, 126);
			this.hScrollBar4.Minimum = -100;
			this.hScrollBar4.Name = "hScrollBar4";
			this.hScrollBar4.Size = new System.Drawing.Size(218, 20);
			this.hScrollBar4.TabIndex = 10;
			this.hScrollBar4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar4_Scroll);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 98);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Ramie 3";
			// 
			// hScrollBar3
			// 
			this.hScrollBar3.Location = new System.Drawing.Point(59, 95);
			this.hScrollBar3.Minimum = -100;
			this.hScrollBar3.Name = "hScrollBar3";
			this.hScrollBar3.Size = new System.Drawing.Size(218, 20);
			this.hScrollBar3.TabIndex = 7;
			this.hScrollBar3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar3_Scroll);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 66);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Ramie 2";
			// 
			// hScrollBar2
			// 
			this.hScrollBar2.Location = new System.Drawing.Point(59, 63);
			this.hScrollBar2.Minimum = -100;
			this.hScrollBar2.Name = "hScrollBar2";
			this.hScrollBar2.Size = new System.Drawing.Size(218, 20);
			this.hScrollBar2.TabIndex = 4;
			this.hScrollBar2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar2_Scroll);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 34);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(46, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Ramie 1";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.panel1.Location = new System.Drawing.Point(65, 54);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(212, 91);
			this.panel1.TabIndex = 4;
			this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
			this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
			this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
			// 
			// connect
			// 
			this.connect.Location = new System.Drawing.Point(142, 12);
			this.connect.Name = "connect";
			this.connect.Size = new System.Drawing.Size(112, 32);
			this.connect.TabIndex = 5;
			this.connect.Text = "Connect";
			this.connect.UseVisualStyleBackColor = true;
			this.connect.Click += new System.EventHandler(this.button2_Click);
			// 
			// hScrollBar7
			// 
			this.hScrollBar7.Location = new System.Drawing.Point(65, 16);
			this.hScrollBar7.Minimum = 10;
			this.hScrollBar7.Name = "hScrollBar7";
			this.hScrollBar7.Size = new System.Drawing.Size(212, 20);
			this.hScrollBar7.TabIndex = 19;
			this.hScrollBar7.Value = 80;
			this.hScrollBar7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar7_Scroll);
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(280, 16);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(47, 20);
			this.textBox7.TabIndex = 20;
			this.textBox7.Text = "80";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 19);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 13);
			this.label7.TabIndex = 21;
			this.label7.Text = "Odległość";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.panel1);
			this.groupBox2.Controls.Add(this.textBox7);
			this.groupBox2.Controls.Add(this.hScrollBar7);
			this.groupBox2.Enabled = false;
			this.groupBox2.Location = new System.Drawing.Point(17, 302);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(333, 151);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Widok";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabControl1.Location = new System.Drawing.Point(0, 50);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(396, 485);
			this.tabControl1.TabIndex = 6;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Controls.Add(this.groupBox2);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(388, 459);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Ogolne";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.groupBox5);
			this.tabPage2.Controls.Add(this.groupBox4);
			this.tabPage2.Controls.Add(this.groupBox3);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(388, 459);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Zarzadzanie";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label8);
			this.groupBox5.Controls.Add(this.wypelnienieCzas);
			this.groupBox5.Controls.Add(this.button10);
			this.groupBox5.Controls.Add(this.pompkaJazda);
			this.groupBox5.Controls.Add(this.wypelnieniPompka);
			this.groupBox5.Location = new System.Drawing.Point(29, 235);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(324, 156);
			this.groupBox5.TabIndex = 7;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Pompka";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(131, 42);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(20, 13);
			this.label8.TabIndex = 4;
			this.label8.Text = "ms";
			// 
			// wypelnienieCzas
			// 
			this.wypelnienieCzas.Location = new System.Drawing.Point(73, 39);
			this.wypelnienieCzas.Name = "wypelnienieCzas";
			this.wypelnienieCzas.Size = new System.Drawing.Size(56, 20);
			this.wypelnienieCzas.TabIndex = 3;
			this.wypelnienieCzas.Text = "500";
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(243, 35);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(75, 26);
			this.button10.TabIndex = 2;
			this.button10.Text = "Stop";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.button10_Click);
			// 
			// pompkaJazda
			// 
			this.pompkaJazda.Location = new System.Drawing.Point(157, 35);
			this.pompkaJazda.Name = "pompkaJazda";
			this.pompkaJazda.Size = new System.Drawing.Size(80, 26);
			this.pompkaJazda.TabIndex = 1;
			this.pompkaJazda.Text = "Zaaplikuj";
			this.pompkaJazda.UseVisualStyleBackColor = true;
			this.pompkaJazda.Click += new System.EventHandler(this.pompkaJazda_Click);
			// 
			// wypelnieniPompka
			// 
			this.wypelnieniPompka.Location = new System.Drawing.Point(6, 40);
			this.wypelnieniPompka.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.wypelnieniPompka.Name = "wypelnieniPompka";
			this.wypelnieniPompka.Size = new System.Drawing.Size(61, 20);
			this.wypelnieniPompka.TabIndex = 0;
			this.wypelnieniPompka.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.TextWypelnienie);
			this.groupBox4.Controls.Add(this.numerServa);
			this.groupBox4.Location = new System.Drawing.Point(29, 135);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(324, 81);
			this.groupBox4.TabIndex = 6;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Serwa wypełnienie";
			// 
			// TextWypelnienie
			// 
			this.TextWypelnienie.Location = new System.Drawing.Point(201, 33);
			this.TextWypelnienie.Name = "TextWypelnienie";
			this.TextWypelnienie.Size = new System.Drawing.Size(100, 20);
			this.TextWypelnienie.TabIndex = 1;
			this.TextWypelnienie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextWypelnienie_KeyPress);
			// 
			// numerServa
			// 
			this.numerServa.FormattingEnabled = true;
			this.numerServa.Items.AddRange(new object[] {
            "Serwo 1",
            "Serwo 2",
            "Serwo 3",
            "Serwo 4",
            "Chwytak"});
			this.numerServa.Location = new System.Drawing.Point(23, 32);
			this.numerServa.Name = "numerServa";
			this.numerServa.Size = new System.Drawing.Size(140, 21);
			this.numerServa.TabIndex = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.c11);
			this.groupBox3.Controls.Add(this.zatwiedzCewki);
			this.groupBox3.Controls.Add(this.c12);
			this.groupBox3.Controls.Add(this.c22);
			this.groupBox3.Controls.Add(this.c21);
			this.groupBox3.Location = new System.Drawing.Point(29, 19);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(324, 86);
			this.groupBox3.TabIndex = 5;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Silnik krokowy";
			// 
			// c11
			// 
			this.c11.AutoSize = true;
			this.c11.Location = new System.Drawing.Point(23, 31);
			this.c11.Name = "c11";
			this.c11.Size = new System.Drawing.Size(15, 14);
			this.c11.TabIndex = 0;
			this.c11.UseVisualStyleBackColor = true;
			// 
			// zatwiedzCewki
			// 
			this.zatwiedzCewki.Location = new System.Drawing.Point(190, 39);
			this.zatwiedzCewki.Name = "zatwiedzCewki";
			this.zatwiedzCewki.Size = new System.Drawing.Size(75, 23);
			this.zatwiedzCewki.TabIndex = 4;
			this.zatwiedzCewki.Text = "Zatwierdz";
			this.zatwiedzCewki.UseVisualStyleBackColor = true;
			this.zatwiedzCewki.Click += new System.EventHandler(this.zatwiedzCewki_Click);
			// 
			// c12
			// 
			this.c12.AutoSize = true;
			this.c12.Location = new System.Drawing.Point(23, 59);
			this.c12.Name = "c12";
			this.c12.Size = new System.Drawing.Size(15, 14);
			this.c12.TabIndex = 1;
			this.c12.UseVisualStyleBackColor = true;
			// 
			// c22
			// 
			this.c22.AutoSize = true;
			this.c22.Location = new System.Drawing.Point(100, 59);
			this.c22.Name = "c22";
			this.c22.Size = new System.Drawing.Size(15, 14);
			this.c22.TabIndex = 3;
			this.c22.UseVisualStyleBackColor = true;
			// 
			// c21
			// 
			this.c21.AutoSize = true;
			this.c21.Location = new System.Drawing.Point(100, 31);
			this.c21.Name = "c21";
			this.c21.Size = new System.Drawing.Size(15, 14);
			this.c21.TabIndex = 2;
			this.c21.UseVisualStyleBackColor = true;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.execute);
			this.tabPage3.Controls.Add(this.executeText);
			this.tabPage3.Controls.Add(this.ruszaj);
			this.tabPage3.Controls.Add(this.moves);
			this.tabPage3.Controls.Add(this.button4);
			this.tabPage3.Controls.Add(this.rb);
			this.tabPage3.Controls.Add(this.button3);
			this.tabPage3.Controls.Add(this.ustaw);
			this.tabPage3.Controls.Add(this.poses);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(388, 459);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Pozycje i Ruchy";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// execute
			// 
			this.execute.Location = new System.Drawing.Point(265, 273);
			this.execute.Name = "execute";
			this.execute.Size = new System.Drawing.Size(87, 24);
			this.execute.TabIndex = 8;
			this.execute.Text = "execute";
			this.execute.UseVisualStyleBackColor = true;
			this.execute.Click += new System.EventHandler(this.execute_Click);
			// 
			// executeText
			// 
			this.executeText.Location = new System.Drawing.Point(27, 134);
			this.executeText.Name = "executeText";
			this.executeText.Size = new System.Drawing.Size(325, 133);
			this.executeText.TabIndex = 7;
			this.executeText.Text = "";
			// 
			// ruszaj
			// 
			this.ruszaj.Location = new System.Drawing.Point(164, 66);
			this.ruszaj.Name = "ruszaj";
			this.ruszaj.Size = new System.Drawing.Size(75, 23);
			this.ruszaj.TabIndex = 6;
			this.ruszaj.Text = "Ruszaj";
			this.ruszaj.UseVisualStyleBackColor = true;
			this.ruszaj.Click += new System.EventHandler(this.ruszaj_Click);
			// 
			// moves
			// 
			this.moves.FormattingEnabled = true;
			this.moves.Location = new System.Drawing.Point(17, 68);
			this.moves.Name = "moves";
			this.moves.Size = new System.Drawing.Size(141, 21);
			this.moves.TabIndex = 5;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(264, 13);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(121, 23);
			this.button4.TabIndex = 4;
			this.button4.Text = "Menadżer Pozycji";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// rb
			// 
			this.rb.Location = new System.Drawing.Point(264, 66);
			this.rb.Name = "rb";
			this.rb.Size = new System.Drawing.Size(121, 23);
			this.rb.TabIndex = 3;
			this.rb.Text = "Menadżer Ruchów";
			this.rb.UseVisualStyleBackColor = true;
			this.rb.Click += new System.EventHandler(this.rb_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(245, 391);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(127, 23);
			this.button3.TabIndex = 2;
			this.button3.Text = "Zapis do pliku";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// ustaw
			// 
			this.ustaw.Location = new System.Drawing.Point(164, 13);
			this.ustaw.Name = "ustaw";
			this.ustaw.Size = new System.Drawing.Size(75, 23);
			this.ustaw.TabIndex = 1;
			this.ustaw.Text = "Ustaw";
			this.ustaw.UseVisualStyleBackColor = true;
			this.ustaw.Click += new System.EventHandler(this.ustaw_Click);
			// 
			// poses
			// 
			this.poses.FormattingEnabled = true;
			this.poses.Location = new System.Drawing.Point(17, 15);
			this.poses.Name = "poses";
			this.poses.Size = new System.Drawing.Size(141, 21);
			this.poses.TabIndex = 0;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.numericUpDown6);
			this.tabPage4.Controls.Add(this.button8);
			this.tabPage4.Controls.Add(this.button7);
			this.tabPage4.Controls.Add(this.button6);
			this.tabPage4.Controls.Add(this.button5);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(388, 459);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Odwrotna Kinematyka";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// numericUpDown6
			// 
			this.numericUpDown6.Location = new System.Drawing.Point(237, 38);
			this.numericUpDown6.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numericUpDown6.Name = "numericUpDown6";
			this.numericUpDown6.Size = new System.Drawing.Size(67, 20);
			this.numericUpDown6.TabIndex = 4;
			this.numericUpDown6.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			this.numericUpDown6.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
			// 
			// button8
			// 
			this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button8.Location = new System.Drawing.Point(141, 71);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(50, 50);
			this.button8.TabIndex = 3;
			this.button8.Text = "→";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button7
			// 
			this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button7.Location = new System.Drawing.Point(85, 71);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(50, 50);
			this.button7.TabIndex = 2;
			this.button7.Text = "↓";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button6
			// 
			this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button6.Location = new System.Drawing.Point(29, 71);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(50, 50);
			this.button6.TabIndex = 1;
			this.button6.Text = "←";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button5
			// 
			this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button5.Location = new System.Drawing.Point(85, 15);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(50, 50);
			this.button5.TabIndex = 0;
			this.button5.Text = "↑";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.textBox1);
			this.tabPage5.Controls.Add(this.button11);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(388, 459);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "tabPage5";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(73, 63);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(75, 23);
			this.button11.TabIndex = 0;
			this.button11.Text = "button11";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.button11_Click);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// comNumber
			// 
			this.comNumber.Location = new System.Drawing.Point(273, 19);
			this.comNumber.Name = "comNumber";
			this.comNumber.Size = new System.Drawing.Size(100, 20);
			this.comNumber.TabIndex = 7;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(176, 66);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 1;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(396, 535);
			this.Controls.Add(this.comNumber);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.connect);
			this.Controls.Add(this.opengl);
			this.Name = "Form1";
			this.Text = "JoBaBaMB";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Shown += new System.EventHandler(this.Form1_Shown);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.wypelnieniPompka)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
			this.tabPage5.ResumeLayout(false);
			this.tabPage5.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button opengl;
		private System.Windows.Forms.HScrollBar hScrollBar1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.HScrollBar hScrollBar6;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.HScrollBar hScrollBar4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.HScrollBar hScrollBar3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.HScrollBar hScrollBar2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button connect;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.HScrollBar hScrollBar7;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.NumericUpDown numericUpDown5;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.ComboBox numerServa;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.CheckBox c11;
		private System.Windows.Forms.Button zatwiedzCewki;
		private System.Windows.Forms.CheckBox c12;
		private System.Windows.Forms.CheckBox c22;
		private System.Windows.Forms.CheckBox c21;
		private System.Windows.Forms.TextBox TextWypelnienie;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.ComboBox poses;
		private System.Windows.Forms.Button push;
		private System.Windows.Forms.CheckBox realTime;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.TextBox posName;
		private System.Windows.Forms.Button ustaw;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button rb;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button ruszaj;
		private System.Windows.Forms.ComboBox moves;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.NumericUpDown numericUpDown6;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button pompkaJazda;
		private System.Windows.Forms.NumericUpDown wypelnieniPompka;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox wypelnienieCzas;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button execute;
		private System.Windows.Forms.RichTextBox executeText;
		private System.Windows.Forms.TextBox comNumber;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.TextBox textBox1;
	}
}

