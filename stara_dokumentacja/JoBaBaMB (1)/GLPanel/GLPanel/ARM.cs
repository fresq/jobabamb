﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GLPanel
{
	class ARM
	{

		public Joint[] Joints;
		public int count;
		public enum Mode { NONE, SET, MOD, SETR, MODR } ;
		Mode mode;

		private Random rand = new Random();

		public ARM()
		{
			count = 0;
			Joints = null;
			mode = ARM.Mode.NONE;
		}
		public ARM(int count)
		{
			this.count = count;
			mode = ARM.Mode.NONE;
			Joints = new Joint[count + 1];
			for (int i = 0; i < count + 1; i++)
				Joints[i] = new Joint(0);
		}
		public ARM(Joint[] list, int count)
		{
			this.count = count;
			mode = Mode.NONE;
			for (int i = 0; i <= count; i++) Joints[i] = list[i];
		}

		public void Move() //podstawa calego systemu...
		{
			mode = Mode.NONE;
			for (int i = 1; i <= count; i++)
			{
				Joint j = Joints[i - 1] ^ Joints[i];
			}
			Joints[count - 1].Angle = -90 - Joints[count - 2].Arg;
			if (Joints[count - 1].Angle > 95) Joints[count - 1].Angle = 95;
			if (Joints[count - 1].Angle < -95) Joints[count - 1].Angle = -95;
		}


		public Joint GetJoint(int Id)
		{
			if (Id > 0 && Id <= count) return Joints[Id - 1]; else return Joints[count];
		}


		float FindAlpha(int id, float dx, float dy)
		{
			//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
			float y = GetJoint(count + 1).Loc.y;
			float x = GetJoint(count + 1).Loc.x;

			//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
			float p1 = GetJoint(id).Loc.x;
			float q1 = GetJoint(id).Loc.y;

			//Pobieramy punkt pierwszego stawu jako srodek nowego ukladu odniesienia
			float y0 = GetJoint(id - 1).Loc.y;
			float x0 = GetJoint(id - 1).Loc.x;

			float R = GetJoint(id - 1).Len;
			float alpha1 = GetJoint(id - 1).Arg;
			float r2 = (float)Math.Sqrt((dx - x0) * (dx - x0) + (dy - y0) * (dy - y0));
			float alpha2 = (float)Math.Atan2((dy - y0), (dx - x0)) * 180.0f / 3.14159265f;
			float l = (float)Math.Sqrt((x - p1) * (x - p1) + (y - q1) * (y - q1));

			float podacosinus = (r2 * r2 + R * R - l * l) / (2 * r2 * R);

			if (Math.Abs(podacosinus) < 1.0)
			{
				podacosinus = (float)Math.Acos(podacosinus) * 180.0f / 3.14159265f;
				return (alpha2 + podacosinus - alpha1);
			}
			else //niemozliwe
			{
				return 0.0f;
			}
		}

		ARM GoX(int id, float alpha)
		{
			//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
			float y = GetJoint(count).Loc.y;
			float x = GetJoint(count).Loc.x;

			//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
			float p1 = GetJoint(id).Loc.x;
			float q1 = GetJoint(id).Loc.y;

			//Pobieramy punkt pierwszego stawu
			float y0 = GetJoint(id - 1).Loc.y;
			float x0 = GetJoint(id - 1).Loc.x;

			//wyliczmy cosinus i sinus
			float c = (float)Math.Cos(alpha * 3.141592f / 180.0f);
			float s = (float)Math.Sin(alpha * 3.141592f / 180.0f);

			//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
			float p = (p1 - x0) * c - (q1 - y0) * s + x0;
			float q = (p1 - x0) * s + (q1 - y0) * c + y0;

			float x2 = (x - x0) * c - (y - y0) * s + x0;
			float y2 = (x - x0) * s + (y - y0) * c + y0;

			//Z rownania wyliczmy podpierwiastek
			float podpierwiastek = (x - p1) * (x - p1) + (y - q1) * (y - q1) - (y - q) * (y - q);

			//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
			if (podpierwiastek >= 0.0f)
			{
				//jesli tak konczymy wyliczanie x
				podpierwiastek = (float)Math.Sqrt(podpierwiastek);
				x = ((float)Math.Abs(p + podpierwiastek - x) < (float)Math.Abs(p - podpierwiastek - x)) ? (p + podpierwiastek) : (p - podpierwiastek);

				//wyliczmy kat zmiany dla drugiego stawu
				float beta = (float)Math.Atan2((y - q), (x - p)) - (float)Math.Atan2(y2 - q, x2 - p);
				beta = (beta * 180.0f) / 3.14159265f;
				//beta = beta - GetJoint(id).Arg;

				//Modyfikujemy go o wyliczony kąt
				if (Math.Abs(Joints[id - 2].Angle + alpha) < 95 && Math.Abs(Joints[id - 1].Angle + beta) < 95)
				{
					Mod(id - 1, alpha).Mod(id, beta).Move();
				}
			}
			else //gdy przsuniecie nie jest mozliwe
			{

			}
			return this;
		}
		ARM GoY(int id, float alpha)
		{
			//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
			float y = GetJoint(count).Loc.y;
			float x = GetJoint(count).Loc.x;

			//Obracamy pierwszy staw o zadany kat

			if (Math.Abs(Joints[id - 2].Angle + alpha) > 95) return this;
			Mod(id - 1, alpha).Move();

			//parametery receptora po pierwszym zgieciu
			float y2 = GetJoint(count).Loc.y;
			float x2 = GetJoint(count).Loc.x;

			//Pobieramy dlugosc i polozenie stawu
			float p = GetJoint(id).Loc.x;
			float q = GetJoint(id).Loc.y;
			float r2 = (x2 - p) * (x2 - p) + (y2 - q) * (y2 - q);

			//Zrownanie wyliczmy nowe polozenie x
			float podpierwiastek = -x * x + 2 * x * p - p * p + r2;

			//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
			if (podpierwiastek >= 0.0f)
			{
				podpierwiastek = (float)Math.Sqrt(podpierwiastek);
				y = ((float)Math.Abs(q + podpierwiastek - y) < (float)Math.Abs(q - podpierwiastek - y)) ? (q + podpierwiastek) : (q - podpierwiastek);

				float beta = (float)Math.Atan2((y - q), (x - p)) - (float)Math.Atan2(y2 - q, x2 - p);
				beta = (beta * 180.0f) / 3.14159265f;
				if (Math.Abs(Joints[id - 2].Angle + beta) > 95)
				{
					Mod(id - 1, -alpha).Move();
				}
				else
				{
					Mod(id, beta).Move();
				}
			}
			else
			{
				Mod(id - 1, -alpha).Move();
			}

			return this;
		}
		float CheckX(int id, float alpha)
		{
			//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
			float y = GetJoint(count + 1).Loc.y;
			float x = GetJoint(count + 1).Loc.x;

			//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
			float p1 = GetJoint(id).Loc.x;
			float q1 = GetJoint(id).Loc.y;

			//Pobieramy punkt pierwszego stawu
			float y0 = GetJoint(id - 1).Loc.y;
			float x0 = GetJoint(id - 1).Loc.x;

			//wyliczmy cosinus i sinus
			float c = (float)Math.Cos(alpha * 3.141592f / 180.0f);
			float s = (float)Math.Sin(alpha * 3.141592f / 180.0f);

			//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
			float p = (p1 - x0) * c - (q1 - y0) * s + x0;
			float q = (p1 - x0) * s + (q1 - y0) * c + y0;

			//Z rownania wyliczmy podpierwiastek
			float podpierwiastek = (x - p1) * (x - p1) + (y - q1) * (y - q1) - (y - q) * (y - q);

			//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
			if (podpierwiastek >= 0.0f)
			{
				//Do testow

				//jesli tak konczymy wyliczanie x
				podpierwiastek = (float)Math.Sqrt(podpierwiastek);
				float x2 = ((float)Math.Abs(p + podpierwiastek - x) < (float)Math.Abs(p - podpierwiastek - x)) ? (p + podpierwiastek) : (p - podpierwiastek);
				//Zwracamy przesuniecie
				return x2 - x;

			}
			else //gdy przsuniecie nie jest mozliwe cofamy pierwsza zmiane
			{
				return 0;
			}
		}
		float CheckY(int id, float alpha)
		{
			//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
			float y = GetJoint(count + 1).Loc.y;
			float x = GetJoint(count + 1).Loc.x;

			//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
			float p1 = GetJoint(id).Loc.x;
			float q1 = GetJoint(id).Loc.y;

			//Pobieramy punkt pierwszego stawu
			float y0 = GetJoint(id - 1).Loc.y;
			float x0 = GetJoint(id - 1).Loc.x;

			//wyliczmy cosinus i sinus
			float c = (float)Math.Cos(alpha * 3.141592f / 180.0f);
			float s = (float)Math.Sin(alpha * 3.141592f / 180.0f);

			//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
			float p = (p1 - x0) * c - (q1 - y0) * s + x0;
			float q = (p1 - x0) * s + (q1 - y0) * c + y0;

			//Z rownania wyliczmy podpierwiastek
			float podpierwiastek = (x - p1) * (x - p1) + (y - q1) * (y - q1) - (x - p) * (x - p);

			if (podpierwiastek >= 0.0f)
			{
				podpierwiastek = (float)Math.Sqrt(podpierwiastek);
				float y2 = ((float)Math.Abs(q + podpierwiastek - y) < (float)Math.Abs(q - podpierwiastek - y)) ? (q + podpierwiastek) : (q - podpierwiastek);
				return y2 - y;

			}
			else
			{
				return 0;
			}
		}

		public ARM MovX(float x)
		{
			for (int i = 0; i < 500; i++)
			{
				//Losujemy jeda z par do manipulacji
				int id = (rand.Next() % (count - 2)) + 2;
				float alpha = ((rand.Next() % 1000) - 500) / 1000.0f;
				//Sprawdzamy o ile by nastapilo przesuniecie
				float TryX = CheckX(id, alpha);
				//Sprawdzamy czy pasuje nam takie przesuniecie
				if ((float)Math.Abs(x - TryX) < (float)Math.Abs(x))
				{
					//Modyfikujemy pozostala odleglosc
					x = x - TryX;
					//Zapisujemy zmieny
					GoX(id, alpha);
				}
				if ((float)Math.Abs(x) < 0.1) break;
			}

			return this;
		}
		public ARM MovY(float y)
		{
			for (int i = 0; i < 500; i++)
			{
				//Losujemy jeda z par do manipulacji
				int id = (rand.Next() % (count - 2)) + 2;
				float alpha = ((rand.Next() % 1000) - 500) / 1000.0f;
				//Sprawdzamy o ile by nastapilo przesuniecie
				float TryY = CheckY(id, alpha);
				//Sprawdzamy czy pasuje nam takie przesuniecie
				if ((float)Math.Abs(y - TryY) < (float)Math.Abs(y))
				{
					//Modyfikujemy pozostala odleglosc
					y = y - TryY;
					//Zapisujemy zmieny
					GoY(id, alpha);
				}
				if ((float)Math.Abs(y) < 0.1) break;
			}

			return this;
		}
		public ARM MovX2(float x)
		{
			float dy = GetJoint(count + 1).Loc.y;
			float dx = GetJoint(count + 1).Loc.x + x;

			for (int j = count; j >= 2; j--)
			{
				float alpha = FindAlpha(j, dx, dy);
				if (alpha != 0.0)
				{
					GoX(j, alpha);
					break;
				}
			}

			return this;
		}
		public ARM MovY2(float y)
		{
			float dy = GetJoint(count + 1).Loc.y + y;
			float dx = GetJoint(count + 1).Loc.x;

			for (int j = count; j >= 2; j--)
			{
				float alpha = FindAlpha(j, dx, dy);
				if (alpha != 0.0)
				{
					GoY(j, alpha);
					break;
				}
			}

			return this;
		}

		#region fun
		float rad(float a)
		{
			return a * 3.14159265f / 180.0f;
		}
		float f1(float a1, float a2, float a3)
		{
			return 9 * ((float)Math.Cos(rad(a1 + a2 + a3)) + (float)Math.Cos(rad(a1 + a2)) + (float)Math.Cos(rad(a1)));
		}
		float f2(float a1, float a2, float a3)
		{
			return 9 * ((float)Math.Sin(rad(a1 + a2 + a3)) + (float)Math.Sin(rad(a1 + a2)) + (float)Math.Sin(rad(a1)));
		}
		float f11(float a1, float a2, float a3)
		{
			return 9 * (-(float)Math.Sin(rad(a1 + a2 + a3)) - (float)Math.Sin(rad(a1 + a2)) - (float)Math.Sin(rad(a1)));
		}
		float f12(float a1, float a2, float a3)
		{
			return 9 * (-(float)Math.Sin(rad(a1 + a2 + a3)) - (float)Math.Sin(rad(a1 + a2)));
		}
		float f13(float a1, float a2, float a3)
		{
			return 9 * (-(float)Math.Sin(rad(a1 + a2 + a3)));
		}
		float f21(float a1, float a2, float a3)
		{
			return 9 * ((float)Math.Cos(rad(a1 + a2 + a3)) + (float)Math.Cos(rad(a1 + a2)) + (float)Math.Cos(rad(a1)));
		}
		float f22(float a1, float a2, float a3)
		{
			return 9 * ((float)Math.Cos(rad(a1 + a2 + a3)) + (float)Math.Cos(rad(a1 + a2)));
		}
		float f23(float a1, float a2, float a3)
		{
			return 9 * ((float)Math.Cos(rad(a1 + a2 + a3)));
		}
		#endregion

		public ARM MovXY(float deltax, float deltay)
		{
			float a1 = Joints[0].Angle;
			float a2 = Joints[1].Angle;
			float a3 = Joints[2].Angle;
			float x = f1(a1, a2, a3);
			float y = f2(a1, a2, a3);
			float dx = x + deltax;
			float dy = y + deltay;

			for (int i = 0; i < 100000; i++)
			{
				float a11 = (dx - x) * f11(a1, a2, a3);
				float a12 = (dx - x) * f12(a1, a2, a3);
				float a13 = (dx - x) * f13(a1, a2, a3);

				float a21 = (dy - y) * f21(a1, a2, a3);
				float a22 = (dy - y) * f22(a1, a2, a3);
				float a23 = (dy - y) * f23(a1, a2, a3);

				float da1 = (a11 + a21) / 1000.0f;
				float da2 = (a12 + a22) / 1000.0f;
				float da3 = (a13 + a23) / 1000.0f;

				a1 += da1;
				a2 += da2;
				a3 += da3;

				x = f1(a1, a2, a3);
				y = f2(a1, a2, a3);

				//
			}
			Joints[0].Angle = a1;
			Joints[1].Angle = a2;
			Joints[2].Angle = a3;
			Move();
			return this;
		}

		public ARM ForceXY(float deltax, float deltay)
		{
			float a1 = Joints[0].Angle;
			float a2 = Joints[1].Angle;
			float a3 = Joints[2].Angle;
			float x = f1(a1, a2, a3);
			float y = f2(a1, a2, a3);
			float dx = x + deltax;
			float dy = y + deltay;
			bool b = false;
			for(a1 = -95.0f; a1 < 95.0f; a1 += 1.0f)
				for (a2 = -95.0f; a2 < 95.0f; a2 += 1.0f)
					for (a3 = -95.0f; a3 < 95.0f; a3 += 1.0f)
					{
						x = f1(a1, a2, a3);
						y = f2(a1, a2, a3);
						float d = (float)Math.Sqrt((dx - x) * (dx - x) + (dy - y) * (dy - y));
						if (d < 0.1)
						{
							Joints[0].Angle = a1;
							Joints[1].Angle = a2;
							Joints[2].Angle = a3;
							Move();
							return this;
						}
					}

			return this;
		}

		public void MoveTo(float x, float y)
		{
			float a1 = Joints[0].Angle;
			float a2 = Joints[1].Angle;
			float a3 = Joints[2].Angle;
			MovXY(x - f1(a1, a2, a3), y - f2(a1, a2, a3));
		}

		public ARM Mod(int Id, float Angle, float Len = 0)
		{
			mode = Mode.MOD;
			if (0 < Id && Id <= count)
			{
				if (Id == 1) Joints[Id - 1].Arg += Angle; //kat elementu bazowego jest jego transformacja
				Joints[Id - 1].Angle += Angle;
				Joints[Id - 1].Len += Len;
			}
			return this;
		}
		public ARM Set(int Id, float Len, float Angle = 0)
		{
			mode = Mode.SET;
			if (0 < Id && Id <= count)
			{
				if (Id == 1) Joints[Id - 1].Arg = Angle; //kat elementu bazowego jest jego transformacja
				Joints[Id - 1].Angle = Angle;
				Joints[Id - 1].Len = Len;
			}
			return this;
		}
		public ARM ModR(int Id, float Angle, float Len)
		{
			mode = Mode.MODR;
			if (0 < Id && Id <= count)
			{
				if (Id == 1) Joints[count - Id].Arg += Angle; //kat elementu bazowego jest jego transformacja
				Joints[count - Id].Angle += Angle;
				Joints[count - Id].Len += Len;
			}
			return this;
		}
		public ARM SetR(int Id, float Len, float Angle)
		{
			mode = Mode.SETR;
			if (0 < Id && Id <= count)
			{
				if (Id == 1) Joints[count - Id].Arg = Angle; //kat elementu bazowego jest jego transformacja
				Joints[count - Id].Angle = Angle;
				Joints[count - Id].Len = Len;
			}
			return this;
		}
		//public ARM this(int Id, float A, float B = 0)
		//{
		//	if(mode==Mode.MOD) return Mod(Id, A, B);
		//	if(mode==Mode.SET) return Set(Id, A, B);
		//	if(mode==Mode.MODR) return ModR(Id, A, B);
		//	if(mode==Mode.SETR) return SetR(Id, A, B);
		//	if(mode==Mode.NONE) throw "Hej, ty3 Wiesz, ze operator() nalezy poprzedzic poleceniem Set() lub Mod()?";

		//	return this; //profilaktycznie
		//}

	};
}
