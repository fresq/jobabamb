﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GLPanel
{
	public class Vec2f
	{
		public float x = 0, y = 0;

		public Vec2f()
		{

		}

		public Vec2f(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		public float Length() { return (float)Math.Sqrt(x * x + y * y); }

		public static Vec2f operator +(Vec2f v, Vec2f v2) { return new Vec2f(v2.x + v.x, v2.y + v.y); }
		public static Vec2f operator -(Vec2f v, Vec2f v2) { return new Vec2f(v2.x - v.x, v2.y - v.y); }

		public static Vec2f operator *(Vec2f v, float f) { return new Vec2f(v.x * f, v.y * f); }
		public static Vec2f operator /(Vec2f v, float f) { return new Vec2f(v.x / f, v.y / f); }

		public static float operator *(Vec2f v, Vec2f v2) { return v2.x * v.x + v2.y * v.y; }
		public static Vec2f operator -(Vec2f v) { return new Vec2f(-v.x, -v.y); }
		public static Vec2f operator ~(Vec2f v) { return new Vec2f(v.x, -v.y); }
	};
}
