﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GLPanel
{
	public partial class movementManager : Form
	{
		Movements moves;
		Positions positions;
		GLengine engine;
		public movementManager(Movements m, Positions p, GLengine engine)
		{
			InitializeComponent();
			this.moves = m;
			this.positions = p;
			this.engine = engine;
			Ref();
		}

		void Ref()
		{
			listBox1.Items.Clear();
			foreach (Move move in moves.moves)
			{
				listBox1.Items.Add(move);
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			Move m = new Move();
			ruchy r = new ruchy(positions, m, engine);
			if (r.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				moves.moves.Add(m);
				Ref();
			}
		}

		
		//edit
		private void button1_Click(object sender, EventArgs e)
		{
			ruchy r = new ruchy(positions,(Move)listBox1.SelectedItem,engine, true);
			r.Show();
			Ref();
		}

		//ok
		private void button4_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}


		//usun
		private void button2_Click(object sender, EventArgs e)
		{
			if (listBox1.SelectedItem != null)
			{
				moves.moves.Remove((Move)listBox1.SelectedItem);
				Ref();
			}
		}

		private void listBox1_DoubleClick(object sender, EventArgs e)
		{
			if (listBox1.SelectedItem != null)
			{
				ruchy r = new ruchy(positions, (Move)listBox1.SelectedItem, engine, true);
				r.Show();
				Ref();
			}
		}
	}
}
