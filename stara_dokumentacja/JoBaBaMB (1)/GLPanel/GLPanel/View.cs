﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GLPanel
{
	public partial class View : Form
	{
		GLengine engine;
		bool drag;
		int mx, my;
		float r = 80, xa = 0, ya = 10.0f * 3.14159265f / 180.0f;

		public View(GLengine engine)
		{
			InitializeComponent();
			this.engine = engine;
			this.Left =  30;
			this.Top = 30;
		}

		#region View
		private void hScrollBar7_Scroll(object sender, ScrollEventArgs e)
		{
			textBox7.Text = hScrollBar7.Value.ToString();
			r = hScrollBar7.Value;
			engine.SetView(r, xa, ya);
		}

		private void panel1_MouseDown(object sender, MouseEventArgs e)
		{
			drag = true;
			mx = e.X;
			my = e.Y;
		}

		private void panel1_MouseUp(object sender, MouseEventArgs e)
		{
			drag = false;
		}

		private void panel1_MouseMove(object sender, MouseEventArgs e)
		{
			if (drag)
			{
				xa += ((e.X - mx)) * 1.0f;
				ya += ((e.Y - my)) * 1.0f;
				mx = e.X;
				my = e.Y;
				engine.SetView(r, xa, ya);
			}
		}
		#endregion
	}
}
