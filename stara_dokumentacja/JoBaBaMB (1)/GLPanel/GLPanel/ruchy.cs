﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GLPanel
{
	public partial class ruchy : Form
	{
		Positions positions;
		Move move;
		GLengine engine;
		public ruchy(Positions p, Move m, GLengine engine, bool edit = false)
		{
			InitializeComponent();
			this.positions = p;
			this.move = m;
			this.engine = engine;
			foreach (Pos pos in positions.poses)
			{
				listBox1.Items.Add(pos);
			}
			if (edit)
			{
				textBox3.Text = m.nazwa;
				richTextBox1.Text = m.polecenie;
			}
		}

		//dodaj pozycje
		private void button1_Click(object sender, EventArgs e)
		{
			richTextBox1.Text += "Move(" + listBox1.SelectedItem.ToString() + ");\n";
		}

		private void button2_Click(object sender, EventArgs e)
		{
			richTextBox1.Text += "Krocz(" + textBox1.Text + ");\n";
		}

		private void button3_Click(object sender, EventArgs e)
		{
			richTextBox1.Text += "Delay(" + textBox2.Text + ");\n";
		}

		private void button4_Click(object sender, EventArgs e)
		{
			move.polecenie = richTextBox1.Text;
			move.nazwa = textBox3.Text;
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void button5_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			richTextBox1.Text += "Move(" + listBox1.SelectedItem.ToString() + ");\n";
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			Pos pos = (Pos)listBox1.SelectedItem;
			engine.SetAngle(pos.a1, pos.a2, pos.a3, pos.a4, pos.ch);
		}

		private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				richTextBox1.Text += "Move(" + listBox1.SelectedItem.ToString() + ");\n";
			}
		}

		private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
		{
			int line = e.Y / 13;
			if (line < richTextBox1.Lines.Length)
			{
				string cmd = richTextBox1.Lines[line];
				string naz = "";
				if (cmd.StartsWith("Move("))
				{
					if (cmd.Contains(","))
					{
						int count = cmd.IndexOf(",") - 5;
						if (!(count <= 0))
						{
							naz = cmd.Substring(5, count);
						}
					}
					else
					{
						int count = cmd.IndexOf(")") - 5;
						if (!(count <= 0))
						{
							naz = cmd.Substring(5, count);
						}
					}

					foreach (Pos pos in positions.poses)
					{
						if (pos.nazwa == naz)
						{
							engine.SetAngle(pos.a1, pos.a2, pos.a3, pos.a4, pos.ch);
							break;
						}
					}
				}
			}
		}
	}
}
