﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GLPanel
{
	public class Pos
	{
		public string nazwa;
		public int a1, a2, a3, a4;
		public int ch;

		public override string ToString()
		{
			return nazwa;
		}

	}

	public class Positions
	{
		public List<Pos> poses = new List<Pos>();


		public void Add(string nazwa, int a1, int a2, int a3, int a4, int ch)
		{
			Pos p = new Pos();
			p.a1 = a1;
			p.a2 = a2;
			p.a3 = a3;
			p.a4 = a4;
			p.ch = ch;
			p.nazwa = nazwa;
			poses.Add(p);
		}

		public void Load(string path = "poses.txt")
		{
			if (File.Exists(path))
			{
				string[] lines = File.ReadAllLines(path);
				foreach (string s in lines)
				{
					string[] ss = s.Split(',');
					Pos pos = new Pos();
					pos.nazwa = ss[0];
					pos.a1 = int.Parse(ss[1]);
					pos.a2 = int.Parse(ss[2]);
					pos.a3 = int.Parse(ss[3]);
					pos.a4 = int.Parse(ss[4]);
					pos.ch = int.Parse(ss[5]);
					poses.Add(pos);
				}
			}
		}

		public void Save(string path = "poses.txt")
		{
			string[] lines = new string[poses.Count];
			for (int i = 0; i < poses.Count; i++)
			{
				Pos pos = poses[i];
				lines[i] = pos.nazwa + "," + pos.a1.ToString() + "," + pos.a2.ToString() + "," + pos.a3.ToString() + "," + pos.a4.ToString()
					 + "," + pos.ch.ToString();

			}
			File.WriteAllLines(path, lines);
		}
	}
}
