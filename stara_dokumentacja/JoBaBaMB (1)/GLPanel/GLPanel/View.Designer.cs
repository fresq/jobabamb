﻿namespace GLPanel
{
	partial class View
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
			this.panel1 = new System.Windows.Forms.Panel();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.hScrollBar7 = new System.Windows.Forms.HScrollBar();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
			this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
			this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
			// 
			// textBox7
			// 
			resources.ApplyResources(this.textBox7, "textBox7");
			this.textBox7.Name = "textBox7";
			// 
			// hScrollBar7
			// 
			resources.ApplyResources(this.hScrollBar7, "hScrollBar7");
			this.hScrollBar7.Minimum = 10;
			this.hScrollBar7.Name = "hScrollBar7";
			this.hScrollBar7.Value = 80;
			this.hScrollBar7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar7_Scroll);
			// 
			// View
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ControlBox = false;
			this.Controls.Add(this.textBox7);
			this.Controls.Add(this.hScrollBar7);
			this.Controls.Add(this.panel1);
			this.Name = "View";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.HScrollBar hScrollBar7;
	}
}