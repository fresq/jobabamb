﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;

namespace GLPanel
{
	public partial class Form1 : Form
	{
		public GLengine engine = new GLengine();
		SerialPort port;
		bool drag = false;
		float mx, my;
		float r = 80, xa = 0, ya = 10.0f*3.14159265f/180.0f;
		Positions pos = new Positions();
		Movements movement = new Movements();

		ARM arm = new ARM(4);
		float krok = 3.0f;

		public Form1()
		{
			InitializeComponent();
			engine.SetView(r, xa, ya);
			numerServa.SelectedIndex = 0;
			//port = new SerialPort("COM11", 2400, Parity.None, 8, StopBits.Two);
			pos.Load();
			movement.Load();
			foreach (Pos p in pos.poses)
			{
				poses.Items.Add(p);
			}
			foreach (Move move in movement.moves)
			{
				moves.Items.Add(move);
			}
			Thread t = new Thread(new ThreadStart(() => { Application.Run(new View(engine)); }));
			t.Start();

			new Thread(new ThreadStart(delegate
			{
				engine.MainSurprise();
			})).Start();
			groupBox1.Enabled = true;
			groupBox2.Enabled = true;
			arm.Set(1, 9).Set(2, 9).Set(3, 9).Set(4, 9).Move();
		}

		//OpenGL window
		private void button1_Click(object sender, EventArgs e)
		{
			new Thread(new ThreadStart(delegate
				{
					engine.MainSurprise();
				})).Start();
			groupBox1.Enabled = true;
			groupBox2.Enabled = true;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			port = new SerialPort("COM" + comNumber.Text, 2400, Parity.None, 8, StopBits.Two);
			port.Handshake = Handshake.None;
			port.DataReceived += port_DataReceived;
			port.Open();
			MessageBox.Show("Connected!");
		}

		private delegate void SetAngleDelegate(int a1, int a2, int a3, int a4, int ch);

		private void SetAngle(int a1, int a2, int a3, int a4, int ch) 
		{
			numericUpDown1.ValueChanged -= numericUpDown1_ValueChanged;
			numericUpDown2.ValueChanged -= numericUpDown2_ValueChanged;
			numericUpDown3.ValueChanged -= numericUpDown3_ValueChanged;
			numericUpDown4.ValueChanged -= numericUpDown4_ValueChanged;
			numericUpDown5.ValueChanged -= numericUpDown5_ValueChanged;

			numericUpDown1.Value = a1;
			numericUpDown2.Value = a2;
			numericUpDown3.Value = a3;
			numericUpDown4.Value = a4;
			numericUpDown5.Value = ch;

			hScrollBar1.Value = a1;
			hScrollBar2.Value = a2;
			hScrollBar3.Value = a3;
			hScrollBar4.Value = a4;
			hScrollBar6.Value = ch;

			engine.SetAngle(a1, a2, a3, a4, ch);

			numericUpDown1.ValueChanged += numericUpDown1_ValueChanged;
			numericUpDown2.ValueChanged += numericUpDown2_ValueChanged;
			numericUpDown3.ValueChanged += numericUpDown3_ValueChanged;
			numericUpDown4.ValueChanged += numericUpDown4_ValueChanged;
			numericUpDown5.ValueChanged += numericUpDown5_ValueChanged;
		}

		void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int polecenie = port.ReadChar();
			char[] dane = new char[20];
			int i = 0;
			string s = "";
			while (port.BytesToRead > 0)
			{
				s += port.ReadChar().ToString() + ",";
				//dane[i] = (char)port.ReadChar();
				//i++;
			}
			MessageBox.Show(s);
			if (polecenie == 1)
			{
				this.BeginInvoke(new SetAngleDelegate(SetAngle), new object[] { dane[0]-100, dane[1]-100, dane[2]-100, dane[3]-100, dane[4]-100});
			}
		}

		#region View
		private void hScrollBar7_Scroll(object sender, ScrollEventArgs e)
		{
			textBox7.Text = hScrollBar7.Value.ToString();
			r = hScrollBar7.Value;
			engine.SetView(r,xa,ya);
		}

		private void panel1_MouseDown(object sender, MouseEventArgs e)
		{
			drag = true;
			mx = e.X;
			my = e.Y;
		}

		private void panel1_MouseUp(object sender, MouseEventArgs e)
		{
			drag = false;
		}

		private void panel1_MouseMove(object sender, MouseEventArgs e)
		{
			if (drag)
			{
				xa += ((e.X - mx)) * 1.0f;
				ya += ((e.Y - my)) * 1.0f;
				mx = e.X;
				my = e.Y;
				engine.SetView(r, xa, ya);
			}
		}
		#endregion

		private void SendAngle()
		{
			byte[] dane = new byte[20];
			dane[0] = 1;
			dane[1] = (byte)Math.Abs(numericUpDown1.Value);
			dane[3] = (byte)Math.Abs(numericUpDown2.Value);
			dane[5] = (byte)Math.Abs(numericUpDown3.Value);
			dane[7] = (byte)Math.Abs(numericUpDown4.Value);
			dane[9] = (byte)Math.Abs(numericUpDown5.Value);
			dane[2] = (byte)((numericUpDown1.Value < 0) ? 1 : 0);
			dane[4] = (byte)((numericUpDown2.Value < 0) ? 1 : 0);
			dane[6] = (byte)((numericUpDown3.Value < 0) ? 1 : 0);
			dane[8] = (byte)((numericUpDown4.Value < 0) ? 1 : 0);
			dane[10] = (byte)((numericUpDown5.Value < 0) ? 1 : 0);
			dane[11] = 126;
			if(port.IsOpen) port.Write(dane, 0, 12);
			//while (port.BytesToWrite > 0) ;
			Thread.Sleep(200);
		}

		private void SendAngle2(int ms = 500)
		{
			int tmp = ms;
			byte[] dane = new byte[20];
			dane[0] = 1;
			dane[1] = (byte)Math.Abs(numericUpDown1.Value);
			dane[3] = (byte)Math.Abs(numericUpDown2.Value);
			dane[5] = (byte)Math.Abs(numericUpDown3.Value);
			dane[7] = (byte)Math.Abs(numericUpDown4.Value);
			dane[9] = (byte)Math.Abs(numericUpDown5.Value);
			dane[2] = (byte)((numericUpDown1.Value < 0) ? 1 : 0);
			dane[4] = (byte)((numericUpDown2.Value < 0) ? 1 : 0);
			dane[6] = (byte)((numericUpDown3.Value < 0) ? 1 : 0);
			dane[8] = (byte)((numericUpDown4.Value < 0) ? 1 : 0);
			dane[10] = (byte)((numericUpDown5.Value < 0) ? 1 : 0);
			dane[11] = (byte)(ms % 10); ms /= 10;
			dane[12] = (byte)(ms % 10); ms /= 10;
			dane[13] = (byte)(ms % 10); ms /= 10;
			dane[14] = (byte)(ms % 10); ms /= 10;
			dane[15] = 126;
			if (port.IsOpen)
			{
				port.Write(dane, 0, 16);
				Thread.Sleep((int)(2.4 * tmp));
			}
		}

		private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
		{
			numericUpDown1.Value = hScrollBar1.Value;
		}

		private void hScrollBar2_Scroll(object sender, ScrollEventArgs e)
		{
			numericUpDown2.Value = hScrollBar2.Value;
		}

		private void hScrollBar3_Scroll(object sender, ScrollEventArgs e)
		{
			numericUpDown3.Value = hScrollBar3.Value;
		}

		private void hScrollBar4_Scroll(object sender, ScrollEventArgs e)
		{
			numericUpDown4.Value = hScrollBar4.Value;
		}

		private void hScrollBar6_Scroll(object sender, ScrollEventArgs e)
		{
			numericUpDown5.Value = hScrollBar6.Value;
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			hScrollBar1.Value = (int)numericUpDown1.Value;
			engine.SetA(0, hScrollBar1.Value);
			if (realTime.Checked) SendAngle();
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
			hScrollBar2.Value = (int)numericUpDown2.Value;
			engine.SetA(1, hScrollBar2.Value);
			if (realTime.Checked) SendAngle();
		}

		private void numericUpDown3_ValueChanged(object sender, EventArgs e)
		{
			hScrollBar3.Value = (int)numericUpDown3.Value;
			engine.SetA(2, hScrollBar3.Value);
			if (realTime.Checked) SendAngle();
		}

		private void numericUpDown4_ValueChanged(object sender, EventArgs e)
		{
			hScrollBar4.Value = (int)numericUpDown4.Value;
			engine.SetA(3, hScrollBar4.Value);
			if (realTime.Checked) SendAngle();
		}

		private void numericUpDown5_ValueChanged(object sender, EventArgs e)
		{
			hScrollBar6.Value = (int)numericUpDown5.Value;
			engine.SetCh(hScrollBar6.Value);
			if(realTime.Checked) SendAngle();
		}

		private void KrokowyMove(int i)
		{
			byte[] dane = new byte[10];
			dane[0] = 3;
			dane[1] = (byte)Math.Abs(i);
			dane[2] = (byte)(i < 0 ? 1 : 0);
			dane[3] = 126;
			if (port.IsOpen) port.Write(dane, 0, 4);
			Thread.Sleep(Math.Abs(i)*200);
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			engine.ModK(5);
			KrokowyMove(1);
		}

		private void button2_Click_1(object sender, EventArgs e)
		{
			engine.ModK(-5);
			KrokowyMove(-1);
		}

		private void TextWypelnienie_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				int us = int.Parse(TextWypelnienie.Text);
				byte[] dane = new byte[10];
				dane[0] = 5;
				dane[1] = (byte)numerServa.SelectedIndex;
				dane[2] = (byte)(us % 10); us /= 10;
				dane[3] = (byte)(us % 10); us /= 10;
				dane[4] = (byte)(us % 10); us /= 10;
				dane[5] = (byte)(us % 10); us /= 10;
				dane[6] = 126;
				if (port.IsOpen) port.Write(dane, 0, 7);
				Thread.Sleep(200);
			}
		}

		private void zatwiedzCewki_Click(object sender, EventArgs e)
		{
			byte[] dane = new byte[6];
			dane[0] = 4;
			dane[1] = (byte)(c11.Checked ? 1 : 0);
			dane[2] = (byte)(c12.Checked ? 1 : 0);
			dane[3] = (byte)(c21.Checked ? 1 : 0);
			dane[4] = (byte)(c22.Checked ? 1 : 0);
			dane[5] = 126;
			if (port.IsOpen) port.Write(dane, 0, 6);
			Thread.Sleep(200);
		}

		private void push_Click(object sender, EventArgs e)
		{
			SendAngle2();
		}

		private void save_Click(object sender, EventArgs e)
		{
			bool b = false;
			foreach (Pos p in pos.poses)
			{
				if (p.nazwa == posName.Text)
				{
					p.a1 = (int)numericUpDown1.Value;
					p.a2 = (int)numericUpDown2.Value;
					p.a3 = (int)numericUpDown3.Value;
					p.a4 = (int)numericUpDown4.Value;
					p.ch = (int)numericUpDown5.Value;
					b = true;
					break;
				}
			}
			if (!b)
			{
				pos.Add(posName.Text, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value, (int)numericUpDown5.Value);
				poses.Items.Add(pos.poses[pos.poses.Count - 1].nazwa);
			}
		}

		private void ustaw_Click(object sender, EventArgs e)
		{
			bool tmp = realTime.Checked;
			realTime.Checked = false;
			SetAngle(pos.poses[poses.SelectedIndex].a1, pos.poses[poses.SelectedIndex].a2, pos.poses[poses.SelectedIndex].a3, pos.poses[poses.SelectedIndex].a4, pos.poses[poses.SelectedIndex].ch);
			//hScrollBar1.Value = pos.poses[poses.SelectedIndex].a1;
			//hScrollBar2.Value = pos.poses[poses.SelectedIndex].a2;
			//hScrollBar3.Value = pos.poses[poses.SelectedIndex].a3;
			//hScrollBar4.Value = pos.poses[poses.SelectedIndex].a4;
			//hScrollBar6.Value = pos.poses[poses.SelectedIndex].ch;
			posName.Text = pos.poses[poses.SelectedIndex].nazwa;
			realTime.Checked = tmp;
			SendAngle2();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			pos.Save();
			movement.Save();
		}

		private void rb_Click(object sender, EventArgs e)
		{
			movementManager mm = new movementManager(movement, pos, engine);
			mm.Show();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			positionsManager pm = new positionsManager(pos, engine);
			pm.Show();
		}

		private void ruszaj_Click(object sender, EventArgs e)
		{
			Move m = (Move)moves.SelectedItem;
			string[] lines = m.polecenie.Split('\n');
			string log = "";
			for(int i = 0; i < lines.Length; i++)
			{
				string cmd = lines[i];
				if (cmd.Length == 0) continue;
				if (cmd[cmd.Length - 1] != ';')
				{
					log += "Brak srednika: linia " + i.ToString() + "\r\n";
					continue;
				}
				if (cmd.StartsWith("Zamknij()"))
				{
					SetAngle((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value, -80);
					SendAngle2();
				}
				else if (cmd.StartsWith("Otworz()"))
				{
					SetAngle((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value, -30);
					SendAngle2();
				}
				else if (cmd.StartsWith("Nalej("))
				{
					int count = cmd.IndexOf(");") - 6;
					string[] param = cmd.Substring(6, count).Split(',');
					WyslijWypelnienie(int.Parse(param[0]), int.Parse(param[1]));

				}
				else if (cmd.StartsWith("Zeruj()"))
				{
					RozkazZerowania();
				}
				else if (cmd.StartsWith("//"))
				{
					continue;
				}
				else if (cmd.StartsWith("Delay("))
				{
					int ms;
					int count = cmd.IndexOf(");") - 6;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(6, count), out ms))
					{
						log += "Bledna ilosc milisekund: linia " + i.ToString() + "\r\n";
						continue;
					}
					int tmp = ms;
					byte[] dane = new byte[10];
					dane[0] = 6;
					dane[1] = (byte)(ms % 10); ms /= 10;
					dane[2] = (byte)(ms % 10); ms /= 10;
					dane[3] = (byte)(ms % 10); ms /= 10;
					dane[4] = (byte)(ms % 10); ms /= 10;
					dane[5] = 126;
					if (port.IsOpen) port.Write(dane, 0, 6);
					Thread.Sleep(tmp + 200);

				}
				else if (cmd.StartsWith("Stop("))
				{
					int ms;
					int count = cmd.IndexOf(");") - 5;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(5, count), out ms))
					{
						log += "Bledna ilosc milisekund: linia " + i.ToString() + "\r\n";
						continue;
					}
					Thread.Sleep(ms);

				}
				else if (cmd.StartsWith("Krocz("))
				{
					int kroki;
					int count = cmd.IndexOf(");") - 6;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(6, count), out kroki))
					{
						log += "Bledna ilosc kroków: linia " + i.ToString() + "\r\n";
						continue;
					}
					KrokowyMove(kroki);
					engine.ModK(kroki*5);
					Thread.Sleep(120 * Math.Abs(kroki));
				}
				else if (cmd.StartsWith("Move("))
				{
					string naz = "";
					int ms = 500;
					if (cmd.Contains(","))
					{
						int count = cmd.IndexOf(",") - 5;
						if (count <= 0)
						{
							log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
							continue;
						}
						naz = cmd.Substring(5, count);
						count = cmd.IndexOf(")") - cmd.IndexOf(",") - 1;
						if (!int.TryParse(cmd.Substring(cmd.IndexOf(",") + 1, count), out ms))
						{
							log += "Bledna ilosc ms: linia " + i.ToString() + "\r\n";
							continue;
						}
					}
					else
					{
						int count = cmd.IndexOf(")") - 5;
						if (count <= 0)
						{
							log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
							continue;
						}
						naz = cmd.Substring(5, count);
					}
					bool finded = false;
					foreach (Pos p in pos.poses)
					{
						if (p.nazwa == naz)
						{
							finded = true;
							bool tmp = realTime.Checked;
							realTime.Checked = false;
							SetAngle(p.a1, p.a2, p.a3, p.a4, (int)numericUpDown5.Value);
							realTime.Checked = tmp;
							SendAngle2(ms);
							break;
						}
					}
					if (!finded)
					{
						log += "Nierozpoznan pozycja: linia " + i.ToString() + "\r\n";
						continue;
					}
				}
			}
			if (log != "")
			{
				MessageBox.Show(log);
			}
		}

		private void Form1_Shown(object sender, EventArgs e)
		{
			this.BringToFront();
			this.TopMost = true;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			this.TopMost = false;
			timer1.Stop();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			arm.Set(1, 9, (float)numericUpDown1.Value).Set(2, 9, (float)numericUpDown2.Value).Set(3, 9, (float)numericUpDown3.Value).Set(4, 9, (float)numericUpDown4.Value).Move();

			arm.MovXY(krok, 0);

			bool tmp = realTime.Checked;
			realTime.Checked = false;
			SetAngle((int)arm.Joints[0].Angle, (int)arm.Joints[1].Angle, (int)arm.Joints[2].Angle, (int)arm.Joints[3].Angle, (int)numericUpDown5.Value);
			realTime.Checked = tmp;
			SendAngle2(500);
		}

		private void button7_Click(object sender, EventArgs e)
		{
			arm.Set(1, 9, (float)numericUpDown1.Value).Set(2, 9, (float)numericUpDown2.Value).Set(3, 9, (float)numericUpDown3.Value).Set(4, 9, (float)numericUpDown4.Value).Move();

			arm.MovXY(-krok, 0);

			bool tmp = realTime.Checked;
			realTime.Checked = false;
			SetAngle((int)arm.Joints[0].Angle, (int)arm.Joints[1].Angle, (int)arm.Joints[2].Angle, (int)arm.Joints[3].Angle, (int)numericUpDown5.Value);
			realTime.Checked = tmp;
			SendAngle2(500);
		}

		private void button6_Click(object sender, EventArgs e)
		{
			arm.Set(1, 9, (float)numericUpDown1.Value).Set(2, 9, (float)numericUpDown2.Value).Set(3, 9, (float)numericUpDown3.Value).Set(4, 9, (float)numericUpDown4.Value).Move();

			arm.MovXY(0, krok);

			bool tmp = realTime.Checked;
			realTime.Checked = false;
			SetAngle((int)arm.Joints[0].Angle, (int)arm.Joints[1].Angle, (int)arm.Joints[2].Angle, (int)arm.Joints[3].Angle, (int)numericUpDown5.Value);
			realTime.Checked = tmp;
			SendAngle2(500);
		}

		private void button8_Click(object sender, EventArgs e)
		{
			arm.Set(1, 9, (float)numericUpDown1.Value).Set(2, 9, (float)numericUpDown2.Value).Set(3, 9, (float)numericUpDown3.Value).Set(4, 9, (float)numericUpDown4.Value).Move();

			arm.MovXY(0, -krok);

			bool tmp = realTime.Checked;
			realTime.Checked = false;
			SetAngle((int)arm.Joints[0].Angle, (int)arm.Joints[1].Angle, (int)arm.Joints[2].Angle, (int)arm.Joints[3].Angle, (int)numericUpDown5.Value);
			realTime.Checked = tmp;
			SendAngle2(500);
		}

		private void numericUpDown6_ValueChanged(object sender, EventArgs e)
		{
			krok = (float)numericUpDown6.Value;
		}

		private void WyslijWypelnienie(int w, int ms)
		{
			int tmp = ms;
			byte[] dane = new byte[20];
			dane[0] = 7;
			dane[1] = (byte)(w % 10); w /= 10;
			dane[2] = (byte)(w);
			dane[3] = (byte)(ms % 10); ms /= 10;
			dane[4] = (byte)(ms % 10); ms /= 10;
			dane[5] = (byte)(ms);
			dane[6] = 126;
			if (port.IsOpen)
			{
				port.Write(dane, 0, 7);
				Thread.Sleep((int)(tmp*1.5));
			}
		}

		private void pompkaJazda_Click(object sender, EventArgs e)
		{
			int ms = int.Parse(wypelnienieCzas.Text);
			WyslijWypelnienie((int)wypelnieniPompka.Value, ms);
		}

		private void button10_Click(object sender, EventArgs e)
		{
			int ms = int.Parse(wypelnienieCzas.Text);
			WyslijWypelnienie(0, ms);
		}

		private void RozkazZerowania()
		{
			byte[] dane = new byte[20];
			dane[0] = 8;
			dane[1] = 126;
			if (port.IsOpen)
			{
				port.Write(dane, 0, 2);
				Thread.Sleep(2000);
			}
		}

		private void button9_Click(object sender, EventArgs e)
		{
			RozkazZerowania();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			pos.Save("autoposes.txt");
			movement.Save("automoves.txt");
		}

		private void execute_Click(object sender, EventArgs e)
		{
			string[] lines = executeText.Text.Split('\n');
			string log = "";
			for (int i = 0; i < lines.Length; i++)
			{
				string cmd = lines[i];
				if (cmd.Length == 0) continue;
				if (cmd[cmd.Length - 1] != ';')
				{
					log += "Brak srednika: linia " + i.ToString() + "\r\n";
					continue;
				}
				if (cmd.StartsWith("Zamknij()"))
				{
					SetAngle((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value, -80);
					SendAngle2();
				}
				else if (cmd.StartsWith("Otworz()"))
				{
					SetAngle((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value, -30);
					SendAngle2();
				}
				else if (cmd.StartsWith("Nalej("))
				{
					int count = cmd.IndexOf(");") - 6;
					string[] param = cmd.Substring(6, count).Split(',');
					WyslijWypelnienie(int.Parse(param[0]), int.Parse(param[1]));

				}
				else if (cmd.StartsWith("Zeruj()"))
				{
					RozkazZerowania();
				}
				else if (cmd.StartsWith("//"))
				{
					continue;
				}
				else if (cmd.StartsWith("Delay("))
				{
					int ms;
					int count = cmd.IndexOf(");") - 6;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(6, count), out ms))
					{
						log += "Bledna ilosc milisekund: linia " + i.ToString() + "\r\n";
						continue;
					}
					int tmp = ms;
					byte[] dane = new byte[10];
					dane[0] = 6;
					dane[1] = (byte)(ms % 10); ms /= 10;
					dane[2] = (byte)(ms % 10); ms /= 10;
					dane[3] = (byte)(ms % 10); ms /= 10;
					dane[4] = (byte)(ms % 10); ms /= 10;
					dane[5] = 126;
					if (port.IsOpen) port.Write(dane, 0, 6);
					Thread.Sleep(tmp + 200);

				}
				else if (cmd.StartsWith("Stop("))
				{
					int ms;
					int count = cmd.IndexOf(");") - 5;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(5, count), out ms))
					{
						log += "Bledna ilosc milisekund: linia " + i.ToString() + "\r\n";
						continue;
					}
					Thread.Sleep(ms);

				}
				else if (cmd.StartsWith("Krocz("))
				{
					int kroki;
					int count = cmd.IndexOf(");") - 6;
					if (count <= 0)
					{
						log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
						continue;
					}
					if (!int.TryParse(cmd.Substring(6, count), out kroki))
					{
						log += "Bledna ilosc kroków: linia " + i.ToString() + "\r\n";
						continue;
					}
					engine.ModK(kroki * 5);
					KrokowyMove(kroki);
					Thread.Sleep(120 * Math.Abs(kroki));
				}
				else if (cmd.StartsWith("Move("))
				{
					string naz = "";
					int ms = 500;
					if (cmd.Contains(","))
					{
						int count = cmd.IndexOf(",") - 5;
						if (count <= 0)
						{
							log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
							continue;
						}
						naz = cmd.Substring(5, count);
						count = cmd.IndexOf(")") - cmd.IndexOf(",") - 1;
						if (!int.TryParse(cmd.Substring(cmd.IndexOf(",") + 1, count), out ms))
						{
							log += "Bledna ilosc ms: linia " + i.ToString() + "\r\n";
							continue;
						}
					}
					else
					{
						int count = cmd.IndexOf(")") - 5;
						if (count <= 0)
						{
							log += "Niepoprawne polecenie: linia " + i.ToString() + "\r\n";
							continue;
						}
						naz = cmd.Substring(5, count);
					}
					bool finded = false;
					foreach (Pos p in pos.poses)
					{
						if (p.nazwa == naz)
						{
							finded = true;
							bool tmp = realTime.Checked;
							realTime.Checked = false;
							SetAngle(p.a1, p.a2, p.a3, p.a4, (int)numericUpDown5.Value);
							realTime.Checked = tmp;
							SendAngle2(ms);
							break;
						}
					}
					if (!finded)
					{
						log += "Nierozpoznan pozycja: linia " + i.ToString() + "\r\n";
						continue;
					}
				}
			}
			if (log != "")
			{
				MessageBox.Show(log);
			}
		}

		char literka = 'a';

		private void button11_Click(object sender, EventArgs e)
		{
			char[] c = new char[1];
			c[0] = (char)int.Parse(textBox1.Text);
			
			port.Write(c,0,1);
			Thread.Sleep(100);
		}




		



	}
}
