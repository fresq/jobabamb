﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GLPanel
{
	public class Move
	{
		public string nazwa;
		public string polecenie;

		public override string ToString()
		{
			return nazwa;
		}
	}

	public class Movements
	{
		public List<Move> moves = new List<Move>();

		public void Save(string path = "moves.txt")
		{
			string line = "";
			foreach (Move m in moves)
			{
				line += m.nazwa + "\0" + m.polecenie + "\0";
			}
			File.WriteAllText(path, line);
		}

		public void Load(string path = "moves.txt")
		{
			string line = File.ReadAllText(path);
			string[] lines = line.Split('\0');
			for (int i = 0; i < lines.Length - 2; i+= 2)
			{
				Move m = new Move();
				m.nazwa = lines[i];
				m.polecenie = lines[i + 1];
				moves.Add(m);
			}
		}

	}
}
