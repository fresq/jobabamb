﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GLPanel
{
	public partial class positionsManager : Form
	{
		Positions positions;
		GLengine engine;

		public positionsManager(Positions p, GLengine engine)
		{
			InitializeComponent();
			this.positions = p;
			this.engine = engine;
			Ref();
		}

		void Ref()
		{
			listBox1.Items.Clear();
			foreach (Pos pos in positions.poses)
			{
				listBox1.Items.Add(pos);
			}
		}

		//edycja
		private void button1_Click(object sender, EventArgs e)
		{
			PositionForm p = new PositionForm((Pos)listBox1.SelectedItem);
			p.Show();
			Ref();
		}

		private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				PositionForm p = new PositionForm((Pos)listBox1.SelectedItem);
				p.Show();
				Ref();
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			positions.poses.Remove((Pos)listBox1.SelectedItem);
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			Pos pos = (Pos)listBox1.SelectedItem;
			engine.SetAngle(pos.a1, pos.a2, pos.a3, pos.a4, pos.ch);
		}

		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			PositionForm p = new PositionForm((Pos)listBox1.SelectedItem);
			p.Show();
			Ref();
		}
	}
}
