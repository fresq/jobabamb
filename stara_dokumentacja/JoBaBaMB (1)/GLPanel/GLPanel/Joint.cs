﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GLPanel
{
	public class Joint
	{
		private Vec2f loc;
		private float len,angle,arg;

		public Joint(Vec2f loc)
		{
			this.loc = loc;
			this.len = 0;
			this.arg = 0;
			this.angle = 0;
		}
		public Joint(float len, float angle=0, float x=0, float y=0)
		{
			this.loc = new Vec2f(x, y);
			this.len = len;
			this.angle = angle;
			this.arg = angle;
		}

		public static Joint operator^(Joint joint1, Joint joint2)
		{
			joint2.arg = joint1.Arg + joint2.Angle;
			joint2.Loc.x = joint1.Loc.x + joint1.Len * (float)Math.Cos(joint1.Arg * 3.141592f/180);
			joint2.Loc.y = joint1.Loc.y + joint1.Len * (float)Math.Sin(joint1.Arg * 3.141592f/180);
			return joint2;
		}

		public float Angle { get{return angle;} set{angle = value;}}
		public float Arg  { get{return arg; } set{arg = value;}}
		public float Len  { get{return len; } set{len = value;}}
		public Vec2f Loc { get { return loc; } set { loc = value; } }


		//Joint Angle(float f) { angle = f; return this; }
		//Joint Arg(float f) { arg = f; return this; }
		//Joint Len(float f) { len = f; return this; }
		//Joint Loc(Vec2f v) { loc = v; return this; }
		//Joint Loc(float x, float y) { loc = new Vec2f(x,y); return this; }
	}
}
