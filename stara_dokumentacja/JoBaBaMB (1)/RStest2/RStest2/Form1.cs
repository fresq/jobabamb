﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace RStest2
{
	public partial class Form1 : Form
	{
		SerialPort _serialPort;

		private delegate void SetTextDeleg(string text);

		private void si_DataReceived(string data) { richTextBox1.Text += data; }

		public Form1()
		{
			InitializeComponent();
			_serialPort = new SerialPort("COM4", 2400, Parity.None, 8, StopBits.Two);
			_serialPort.Handshake = Handshake.None;
			_serialPort.DataReceived += _serialPort_DataReceived;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				if (!(_serialPort.IsOpen))
					_serialPort.Open();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error opening to serial port :: " + ex.Message, "Error!");
			}
		}

		void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			this.BeginInvoke(new SetTextDeleg(si_DataReceived), new object[] { "Dane odebrane: " });
			string data = ((char)_serialPort.ReadChar()).ToString();
			// Invokes the delegate on the UI thread, and sends the data that was received to the invoked method.
			// ---- The "si_DataReceived" method will be executed on the UI thread which allows populating of the textbox.
			this.BeginInvoke(new SetTextDeleg(si_DataReceived), new object[] { data + " \n" });
		}

		private void button2_Click(object sender, EventArgs e)
		{
			_serialPort.WriteLine(textBox1.Text);
		}
	}
}
