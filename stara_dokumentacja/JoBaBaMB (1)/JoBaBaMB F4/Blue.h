#pragma once

/**
 * \file Plik zawiera funkcje odpowiadające za komunikację z telefonem
 */

#include "Queue.h"
#include "SimpleKinematic.h"
#include "MotorControl.h"
#include "Pompka.h"
#include "ReversKinematic.h"
#include "hd44780.h"
#include "SteperControl.h"
#include "TimeControl.h"
#include "buzzer.h"

#define RIGHT_PAD 0x7e //126
#define GYRO 0x7d //125
#define POMPKA 0x7c
#define PODSTAWA 0x7b
#define S1 0x7a
#define S2 0x79
#define S3 0x78
#define S4 0x77
#define CHW_OBR 0x76
#define CHW 0x75
#define LEFT_PAD 0x74
#define VBAR 0x73
#define DEFAULT 0x72

#define LCD_Clear LCDClear
#define LCD_GoTo LCDXY
#define LCD_WriteText LCDOutString
#define LCD_WriteInt(x) LCDOutint32(x,6)

#define ABS(x) (((x < 0) ? (-x) : (x)))


void USART2_IRQHandler(void)
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		qAdd(&droidQ, USART_ReceiveData(USART2));
	}
}

int8_t GetNextData()
{
	while(qEmpty(&droidQ)) {_delay_ms(10);}
	return qGet(&droidQ);
}


void VBar(int v)
{

}

void RightPad(int x, int y)
{
	float l = 0;
	float r = 0;
	l += (-y / 100.0f) * 0.8f;
	r += (-y / 100.0f) * 0.8f;
	//int znak = (y < 0) ? -1 : 1;

	if(y < 38)
	{
		l += (x / 100.0f) * 0.5f;
		r -= (x / 100.0f) * 0.5f;
	}
	else
	{
		l -= (x / 100.0f) * 0.5f;
		r += (x / 100.0f) * 0.5f;
	}
	if(l <= -1) l = -0.99;
	if(l >= 1) l = 0.99;
	if(r <= -1) r = -0.99;
	if(r >= 1) r = 0.99;
	LCD_Clear();
	LCD_WriteText("RP: ");
	LCD_WriteInt(l*100);
	LCD_WriteText(", ");
	LCD_WriteInt(r*100);
	JazdaP(l, r);
}

void LeftPad(int x, int y)
{
	//float xx = x / 33.333f;
	//float yy = y / 3.333f;
	//LCD_Clear();
	//LCD_WriteText("LP: ");
	//LCD_WriteInt(xx*100);
	//LCD_WriteText(", ");
	//LCD_WriteInt(yy*100);
	//rkMoveArm(a, xx, yy);
}

void Gyro(int x, int y, int z)
{
	LCD_Clear();
	LCD_WriteText("Zyro: ");
	LCD_WriteInt(x);
	LCD_WriteText(", ");
	LCD_WriteInt(y);
	LCD_WriteText(", ");
	LCD_WriteInt(z);
}

void DelayMs(int ms)
{
	for(int i = 0; i < ms; i++)
		_delay_ms(1);
}

void Delay100Us(int us)
{
	for(int i = 0; i < us; i++)
		_delay_us(100);
}

void BluetoothComunication(arm* a)
{
	droidQ.head = 0;
	droidQ.tail = 0;
	int baseCount = 0;
	int baseDir = 0;
	int baseMax = 50;

	LCD_Clear();
	LCD_WriteText("Blue1!");
	KrokowyStop();

	while(1)
	{

		if(!qEmpty(&droidQ))
		{

			int8_t data = qGet(&droidQ);
			if(data == 112)
			{
				GPIO_ToggleBits(GPIOD, GPIO_Pin_12);
				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, 111);
				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[0].ang);
				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[1].ang);
				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[2].ang);
				while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); USART_SendData(USART2, (uint8_t)a->tab[3].ang);
				LCD_WriteText(" 112 ");
			}
			else
			if(data == RIGHT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				RightPad(x, y);
				LCD_WriteText(" Right Pad ");
			}
			else if(data == DEFAULT)
			{
				SetSlowAngle(a, 0, 30, 30, 30, 0, 0, 500);
				LCD_WriteText(" Default ");
			}
			else if(data == LEFT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				float yy = y / 33.333f;
				LCD_Clear();

				BuzzerSet();
				if(ABS(yy)>0.2)
					rkMoveArm(a, yy, 0);
				BuzzerReset();

				baseDir = (x < 0) ? 0 : 1;
				baseMax = 50 - abs(x / 2);


				steper_speed = baseMax;
				if(ABS(x) < 15) steper_speed = 0;
				if(ABS(x) == 50) steper_speed = 1;

				steper_dir = baseDir;


				LCD_WriteText("LP: ");
				LCD_WriteInt(x);
				LCD_WriteText(", ");
				LCD_WriteInt(yy*100);
			}
			else if(data == VBAR)
			{
				int8_t h = GetNextData();
				float step_down = h / 16.666f;
				int res = rkMoveArm(a, 0.0f, step_down);

				LCD_Clear();
				LCD_WriteText("rkRes: ");
				LCD_WriteInt(ABS(res));
			}
			else if(data == GYRO)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				int8_t z = GetNextData();
				Gyro(x, y, z);
				LCD_WriteText(" GYRO ");
			}
			else if(data == S1)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 0, angle,300);
				LCD_WriteText(" S1 ");
			}
			else if(data == S2)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 1, angle,300);
				LCD_WriteText(" S2 ");
			}
			else if(data == S3)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 2, angle,300);
				LCD_WriteText(" S3 ");
			}
			else if(data == S4)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 3, angle,300);
				LCD_WriteText(" S4 ");
			}
			else if(data == CHW)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 5, angle,300);
				LCD_WriteText(" chwytak ");
			}
			else if(data == CHW_OBR)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 4, angle,300);
				LCD_WriteText(" chwobr ");
			}
			else if(data == POMPKA)
			{
				int8_t v = GetNextData();
				PompkaWypelnienieP( v / 100.0f);
				LCD_Clear();
				LCD_WriteText("Pompka: ");
				LCD_WriteInt(v);
				LCD_WriteText("%");
			}
			else if(data == PODSTAWA)
			{
				int8_t v = GetNextData();
				baseDir = (v < 0) ? 0 : 1;
				baseMax = 50 - ABS(v);
				steper_speed = baseMax;
				if(v == 0) steper_speed = 0;
				if(ABS(v) == 50) steper_speed = 1;
				steper_dir = baseDir;
				LCD_Clear();
				LCD_WriteText("ob: ");
				LCD_WriteInt(baseMax);
				//LCD_WriteText(", ");
				//LCD_WriteInt(baseDir);
			}
		}
		//if(baseMax != 50) baseCount++;
		//if(baseCount >= baseMax)
		//{
			//Krocz(1, baseDir);
			//MikroKrok2(baseDir);
			//baseCount = 0;
			//LCD_WriteText(".");
		//}
		_delay_ms(10);
	}
}
