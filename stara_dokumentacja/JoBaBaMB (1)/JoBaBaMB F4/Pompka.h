#pragma once


#define POMPKA_TIM TIM3
#define POMPKA_GPIO GPIOB
#define POMPKA_PIN GPIO_Pin_1
#define POMPKA_CCR CCR4
#define POMPKA_MAX_VALUE 10000


void POMPKA_Config(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = POMPKA_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(POMPKA_GPIO, &GPIO_InitStructure);

	TIM_TimeBaseInitTypeDef  TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 10000;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 335;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(POMPKA_TIM, &TIM_TimeBaseInitStructure);

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 0;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	outputChannelInit.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outputChannelInit.TIM_OCIdleState = TIM_OCIdleState_Set;
	outputChannelInit.TIM_OCNIdleState = TIM_OCIdleState_Reset;

	TIM_OC4Init(POMPKA_TIM, &outputChannelInit);
	TIM_OC4PreloadConfig(POMPKA_TIM, TIM_OCPreload_Enable);

	GPIO_PinAFConfig(POMPKA_GPIO, GPIO_PinSource1, GPIO_AF_TIM3);

	TIM_Cmd(POMPKA_TIM, ENABLE);
	TIM_CtrlPWMOutputs(POMPKA_TIM, ENABLE);
}


void PompkaWypelnienieP(float p)
{
	POMPKA_TIM->POMPKA_CCR = (int)(p * POMPKA_MAX_VALUE);
}

void PompkaWypelnienie(uint16_t w)
{
	POMPKA_TIM->POMPKA_CCR = w;
}

void PompkaWypelnienieMod(uint16_t w)
{
	POMPKA_TIM->POMPKA_CCR += w;
}
