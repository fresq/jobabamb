#pragma once

// Servo times (this is Futaba timing).
#define SERVO0_MIN  1010// microseconds
#define SERVO0_MAX   1710 // microseconds
#define SERVO0_MID   1310
#define SERVO0_ANGLESPAN 90.0f

#define SERVO1_MIN    700 // microseconds
#define SERVO1_MAX   2540 // microseconds
#define SERVO1_MID   1610
#define SERVO1_ANGLESPAN 180.0f

#define SERVO2_MIN    590 // microseconds
#define SERVO2_MAX   2540 // microseconds
#define SERVO2_MID   1580
#define SERVO2_ANGLESPAN 180.0f

#define SERVO3_MIN    700 // microseconds
#define SERVO3_MAX   2710 // microseconds
#define SERVO3_MID   1670
#define SERVO3_ANGLESPAN 180.0f

#define SERVO4_MIN    660 // microseconds
#define SERVO4_MAX   2700 // microseconds
#define SERVO4_MID   1700
#define SERVO4_ANGLESPAN 180.0f


#define SERVO5_MIN    700 // microseconds
#define SERVO5_MAX   1840 // microseconds
#define SERVO5_MID   1140
#define SERVO5_ANGLESPAN 180.0f


#define SERVOTIM1 TIM3
#define SERVOCCR1 CCR1
#define SERVOTIM2 TIM3
#define SERVOCCR2 CCR2
#define SERVOTIM3 TIM3
#define SERVOCCR3 CCR3
#define SERVOTIM4 TIM1
#define SERVOCCR4 CCR2
#define SERVOTIM5 TIM12
#define SERVOCCR5 CCR1
#define SERVOTIM6 TIM2
#define SERVOCCR6 CCR4
#define SERVOTIM7 TIM2
#define SERVOCCR7 CCR3



void servoSet(uint8_t servo, float time /* microseconds */)
{
	switch (servo) {
		case 0:	SERVOTIM1->SERVOCCR1 = 0.5*time; break;
		case 1:	SERVOTIM2->SERVOCCR2 = 0.5*time; break;
		case 2:	SERVOTIM3->SERVOCCR3 = 0.5*time; break;
		case 3:	SERVOTIM4->SERVOCCR4 = 0.5*time; break;
		case 4:	SERVOTIM5->SERVOCCR5 = 0.5*time; break;
		case 5:	SERVOTIM6->SERVOCCR6 = 0.5*time; break;
		case 6:	SERVOTIM7->SERVOCCR7 = 0.5*time; break;
		default:
			break;
	}
}
void servoAngle0(float angle)
{
	float dz = (SERVO0_MAX - SERVO0_MIN) / SERVO0_ANGLESPAN;

	servoSet(0, SERVO0_MID +(angle * dz));
}

void servoAngle1(float angle)
{
	float dz = (SERVO1_MAX - SERVO1_MIN) / SERVO1_ANGLESPAN;

	servoSet(1, SERVO1_MID + (angle * dz));
}

void servoAngle2(float angle)
{
	float dz = (SERVO2_MAX - SERVO2_MIN) / SERVO2_ANGLESPAN;

	servoSet(2, SERVO2_MID + (angle * dz));
}

void servoAngle3(float angle)
{
	float dz = (SERVO3_MAX - SERVO3_MIN) / SERVO3_ANGLESPAN;
	servoSet(3, SERVO3_MID + (angle * dz));
}

void servoAngle4(float angle)
{
	float dz = (SERVO4_MAX - SERVO4_MIN) / SERVO4_ANGLESPAN;
	servoSet(4, SERVO4_MID + (angle * dz));
}

void servoAngle5(float angle)
{
	float dz = (SERVO5_MAX - SERVO5_MIN) / SERVO5_ANGLESPAN;
	servoSet(5, SERVO5_MID + (angle * dz));
}
