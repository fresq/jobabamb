#pragma once

#include "rc5.h"

#define LCD_Clear LCDClear
#define LCD_GoTo LCDXY
#define LCD_WriteText LCDOutString


int MainMenu()
{
	char* tab[] = {"Nalej wodki", "Zapamietane Pozycje", "Kalibracja Srerv", "Reczna manipulacja", "Odwrotna", "Zwierzeta", "Pompka", "Znajdz zero", "Komunikacja", "Napiecie", "Jazda", "Blue"};
	int max = 12;
	int aktualny = 0;
	int zMin = 0, zMax = 3;
	int cmd;
	
	LCD_Clear();
	for(int i = 0; i < 4; i++)
	{
		LCD_GoTo(0,i);
		if(i+zMin == aktualny) LCD_WriteText("->");
		LCD_WriteText(tab[zMin+i]);
	}

	while(1)
	{
		while((cmd = detect()) < 0);
		//toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return -1;
		if(cmd == KEY_OK) return aktualny;
		if(cmd > KEY_1 && cmd <= max) return cmd - 1;
		if(cmd == KEY_DOWN)
		{
			aktualny++; if(aktualny >= max) {aktualny = 0; zMin = 0; zMax = 3;}
			if(aktualny > zMax) {zMax++; zMin++;}
		}
		else if(cmd == KEY_UP)
		{
			aktualny--; if(aktualny < 0) {aktualny = max - 1; zMin = max - 4; zMax = max - 1;}
			if(aktualny < zMin) {zMin--; zMax++;}
		}

		LCD_Clear();
		for(int i = 0; i < 4; i++)
		{
			LCD_GoTo(0, i);
			if(i+zMin == aktualny) LCD_WriteText("->");
			LCD_WriteText(tab[zMin+i]);
		}

	}
}
