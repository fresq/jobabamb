#pragma once

#define KROKOWY1_GPIO GPIOA
#define KROKOWY2_GPIO GPIOA
#define KROKOWY3_GPIO GPIOC
#define KROKOWY4_GPIO GPIOC

#define KROKOWY1 GPIO_Pin_9
#define KROKOWY2 GPIO_Pin_10
#define KROKOWY3 GPIO_Pin_10
#define KROKOWY4 GPIO_Pin_11

#define KROKOWY1_PWM_GPIO GPIOC
#define KROKOWY2_PWM_GPIO GPIOA

#define KROKOWY1_PWM_PIN GPIO_Pin_9
#define KROKOWY2_PWM_PIN GPIO_Pin_8

volatile int8_t krok = 0;
volatile int16_t krok_poz = 180;
float t;
volatile int zerowa = 0;
#define MICRO_COUNT 50

//50 //int16_t values[MICRO_COUNT] = {0,320,640,960,1278,1595,1911,2225,2536,2845,3151,3453,3752,4047,4338,4625,4907,5183,5455,5721,5981,6234,6482,6723,6956,7183,7402,7614,7818,8014,8201,8380,8551,8713,8865,9009,9144,9269,9384,9490,9586,9672,9749,9815,9871,9917,9953,9979,9994,10000};
//int16_t values[MICRO_COUNT] = {0,1736,3420,4999,6427,7660,8660,9396,9848,10000}; //10
int16_t values[MICRO_COUNT] = {0,204,408,612,816,1020,1224,1428,1632,1836,2040,2244,2448,2653,2857,3061,3265,3469,3673,3877,4081,4285,4489,4693,4897,5102,5306,5510,5714,5918,6122,6326,6530,6734,6938,7142,7346,7551,7755,7959,8163,8367,8571,8775,8979,9183,9387,9591,9795,10000};
//int16_t values[MICRO_COUNT] = {0,825,1645,2454,3246,4016,4759,5469,6142,6772,7357,7891,8371,8794,9157,9458,9694,9863,9965,10000};//20
volatile int mMicroKrok = 0;
volatile int mKrokPoz = 0;
volatile int mKrok = 0;

//Przerwanie pozycji zerowej
void EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		zerowa = 1;
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

void STEPER_Config()
{
	t = 0.5f*3.14159265f/((float)MICRO_COUNT);

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOC, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = KROKOWY1 | KROKOWY2;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;

	GPIO_Init(KROKOWY1_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = KROKOWY3 | KROKOWY4;
	GPIO_Init(KROKOWY3_GPIO, &GPIO_InitStructure);

	///PWM
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

	GPIO_InitStructure.GPIO_Pin = KROKOWY1_PWM_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(KROKOWY1_PWM_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = KROKOWY2_PWM_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(KROKOWY2_PWM_GPIO, &GPIO_InitStructure);

	GPIO_PinAFConfig(KROKOWY1_PWM_GPIO, GPIO_PinSource9, GPIO_AF_TIM8);
	GPIO_PinAFConfig(KROKOWY2_PWM_GPIO, GPIO_PinSource8, GPIO_AF_TIM1);





	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 10000;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 335;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseInitStructure);

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 9000;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	outputChannelInit.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outputChannelInit.TIM_OCIdleState = TIM_OCIdleState_Set;
	outputChannelInit.TIM_OCNIdleState = TIM_OCIdleState_Reset;

	TIM_OC4Init(TIM8, &outputChannelInit);
	TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);

	TIM_OC1Init(TIM1, &outputChannelInit);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);

	TIM_Cmd(TIM1, ENABLE);
	TIM_Cmd(TIM8, ENABLE);

	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_CtrlPWMOutputs(TIM8, ENABLE);

	/////////
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

	TIM_TimeBaseInitTypeDef timerInitStructure;
	timerInitStructure.TIM_Prescaler = 1000;
	timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	timerInitStructure.TIM_Period = 33;
	timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	timerInitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM7, &timerInitStructure);
	TIM_Cmd(TIM7, ENABLE);
	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);

	  NVIC_InitTypeDef nvicStructure;
	    nvicStructure.NVIC_IRQChannel = TIM7_IRQn;
	    nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
	    nvicStructure.NVIC_IRQChannelSubPriority = 1;
	    nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	    NVIC_Init(&nvicStructure);


	//Konfiguracja pozycji zerowej
#define ZEROWA_GPIO GPIOB
#define ZEROWA_PIN GPIO_Pin_0

	EXTI_InitTypeDef   EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = ZEROWA_PIN;
	GPIO_Init(ZEROWA_GPIO, &GPIO_InitStructure);

	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource0);

	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


void PolKrok();


void MikroKrok8(int lewo)
{
	if(lewo)
	{
		mMicroKrok++;
		mKrokPoz++;
		if(mMicroKrok == MICRO_COUNT)
		{
			mKrok++;
			mMicroKrok = 0;
			if(mKrok == 8)
			{
				mKrok = 0;
			}
		}
	}
	else
	{
		mMicroKrok--;
		mKrokPoz--;
		if(mMicroKrok == -1)
		{
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1;
			if(mKrok == -1)
			{
				mKrok = 7;
			}
		}
	}

	if(mKrok == 0)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = 10000;
		TIM1->CCR1 = values[mMicroKrok];
	}
	else if(mKrok == 1)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = 10000;
	}
	else if(mKrok == 2)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = 10000;
	}
	else if(mKrok == 3)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = 10000;
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	}
	else if(mKrok == 4)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = 10000;
		TIM1->CCR1 = values[mMicroKrok];
	}
	else if(mKrok == 5)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = 10000;
	}
	else if(mKrok == 6)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = 10000;
	}
	else if(mKrok == 7)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = 10000;
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	}
}

void MikroKrok2(int lewo)
{
	if(lewo)
	{
		mMicroKrok++;
		mKrokPoz++;
		if(mMicroKrok == MICRO_COUNT)
		{
			mKrok++;
			mMicroKrok = 0;
			if(mKrok == 4)
			{
				mKrok = 0;
			}
		}
	}
	else
	{
		mMicroKrok--;
		mKrokPoz--;
		if(mMicroKrok == -1)
		{
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1;
			if(mKrok == -1)
			{
				mKrok = 3;
			}
		}
	}

	if(mKrok == 0)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	}
	else if(mKrok == 1)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = values[mMicroKrok];
	}
	else if(mKrok == 2)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[mMicroKrok];
		TIM1->CCR1 = values[MICRO_COUNT - mMicroKrok - 1];
	}
	else if(mKrok == 3)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
		TIM8->CCR4 = values[MICRO_COUNT - mMicroKrok - 1];
		TIM1->CCR1 = values[mMicroKrok];
	}
}


void MikroKrokSC(int lewo)
{
	if(lewo)
		krok_poz++;
	else
		krok_poz--;
	float t1 = sin(krok_poz * t);
	float t2 = cos(krok_poz * t);
	if(t1 > 0)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
	}
	else
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
	}
	t1 = (t1 > 0 ? t1 : -t1);
	t1 = t1*10000.0f;
	TIM8->CCR4 = (int)t1;

	if(t2 > 0)
	{
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else
	{
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	t2 = (t2 > 0 ? t2 : -t2);
	t2 = t2*10000.0f;
	TIM1->CCR1 = (int)(t2);
}


volatile int steper_speed = 0;
volatile int steper_dir = 0;

void SteperTimerHandler()
{
	static volatile licz = 0;
	licz++;
	if(steper_speed != 0 && licz >  steper_speed)
	{
		licz = 0;
		MikroKrok2(steper_dir);
	}
}

void TIM7_IRQHandler()
{
    if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
        SteperTimerHandler();
    }
}

void Krocz(int krok, int dir)
{
	for(int i = 0; i < krok; i++)
	{
		MikroKrokSC(dir);
		for(volatile int j = 0; j < 1000; j++);
	}
}

void delay()
{
	for(volatile uint32_t i = 1000000; i > 0; i--);
}

void PolKrok(int prawo)
{
	if(prawo)
	{
		krok++;
		krok_poz++;
	}
	else
	{
		krok--;
		krok_poz--;
	}

	if(krok == -1) krok = 7;
	if(krok == 8) krok = 0;

	if(krok == 0)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 1)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 2)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 3)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_SetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 4)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_ResetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 5)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_SetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 6)
	{
		GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	else if(krok == 7)
	{
		GPIO_SetBits(KROKOWY1_GPIO, KROKOWY1);
		GPIO_ResetBits(KROKOWY2_GPIO, KROKOWY2);
		GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3);
		GPIO_SetBits(KROKOWY4_GPIO, KROKOWY4);
	}
	delay();
}

void ZerujKrokowy()
{
	zerowa = 0;
	while(!zerowa)
	{
		MikroKrok2(1);
		for(volatile int i = 0; i < 10000; i++);
	}
	krok_poz = 180;
	for(int j = 0; j < 362; j++)
	{
		MikroKrok2(0);
		for(volatile int i = 0; i < 10000; i++);
	}
}

void KrokowyStop()
{
	GPIO_ResetBits(KROKOWY1_GPIO, KROKOWY1 | KROKOWY2);
	GPIO_ResetBits(KROKOWY3_GPIO, KROKOWY3 | KROKOWY4);
}
