#ifndef _FM_JOBABAMB__HH
#define _FM_JOBABAMB__HH

#include "FastMatrix.h"

float LengthArray[4] = {11,9,8,8};

//Matrix declaration
FastMatrix tmp_q0;
FastMatrix tmp_yd;

FastMatrix tmp_q;
FastMatrix tmp_y;
FastMatrix tmp_E;
FastMatrix tmp_J;
FastMatrix tmp_JT;
FastMatrix tmp_J1;
FastMatrix tmp_J1I;
FastMatrix tmp_JSH;
FastMatrix tmp_q1;
FastMatrix tmp_h;

FastMatrix tmp_J1DetTmp;
FastMatrix tmp_J1InvTmp;

//Matrix pointer declaration
FastMatrix* fm_q0;
FastMatrix* fm_yd;

FastMatrix* fm_q;
FastMatrix* fm_y;
FastMatrix* fm_E;
FastMatrix* fm_J;
FastMatrix* fm_JT;
FastMatrix* fm_J1;
FastMatrix* fm_J1I;
FastMatrix* fm_JSH;
FastMatrix* fm_q1;
FastMatrix* fm_h;

FastMatrix* fm_J1DetTmp;
FastMatrix* fm_J1InvTmp;

//some const value
int maxIteration = 15000;
float gmm = 0.1;
float stopCondition = 0.01;
float hh = 0.01;


//wylicza jakobian
void rkJacobian(FastMatrix* q, FastMatrix* J)
{
	double l1 = LengthArray[0];
	double l2 = LengthArray[1];
	double l3 = LengthArray[2];
	double l4 = LengthArray[3];
	double q1 = fmGet(q, 0, 0);
	double q12 = q1 + fmGet(q, 1, 0);
	double q123 = q12 + fmGet(q, 2, 0);
	double q1234 = q123 + fmGet(q, 3, 0);
	double c1 = cos(q1);
	double c12 = cos(q12);
	double c123 = cos(q123);
	double c1234 = cos(q1234);
	double s1 = sin(q1);
	double s12 = sin(q12);
	double s123 = sin(q123);
	double s1234 = sin(q1234);
	fmSet(J, 0, 0, - l1*s1 - l2*s12 - l3*s123 - l4*s1234);
	fmSet(J, 0, 1, - l2*s12 - l3*s123 - l4*s1234);
	fmSet(J, 0, 2, - l3*s123 - l4*s1234);
	fmSet(J, 0, 3, - l4*s1234);
	fmSet(J, 1, 0, l1*c1 + l2*c12 + l3*c123 + l4*c1234);
	fmSet(J, 1, 1, l2*c12 + l3*c123 + l4*c1234);
	fmSet(J, 1, 2, l3*c123 + l4*c1234);
	fmSet(J, 1, 3, l4*c1234);
	fmSet(J, 2, 0, 1);
	fmSet(J, 2, 1, 1);
	fmSet(J, 2, 2, 1);
	fmSet(J, 2, 3, 1);
}

//Zwraza polozenie chwytaka w wspolzednych ukladu 0. K(q)
void rkKinematic(FastMatrix* q, FastMatrix* k)
{
	double x = 0, y = 0, qs = 0;
	for(int i = 0; i < q->r; i++)
	{
		qs += fmGet(q, i, 0);
		x += LengthArray[i] * cos(qs);
		y += LengthArray[i] * sin(qs);
	}
	fmSet(k, 0, 0, x);
	fmSet(k, 1, 0, y);
	fmSet(k, 2, 0, qs);
}

//core
//PRE, nalezy ustawic:
// fm_yd - destiny point
// fm_q0 - nastawy poczatkow
// POST:
// fm_q - result
void rkReversKinematic()
{
	int count = maxIteration;

	fmCopy(fm_q0, fm_q);
	rkKinematic(fm_q, fm_y);
	fmSub(fm_y, fm_yd, fm_E);
	float errStart = fmNorm(fm_E);

	while(1)
	{
		rkKinematic(fm_q, fm_y); //Aktualne polozenie
		fmSub(fm_y, fm_yd, fm_E); //Blad polozenia
		float err = fmNorm(fm_E); //dlugosc bledu(norma euklidesowa)

		if(err > errStart * 5) {/*"Spierdolilo!"*/; break; }
		if(err < stopCondition) { /*std::cout << "Koniec... " << err << "\n"; */ break; }

		rkJacobian(fm_q, fm_J); //J
		fmTrans(fm_J, fm_JT);   //JT
		fmMul(fm_J, fm_JT, fm_J1); //J1= J * JT

		if(fmDetEvenFaster(fm_J1, fm_J1DetTmp) < 0.01) /*"Macierz Osobliwa"*/
		{
			fmAdd(fm_J1, fm_h, fm_J1); //Dodajemy male co nieco
		}

		//q = q - ((JT * J1.Invert()) * (E) )* gmm;
		fmInvertEvenFaster(fm_J1, fm_J1I, fm_J1InvTmp);
		fmMul(fm_JT, fm_J1I, fm_JSH);
		fmMul(fm_JSH, fm_E, fm_q1);
		fmMulfS(fm_q1, gmm);
		fmSub(fm_q, fm_q1, fm_q);
		if(count-- < 1)
		{
			//std::cout << "Nie tak mialo byc... " << err << "\n";
			break;
		}
		
	}
}

float rkBoundAngle(float angle)
{
	if(angle > 3.14159256f) return 3.14159256f;
	if(angle < -3.14159256f / 2) return -3.14159256f / 2;
	return angle;
}

//q - aktualne nastawy
// after: q - nastawy koncowe
int rkMove(float start_x, float start_y, float delta_x, float delta_y, float* q)
{
	float dx = start_x + delta_x;
	float dy = start_y + delta_y;
	float l1 = LengthArray[0];
	float l2 = LengthArray[1];
	float l3 = LengthArray[2];
	float l4 = LengthArray[3];

	float d = sqrt((-l4-dx)*(-l4-dx) + dy*dy);
	if( d > (l1+l2+l3)*0.95) return -1;
	if( d < (l1)*1.1 )return -2;
	if(dx > -l1) return -3;
	if(dy < -0.3f * l3) return -4;
	if(dy < 0.8f * l1)
	{
		fm_q0->tab[0] = 135 * 3.14149265f / 180.0f;
		fm_q0->tab[1] = 45 * 3.14149265f / 180.0f;
		fm_q0->tab[2] = 45 * 3.14149265f / 180.0f;
		fm_q0->tab[3] = -45 * 3.14149265f / 180.0f;
	}
	else
	{
		fm_q0->tab[0] = 90 * 3.14149265f / 180.0f;
		fm_q0->tab[1] = 30 * 3.14149265f / 180.0f;
		fm_q0->tab[2] = 30 * 3.14149265f / 180.0f;
		fm_q0->tab[3] = 30 * 3.14149265f / 180.0f;
	}

	fm_yd->tab[0] = start_x + delta_x;
	fm_yd->tab[1] = start_y + delta_y;
	fm_yd->tab[2] = 3.14159265f;

	rkReversKinematic();

	for(int i = 0; i < 4; i++)
	q[i] = rkBoundAngle(fm_q->tab[i]);

	return 0;
}

int rkMoveAbout(float delta_x, float delta_y, float* q)
{
	fm_q0->tab[0] = q[0] * 3.14149265f / 180.0f;
	fm_q0->tab[1] = q[1] * 3.14149265f / 180.0f;
	fm_q0->tab[2] = q[2] * 3.14149265f / 180.0f;
	fm_q0->tab[3] = q[3] * 3.14149265f / 180.0f;
	rkKinematic(fm_q0, fm_y);
	float start_x = fm_y->tab[0];
	float start_y = fm_y->tab[1];

	return rkMove(start_x, start_y, delta_x, delta_y, q);
}

void rkInit()
{
	fm_q0   = &tmp_q0;   fmCreate(fm_q0, 4, 1);
	fm_yd   = &tmp_yd;   fmCreate(fm_yd, 3, 1);

	fm_q   = &tmp_q;   fmCreate(fm_q, 4, 1);
	fm_y   = &tmp_y;   fmCreate(fm_y, 3, 1);
	fm_E   = &tmp_E;   fmCreate(fm_E, 3, 1);
	fm_J   = &tmp_J;   fmCreate(fm_J, 3, 4);
	fm_JT  = &tmp_JT;  fmCreate(fm_JT, 4, 3);
	fm_J1  = &tmp_J1;  fmCreate(fm_J1, 3, 3);
	fm_J1I = &tmp_J1I; fmCreate(fm_J1I, 3, 3);
	fm_JSH = &tmp_JSH; fmCreate(fm_JSH, 4, 3);
	fm_q1  = &tmp_q1;  fmCreate(fm_q1, 4, 1);
	fm_h   = &tmp_h;   fmCreate(fm_h, 3, 3);

	fm_J1DetTmp = &tmp_J1DetTmp; fmCreate(fm_J1DetTmp, 3, 3);
	fm_J1InvTmp = &tmp_J1InvTmp; fmCreate(fm_J1InvTmp, 3, 6);

	fm_h->tab[0] = 1; fm_h->tab[1] = 0; fm_h->tab[2] = 0;
	fm_h->tab[3] = 0; fm_h->tab[4] = 1; fm_h->tab[5] = 0;
	fm_h->tab[6] = 0; fm_h->tab[7] = 0; fm_h->tab[8] = 1;
	fmMulfS(fm_h, hh);
}

float R2D(float rad)
{
	return rad * 180.0f / 3.14159265f;
}
int rkMoveArm(arm* a, float x, float y)
{
	float tab[4];
	tab[0] = 90 + a->tab[0].ang;
	tab[1] = a->tab[1].ang;
	tab[2] = a->tab[2].ang;
	tab[3] = a->tab[3].ang;
	int res = rkMoveAbout(x, y, tab);
	if(res == 0)
		SetSlowAngle(a, R2D(tab[0]) - 90, R2D(tab[1]), R2D(tab[2]), R2D(tab[3]), 151, 151, 300);
	return res;
}


#endif
