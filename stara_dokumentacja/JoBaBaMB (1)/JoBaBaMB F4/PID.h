#pragma once

#include "Motors.h"
#include "Bluetooth.h"



float speedTargets[6];
float legPosition[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
float calki[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
float legSpeed[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
float r[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
int legTemerature[6] = {0,0,0,0,0,0};
int calkowanie_on[6] = {1,1,1,1,1,1};


void SetMotorSpeed(int motor, float speed)
{
	int dir = MOTOR_NEUTRAL;
	if(speed < -0.01f) dir = MOTOR_BACKWARD;
	else if(speed > 0.01f) dir = MOTOR_FORWARD;
	else dir = MOTOR_STOP;
	Set_Motor_Dir(motor, dir);
	speedTargets[motor-1] = abs(speed);
}

void PID_Handler()
{
	float e;

	for(int i = 0; i < 6; i++)
	{
		legPosition[i] += (encoders[i] * 360.0f / (float)PULS_PER_ROTATION);
		if(legPosition[i] > 360) legPosition[i] -= 360;
		if(legPosition[i] < 0) legPosition[i] += 360;
		legSpeed[i] = (abs(encoders[i]) / (float)PULS_PER_ROTATION) / 0.1f; //todo //liczmy w obrotach an minute
		e = speedTargets[i] - legSpeed[i];

		calki[i] += calkowanie_on[i] * e * 0.025f;

		r[i] = k*e + (1/ti)*calki[i] + td*(e / 0.025);

		if (r[i] > 1) {r[i] = 1.0f; calkowanie_on[i] = 0;}
		else if (r[i] < 0) {r[i] = 0; calkowanie_on[i] = 0;}
		else calkowanie_on[i] = 1;

		//Teperature controll
//		if(legSpeed[i] < 0.1f && r[i] > 0.99f)
//			legTemerature[i]++;
//		else
//			legTemerature[i]--;
//		if(legTemerature[i] < 0) legTemerature[i] = 0;
//
//		if(legTemerature[i] > 50)
//			r[i] = 0;
		//////////////////////

		Set_Motor_PWM(i+1, r[i]);
	}
}

void SendPID()
{
	ramka_danych[0] = START_BYTE;
	ramka_danych[1] = PID_DATA;
	for(int i = 0; i < 6; i++)
	{
		ramka_danych[3*i+2] = (unsigned int)(legSpeed[i] / 3.5f);
		ramka_danych[3*i+3] = (unsigned int)(speedTargets[i] / 3.5f);
		ramka_danych[3*i+4] = 100 * r[i];
	}
	ramka_danych[21] = STOP_BYTE;
	SendPacket();
}

void SendEncoders()
{
	ramka_danych[0] = START_BYTE;
	ramka_danych[1] = ENCODERS_DATA;
	for(int i = 0; i < 6; i++)
	{
		ramka_danych[i*2+2] = (encoders[i] & 0x00FF);
		ramka_danych[i*2+3] = (encoders[i] & 0xFF00) >> 8;
	}
	ramka_danych[21] = STOP_BYTE;
	SendPacket();
}


void FindZero()
{
	int tab[6] = {0,0,0,0,0,0};
	for(int i = 0; i < 0; i++)
		SetMotorSpeed(i+1, 10);
}


void StablePosition(float tp)
{
	for(int i = 0; i < 6; i++)
	{
		float e = tp - legPosition[i];
		if(e < -180) e += 360;
		if(e > 180) e -= 360;
		//int znak = (e > 0 ? -1 : 1);
		float speed = 280*sin(e*(2*3.14159265f/720.0f));
		//float speed = 1.3f*abs(e);
		//float speed = 20*sqrt(abs(e));
		SetMotorSpeed(i+1, speed);
	}
}



