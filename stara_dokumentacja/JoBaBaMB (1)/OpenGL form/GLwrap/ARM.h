#pragma once

#include "Joint.h"


class ARM
{
public:
	Joint *Joints;
	int Count;
	enum { NONE, SET, MOD, SETR, MODR } Mode;
	
public:
	ARM(): Count(0), Joints(0), Mode(NONE) {}
	ARM(int count): Count(count), Mode(NONE) { Joints = new Joint[count+1]; }
	ARM(Joint *list, int count): Count(count), Mode(NONE) { for(int i=0; i<=count; i++) Joints[i] = list[i]; }
	~ARM() { if(Joints) delete[] Joints; }
	
	void Move(); //podstawa calego systemu...
	void MoveTo(const Vec2f& target);

	const int GetCount() const { return Count; }
	const Joint& GetJoint(int Id) const { if(Id>0 && Id<=Count) return Joints[Id-1]; else return Joints[Count]; }


	float FindAlpha(int id, float dx, float dy);
	ARM& GoX(int id, float alpha);
	ARM& GoY(int id, float alpha);
	float CheckX(int id, float alpha);
	float CheckY(int id, float alpha);
	ARM& MovX(float x);
	ARM& MovY(float x);
	ARM& MovX2(float x);
	ARM& MovY2(float x);

	ARM& Mod(int Id, float Angle, float Len = 0);
	ARM& Set(int Id, float Len, float Angle = 0);
	ARM& ModR(int Id, float Angle, float Len);
	ARM& SetR(int Id, float Len, float Angle);
	ARM& operator() (int Id, float A, float B = 0);

	//void glDraw();
};

float ARM::FindAlpha(int id, float dx, float dy)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = GetJoint(Count+1).Loc().y;
	float x = GetJoint(Count+1).Loc().x;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = GetJoint(id).Loc().x;
	float q1 = GetJoint(id).Loc().y;

	//Pobieramy punkt pierwszego stawu jako srodek nowego ukladu odniesienia
	float y0 = GetJoint(id - 1).Loc().y;
	float x0 = GetJoint(id - 1).Loc().x;

	float R = GetJoint(id - 1).Len();
	float alpha1 = GetJoint(id - 1).Arg();
	float r2 = sqrt((dx-x0)*(dx-x0)+(dy-y0)*(dy-y0));
	float alpha2 = atan2((dy-y0),(dx-x0))*180.0f/3.14159265f;
	float l = sqrt((x-p1)*(x-p1) + (y-q1)*(y-q1));

	float podacosinus = (r2*r2 + R*R - l*l)/(2*r2*R);

	if(abs(podacosinus)<1.0)
	{
		podacosinus = acos(podacosinus)*180.0f/3.14159265f;
		return (alpha2 + podacosinus - alpha1);
	}
	else //niemozliwe
	{
		return 0.0f;
	}
}

ARM& ARM::MovX2(float x)
{
	float dy = GetJoint(Count+1).Loc().y;
	float dx = GetJoint(Count+1).Loc().x + x;

	for(int j = Count; j>=2; j--)
	{
		float alpha = FindAlpha(j,dx,dy);
		if (alpha != 0.0)
		{
			GoX(j,alpha);
			break;
		}
	}

	return *this;
}

ARM& ARM::MovY2(float y)
{
	float dy = GetJoint(Count+1).Loc().y + y;
	float dx = GetJoint(Count+1).Loc().x;

	for(int j = Count; j>=2; j--)
	{
		float alpha = FindAlpha(j,dx,dy);
		if (alpha != 0.0)
		{
			GoY(j,alpha);
			break;
		}
	}

	return *this;
}

ARM& ARM::MovX(float x)
{
	for(int i = 0; i < 50; i++)
	{
		//Losujemy jeda z par do manipulacji
		int id = (rand() % (Count-1)) + 2;
		float alpha = ((rand() % 1000) - 500)/1000.0f; 
		//Sprawdzamy o ile by nastapilo przesuniecie
		float TryX = CheckX(id,alpha);
		//Sprawdzamy czy pasuje nam takie przesuniecie
		if(abs(x-TryX) < abs(x)) 
		{
			//Modyfikujemy pozostala odleglosc
			x = x -TryX;
			//Zapisujemy zmieny
			GoX(id, alpha);
		}
		if(abs(x) < 0.1) break;
	}

	return *this;
}

ARM& ARM::MovY(float y)
{
	for(int i = 0; i < 50; i++)
	{
		//Losujemy jeda z par do manipulacji
		int id = (rand() % (Count-1)) + 2;
		float alpha = ((rand() % 1000) - 500)/1000.0f; 
		//Sprawdzamy o ile by nastapilo przesuniecie
		float TryY = CheckY(id,alpha);
		//Sprawdzamy czy pasuje nam takie przesuniecie
		if(abs(y-TryY) < abs(y)) 
		{
			//Modyfikujemy pozostala odleglosc
			y = y - TryY;
			//Zapisujemy zmieny
			GoY(id, alpha);
		}
		if(abs(y) < 0.1) break;
	}

	return *this;
}

float ARM::CheckX(int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = GetJoint(Count+1).Loc().y;
	float x = GetJoint(Count+1).Loc().x;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = GetJoint(id).Loc().x;
	float q1 = GetJoint(id).Loc().y;

	//Pobieramy punkt pierwszego stawu
	float y0 = GetJoint(id - 1).Loc().y;
	float x0 = GetJoint(id - 1).Loc().x;

	//wyliczmy cosinus i sinus
	float c = cos(alpha * 3.141592f / 180.0f);
	float s = sin(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1)-(y-q)*(y-q);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		//Do testow

		//jesli tak konczymy wyliczanie x
		podpierwiastek = sqrt(podpierwiastek);
		float x2 = ( abs(p + podpierwiastek - x) < abs(p - podpierwiastek - x) ) ? (p + podpierwiastek) : (p - podpierwiastek);
		//Zwracamy przesuniecie
		return x2 - x;

	}else //gdy przsuniecie nie jest mozliwe cofamy pierwsza zmiane
	{
		return 0;
	}
}

float ARM::CheckY(int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = GetJoint(Count+1).Loc().y;
	float x = GetJoint(Count+1).Loc().x;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = GetJoint(id).Loc().x;
	float q1 = GetJoint(id).Loc().y;

	//Pobieramy punkt pierwszego stawu
	float y0 = GetJoint(id - 1).Loc().y;
	float x0 = GetJoint(id - 1).Loc().x;

	//wyliczmy cosinus i sinus
	float c = cos(alpha * 3.141592f / 180.0f);
	float s = sin(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1) - (x-p)*(x-p);

	if (podpierwiastek >= 0.0f)
	{
		podpierwiastek = sqrt(podpierwiastek);
		float y2 = ( abs(q + podpierwiastek - y) < abs(q - podpierwiastek - y) ) ? (q + podpierwiastek) : (q - podpierwiastek);
		return y2 - y;

	}else
	{
		return 0;
	}


}

ARM& ARM::GoX(int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = GetJoint(Count+1).Loc().y;
	float x = GetJoint(Count+1).Loc().x;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = GetJoint(id).Loc().x;
	float q1 = GetJoint(id).Loc().y;

	//Pobieramy punkt pierwszego stawu
	float y0 = GetJoint(id - 1).Loc().y;
	float x0 = GetJoint(id - 1).Loc().x;

	//wyliczmy cosinus i sinus
	float c = cos(alpha * 3.141592f / 180.0f);
	float s = sin(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	float x2 = (x-x0)*c - (y-y0)*s + x0;
	float y2 = (x-x0)*s + (y-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1)-(y-q)*(y-q);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		//jesli tak konczymy wyliczanie x
		podpierwiastek = sqrt(podpierwiastek);
		x = ( abs(p + podpierwiastek - x) < abs(p - podpierwiastek - x) ) ? (p + podpierwiastek) : (p - podpierwiastek);
			
		//wyliczmy kat zmiany dla drugiego stawu
		float beta = atan2((y-q),(x-p)) - atan2(y2-q,x2-p) ;
		beta = (beta*180.0f)/3.14159265f;
		//beta = beta - GetJoint(id).Arg();

		//Modyfikujemy go o wyliczony k�t
		Mod(id-1,alpha)(id,beta).Move();
	}else //gdy przsuniecie nie jest mozliwe
	{
		
	}
	return *this;
}

ARM& ARM::GoY(int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = GetJoint(Count+1).Loc().y;
	float x = GetJoint(Count+1).Loc().x;

	//Obracamy pierwszy staw o zadany kat
	Mod(id-1,alpha).Move();

	//parametery receptora po pierwszym zgieciu
	float y2 = GetJoint(Count+1).Loc().y;
	float x2 = GetJoint(Count+1).Loc().x;

	//Pobieramy dlugosc i polozenie stawu
	float p = GetJoint(id).Loc().x;
	float q = GetJoint(id).Loc().y;
	float r2 = (x2-p)*(x2-p)+(y2-q)*(y2-q);

	//Zrownanie wyliczmy nowe polozenie x
	float podpierwiastek = -x*x + 2*x*p - p*p + r2;

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		podpierwiastek = sqrt(podpierwiastek);
		y = ( abs(q + podpierwiastek - y) < abs(q - podpierwiastek - y) ) ? (q + podpierwiastek) : (q - podpierwiastek);

		float beta = atan2((y-q),(x-p)) - atan2(y2-q,x2-p);
		beta = (beta*180.0f)/3.14159265f;

		Mod(id,beta).Move();
	}else
	{
		Mod(id-1,-alpha).Move();
	}

	return *this;
}

void ARM::MoveTo(const Vec2f& target)

{

}

void ARM::Move()
{
	Mode = NONE;
	for(int i=1; i<=Count; i++)
	{
		Joints[i-1] ^ Joints[i];
	}
}

ARM& ARM::Mod(int Id, float Angle, float Len)
{
	Mode = MOD;
	if(0<Id && Id<=Count)
	{
		if(Id==1) Joints[Id-1].Arg() += Angle; //kat elementu bazowego jest jego transformacja
		Joints[Id-1].Angle() += Angle;
		Joints[Id-1].Len() += Len;
	}
	return *this;
}

ARM& ARM::Set(int Id, float Len, float Angle)
{
	Mode = SET;
	if(0<Id && Id<=Count)
	{
		if(Id==1) Joints[Id-1].Arg() = Angle; //kat elementu bazowego jest jego transformacja
		Joints[Id-1].Angle() = Angle;
		Joints[Id-1].Len() = Len;
	}
	return *this;
}

ARM& ARM::ModR(int Id, float Angle, float Len = 0)
{
	Mode = MODR;
	if(0<Id && Id<=Count)
	{
		if(Id==1) Joints[Count-Id].Arg() += Angle; //kat elementu bazowego jest jego transformacja
		Joints[Count-Id].Angle() += Angle;
		Joints[Count-Id].Len() += Len;
	}
	return *this;
}

ARM& ARM::SetR(int Id, float Len, float Angle = 0)
{
	Mode = SETR;
	if(0<Id && Id<=Count)
	{
		if(Id==1) Joints[Count-Id].Arg() = Angle; //kat elementu bazowego jest jego transformacja
		Joints[Count-Id].Angle() = Angle;
		Joints[Count-Id].Len() = Len;
	}
	return *this;
}

ARM& ARM::operator() (int Id, float A, float B)
{
	if(Mode==MOD) return Mod(Id, A, B);
	if(Mode==SET) return Set(Id, A, B);
	if(Mode==MODR) return ModR(Id, A, B);
	if(Mode==SETR) return SetR(Id, A, B);
	if(Mode==NONE) throw "Hej, ty3 Wiesz, ze operator() nalezy poprzedzic poleceniem Set() lub Mod()?";

	return *this; //profilaktycznie
}
