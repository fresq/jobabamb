#pragma once

#define GLFW_DLL
#include "glfw.h"

class Screen
{
private:
	static int Width;
	static int Height;

public:
	Screen(int width, int height, bool fullscreen = false);
	static void GLFWCALL ResizeGLScene(int width, int height);
	static void SetWindowSize(int width, int height);

	void OpenWindow(int width, int height, bool fullscreen = false);
	void KillWindow();
	
	void setWidth(int w) { if(w > 0) Width = w; else w = 1; }
	void setHeight(int h) { if(h > 0) Height = h; else h = 1; }
	int getWidth() { return Width; }
	int getHeight() { return Height; }
};
