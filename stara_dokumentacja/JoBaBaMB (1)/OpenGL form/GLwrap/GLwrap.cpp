
#include "Screen.h"
#include "Jobabamb.h"
#include <cmath>

public ref class GLengine
{
private:
	Jobabamb* jobabamb;

private:
	float r,g,b;
	float R;		
	float Wx,Wy,Wz;
	float Xa;
	float Ya;

public:
	void MainSurprise()
	{

	GLFWvidmode return_struct;
	glfwGetDesktopMode( &return_struct );

		Screen* screen = new Screen(return_struct.Width, return_struct.Height);
		jobabamb = new Jobabamb();
		glfwSwapInterval(1);
		glDisable(GL_TEXTURE_2D);

		R = 80;
		Xa = 0;
		Ya = 10.0f*3.14159265f/180.0f;
		Wx=R*cos(Xa)*cos(Ya);
		Wy=R*sin(Ya);
		Wz=R*cos(Ya)*sin(Xa);

		glPolygonMode( GL_BACK, GL_LINE );          // Back Face Is Filled In
		glPolygonMode( GL_FRONT, GL_LINE );         // Front Face Is Drawn With Lines

		GLfloat LightAmbient[]=		{ 0.5f, 0.5f, 0.5f, 1.0f };
		GLfloat LightDiffuse[]=		{ 1.0f, 1.0f, 1.0f, 1.0f };
		GLfloat LightPosition[]=	{ 0.0f, 0.0f, 2.0f, 1.0f };

		
	glEnable(GL_TEXTURE_2D);							// Enable Texture Mapping
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glEnable(GL_COLOR_MATERIAL);						// Enable Coloring Of Materia
	glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);


	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_COLOR_MATERIAL);
	//glEnable(GL_NORMALIZE);
	//glShadeModel(GL_SMOOTH);

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);		// Setup The Ambient Light
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);		// Setup The Diffuse Light
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);	// Position The Light
	glEnable(GL_LIGHT1);	// Enable Light One
	glEnable(GL_LIGHTING);


		while(1)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
			glLoadIdentity();
			
			gluLookAt(Wx,Wy,Wz,0,0,0,0,1,0);

			glColor3f(r,g,b);
			jobabamb->Draw(0, -20, 0);

			glfwSwapBuffers();
		}
		if(screen != nullptr) delete screen;
	}
	
	void SetColor(float rr, float gg, float bb)
	{
		r = rr;
		g = gg;
		b = bb;
	}

	void SetView(float r, float xa, float ya)
	{
		R = r;		
		Xa = xa*3.14159265f/180.0f;
		Ya = ya*3.14159265f/180.0f;
		Wx=R*cos(Xa)*cos(Ya);
		Wy=R*sin(Ya);
		Wz=R*cos(Ya)*sin(Xa);
	}


	void SetAngle(float a1, float a2, float a3, float a4, float ch)
	{
		jobabamb->SetAngle(a1,a2,a3,a4,ch);
	}

	void SetA(int i, int a)
	{
		jobabamb->SetA(i,a);
	}

	void SetK(int k)
	{
		jobabamb->SetK(k);
	}

	void SetCh(int ch)
	{
		jobabamb->SetCh(ch);
	}

	void ModAngle(float a1, float a2, float a3, float a4, float ch)
	{
		jobabamb->ModAngle(a1,a2,a3,a4,ch);
	}

	void ModA(int i, int a)
	{
		jobabamb->ModA(i,a);
	}

	void ModK(int k)
	{
		jobabamb->ModK(k);
	}

	void ModCh(int ch)
	{
		jobabamb->ModCh(ch);
	}
};