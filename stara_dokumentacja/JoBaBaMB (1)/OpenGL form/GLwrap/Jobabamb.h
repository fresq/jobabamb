#pragma once
#include "glfw.h"

class Jobabamb
{
private:
	float arms[4];
	float length[4];
	float krokowy;
	float chwytak;

private:
	GLUquadricObj *quadratic;

public:
	Jobabamb()
	{
		krokowy = 130;
		chwytak = -55;
		arms[0] = 0; 
		arms[1] = 0; 
		arms[2] = 0; 
		arms[3] = 0;
		length[0] = 9;
		length[1] = 9;
		length[2] = 9;
		length[3] = 9;
		quadratic = gluNewQuadric();          // Create A Pointer To The Quadric Object ( NEW )
		gluQuadricNormals(quadratic, GLU_SMOOTH);   // Create Smooth Normals ( NEW )
		gluQuadricTexture(quadratic, GL_TRUE);  
	}

	void DrawCh()
	{
		gluSphere(quadratic,1.5,16,16);
		glBegin(GL_QUADS);

			glNormal3f(0,0,1);
			glVertex3f(-0.7f,0.7f,1);
			glVertex3f(0.7f,-0.7f,1);
			glVertex3f(5,5.5f,1);
			glVertex3f(2.5f,4.7f,1);

			
			glNormal3f(-0.7,-0.7,0);
			glVertex3f(-0.7f,0.7f,1);
			glVertex3f(0.7f,-0.7f,1);
			glVertex3f(0.7f,-0.7f,-1);
			glVertex3f(-0.7f,0.7f,-1);

			
			glNormal3f(1,-1.2,0);
			glVertex3f(0.7f,-0.7f,1);
			glVertex3f(5,5.5f,1);
			glVertex3f(5,5.5f,-1);
			glVertex3f(0.7f,-0.7f,-1);

			
			glNormal3f(-0.7,2,0);
			glVertex3f(5,5.5f,1);
			glVertex3f(2.5f,4.7f,1);
			glVertex3f(2.5f,4.7f,-1);
			glVertex3f(5,5.5f,-1);

			
			glNormal3f(-1,1.2,0);
			glVertex3f(-0.7f,0.7f,1);
			glVertex3f(2.5f,4.7f,1);
			glVertex3f(2.5f,4.7f,-1);
			glVertex3f(-0.7f,0.7f,-1);

			
			glNormal3f(0,0,1);
			glVertex3f(2.5f,4.7f,1);
			glVertex3f(5,5.5f,1);
			glVertex3f(-0.7f,11.5f,1);
			glVertex3f(-0.7f,9.0f,1);

			
			glNormal3f(1,1,0);
			glVertex3f(5,5.5f,1);
			glVertex3f(-0.7f,11.5f,1);
			glVertex3f(-0.7f,11.5f,-1);
			glVertex3f(5,5.5f,-1);

			
			glNormal3f(-1,0,0);
			glVertex3f(-0.7f,11.5f,1);
			glVertex3f(-0.7f,9.0f,1);
			glVertex3f(-0.7f,9.0f,-1);
			glVertex3f(-0.7f,11.5f,-1);

			
			glNormal3f(-1,-1,0);
			glVertex3f(2.5f,4.7f,1);
			glVertex3f(-0.7f,9.0f,1);
			glVertex3f(-0.7f,9.0f,-1);
			glVertex3f(2.5f,4.7f,-1);

			
			glNormal3f(0,0,-1);
			glVertex3f(-0.7f,0.7f,-1);
			glVertex3f(0.7f,-0.7f,-1);
			glVertex3f(5,5.5f,-1);
			glVertex3f(2.5f,4.7f,-1);

			
			glNormal3f(0,0,-1);
			glVertex3f(2.5f,4.7f,-1);
			glVertex3f(5,5.5f,-1);
			glVertex3f(-0.7f,11.5f,-1);
			glVertex3f(-0.7f,9.0f,-1);

		glEnd();
	}

	void Draw(float x, float y, float z)
	{
		glTranslatef(x, y, z);
		glRotatef(krokowy, 0, 1, 0);
		glRotatef(90, 1, 0, 0);
		glTranslatef(0,0,3);

		glPolygonMode( GL_BACK, GL_FILL );          // Back Face Is Filled In
		glPolygonMode( GL_FRONT, GL_FILL );         // Front Face Is Drawn With Lines
		glColor3f(0.4,0.2,0.2);
		glScalef(6,6,2);
		glBegin(GL_QUADS);
			// Front Face
			glNormal3f( 0.0f, 0.0f, 1.0f);
			glVertex3f(-1.0f, -1.0f,  1.0f);
			glVertex3f( 1.0f, -1.0f,  1.0f);
			glVertex3f( 1.0f,  1.0f,  1.0f);
			glVertex3f(-1.0f,  1.0f,  1.0f);
			// Back Face
			glNormal3f( 0.0f, 0.0f,-1.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f,  1.0f, -1.0f);
			glVertex3f( 1.0f,  1.0f, -1.0f);
			glVertex3f( 1.0f, -1.0f, -1.0f);
			// Top Face
			glNormal3f( 0.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f,  1.0f, -1.0f);
			glVertex3f(-1.0f,  1.0f,  1.0f);
			glVertex3f( 1.0f,  1.0f,  1.0f);
			glVertex3f( 1.0f,  1.0f, -1.0f);
			// Bottom Face
			glNormal3f( 0.0f,-1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
			glVertex3f( 1.0f, -1.0f, -1.0f);
			glVertex3f( 1.0f, -1.0f,  1.0f);
			glVertex3f(-1.0f, -1.0f,  1.0f);
			// Right face
			glNormal3f( 1.0f, 0.0f, 0.0f);
			glVertex3f( 1.0f, -1.0f, -1.0f);
			glVertex3f( 1.0f,  1.0f, -1.0f);
			glVertex3f( 1.0f,  1.0f,  1.0f);
			glVertex3f( 1.0f, -1.0f,  1.0f);
			// Left Face
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f,  1.0f);
			glVertex3f(-1.0f,  1.0f,  1.0f);
			glVertex3f(-1.0f,  1.0f, -1.0f);
		glEnd();
		glScalef(1/6.0f,1/6.0f,0.5);
		glPolygonMode( GL_BACK, GL_LINE );          // Back Face Is Filled In
		glPolygonMode( GL_FRONT, GL_LINE );         // Front Face Is Drawn With Lines,

		glColor3f(1,1,0);
		glTranslatef(0,0,-3);
		glRotatef(-90, 1, 0, 0);
		for(int i = 0; i < 4; i++)
		{
			gluSphere(quadratic,3.0f,16,16); 
			glRotatef(arms[i], 0 ,0, 1);
			glRotatef(-90, 1, 0, 0);
			gluCylinder(quadratic,2.0f,2.0f,length[i],16,16);
			glRotatef(90, 1, 0, 0);
			glTranslatef(0, length[i], 0);
		}

		
		glPolygonMode( GL_BACK, GL_FILL );          // Back Face Is Filled In
		glPolygonMode( GL_FRONT, GL_FILL );         // Front Face Is Drawn With Lines,
		glColor3f(0.6,0.6,0.6);
		glRotatef(-(chwytak+90),1,0,0);
		glRotatef(90,0,1,0);
		DrawCh();
		glRotatef(-90,0,1,0);
		glRotatef(2*(chwytak+90),1,0,0);
		glRotatef(-90,0,1,0);
		DrawCh();
	}

	void SetAngle(float a1, float a2, float a3, float a4, float ch)
	{
		chwytak = ch;
		arms[0] = a1;
		arms[1] = a2;
		arms[2] = a3;
		arms[3] = a4;
	}

	void SetA(int i, int a)
	{
		arms[i] = a;
	}

	void SetK(int k)
	{
		krokowy = k;
	}
	
	void SetCh(int ch)
	{
		chwytak = ch;
	}

	void ModAngle(float a1, float a2, float a3, float a4, float ch)
	{
		chwytak += ch;
		arms[0] += a1;
		arms[1] += a2;
		arms[2] += a3;
		arms[3] += a4;
	}

	void ModA(int i, int a)
	{
		arms[i] += a;
	}

	void ModK(int k)
	{
		krokowy += k;
	}

	void ModCh(int ch)
	{
		chwytak += ch;
	}
};