#define GLFW_DLL

#include "Screen.h"
#include <cstdlib>

int Screen::Width;
int Screen::Height;

int GLFWCALL Close()
{
	glfwCloseWindow();
	glfwTerminate();
	exit(0);
	return 0;
}

Screen::Screen(int width, int height, bool fullscreen)
{
	this->SetWindowSize(width,height);

	glfwInit();
	glfwOpenWindow(Width,Height,8,8,8,0,0,0,(fullscreen)?GLFW_FULLSCREEN:GLFW_WINDOW);
	glfwEnable(GLFW_MOUSE_CURSOR);
	glfwSetWindowTitle("JoBaBaMB");
	glfwSetWindowCloseCallback(Close);
	//glfwSetKeyCallback(Input::InputManager::KeyCallback);
	glfwSetWindowSizeCallback(Screen::ResizeGLScene);
	//glfwSetMousePosCallback(Input::InputManager::MouseCallback);
	//glfwSetMouseButtonCallback(Input::InputManager::MouseButtonCallback);
	//glfwSetCharCallback(Input::InputManager::CharCallback);
	glViewport(0,0,Width,Height);						// Reset The Current Viewport
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,0.1f,1000.0f);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix

	//glEnable(GL_TEXTURE_2D);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void GLFWCALL Screen::ResizeGLScene(int width, int height)
{
	Screen::SetWindowSize(width,height);

	glViewport(0,0,Width,Height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix
	gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,0.1f,1000.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix

	glfwSwapBuffers();
}

void Screen::SetWindowSize(int width, int height)
{
	if(width > 0) Width = width; else width = 1;
	if(height > 0) Height = height; else height = 1;
}

void Screen::OpenWindow(int width, int height, bool fullscreen)
{
	glfwOpenWindow(width,height,8,8,8,0,0,0,(fullscreen)?GLFW_FULLSCREEN:GLFW_WINDOW);
}

void Screen::KillWindow()
{
	glfwCloseWindow();
}