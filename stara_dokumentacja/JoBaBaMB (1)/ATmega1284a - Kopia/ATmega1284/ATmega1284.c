﻿#define F_CPU 16000000

#include <stdint.h> 
#include <stdbool.h> 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include "LCD4.h"
#include "Serwa.h"
#include "USART.h"
//#include "USART1.h"
#include "Kinematyka.h"
#include "rc5.h"
#include "Praktyczne.h"
#include "Krokowy.h"
#include "Pompka.h"
#include "ADC.h"
#include "Jazda.h"
#include "blue.h"
#include "buzzer.h"


void ShowByte(int8_t b)
{
	//LCD_WriteText((b & (1 << 8)) ? "1" : "0");
	LCD_WriteText((b & (1 << 7)) ? "1" : "0");
	LCD_WriteText((b & (1 << 6)) ? "1" : "0");
	LCD_WriteText((b & (1 << 5)) ? "1" : "0");
	LCD_WriteText((b & (1 << 4)) ? "1" : "0");
	LCD_WriteText((b & (1 << 3)) ? "1" : "0");
	LCD_WriteText((b & (1 << 2)) ? "1" : "0");
	LCD_WriteText((b & (1 << 1)) ? "1" : "0");
	LCD_WriteText((b & (1 << 0)) ? "1" : "0");
	LCD_WriteText("||");
}

void u8ToText(char *text, int a)
{
	text[0] = (a<0) ? '-' : ' ';
	
	a = (a<0) ? -a : a;
	
	text[1] = (a/10000000) % 10 + '0';
	text[2] = (a/1000000) % 10 + '0';
	text[3] = (a/100000) % 10 + '0';
	text[4] = (a/10000) % 10 + '0';
	text[5] = (a/1000) % 10 + '0';
	text[6] = (a/100) % 10 + '0';
	text[7] = (a/10) % 10 + '0';
	text[8] = (a) % 10 + '0';
}

void GetAngle4(char *text, int angle)
{
	text[0] = (angle<0) ? '-' : ' ';
	
	angle = (angle<0) ? -angle : angle;
	
	text[1] = (angle/1000) % 10 + '0';
	text[2] = (angle/100) % 10 + '0';
	text[3] = (angle/10) % 10 + '0';
	text[4] = (angle) % 10 + '0';
}

void GetAngle3(char *text, int angle)
{
	text[0] = (angle<0) ? '-' : ' ';
	
	angle = (angle<0) ? -angle : angle;
	
	text[1] = (angle/100) % 10 + '0';
	text[2] = (angle/10) % 10 + '0';
	text[3] = (angle) % 10 + '0';
}

void GetAngle2(char *text, int angle)
{
	text[0] = (angle<0) ? '-' : ' ';
	
	angle = (angle<0) ? -angle : angle;
	
	text[1] = (angle/10) % 10 + '0';
	text[2] = (angle) % 10 + '0';
	//text[3] = (angle) % 10 + '0';
}

void ShowAngle(int a)
{
	char text[] = "+0000";
	
	GetAngle4(text,a);

	LCD_GoTo(0,1);
	LCD_WriteText(text);
}

void ShowMode(int mode)
{
	LCD_GoTo(8,1);
	if(mode == 0) LCD_WriteText("FREE");
	else if(mode == 1) LCD_WriteText("AUTO");
	else if(mode == 2) LCD_WriteText("FLAT");
	else if(mode == 14)LCD_WriteText("DUPA14");
	else if(mode == 13)LCD_WriteText("DUPA13");
	else if(mode == 12)LCD_WriteText("DUPA12");
	else if(mode == 18)LCD_WriteText("DUPA18");
}

void Show3Angle(int a, int b, int c, int d)
{
	char text[] = "+000";
	LCD_GoTo(0,0);
	
	GetAngle3(text,a);
	LCD_WriteText(text);
	LCD_WriteText(" ");
	GetAngle3(text,b);
	LCD_WriteText(text);
	LCD_WriteText(" ");
	GetAngle3(text,c);
	LCD_WriteText(text);
	LCD_WriteText(" ");
	GetAngle3(text,d);
	LCD_WriteText(text);
}

void ShowAllAngle(arm* a)
{
	int i, b;
	LCD_GoTo(0,0);
	for(i = 0;  i < COUNT; i++)
	{
		b = (int)(a->tab[i].ang);
		LCD_WriteInt(b);
		LCD_WriteText(" ");
	}		
	LCD_WriteText("K(");
	LCD_WriteInt(mKrok);
	LCD_WriteText(", ");
	LCD_WriteInt(mMicroKrok);
	LCD_WriteText(")  ");
	
}

int MainMenu()
{
	char* tab[] = {"Nalej wodki", "Zapamietane Pozycje", "Kalibracja Srerv", "Reczna manipulacja", "Odczyt z pilota", "Zwierzeta", "Pompka", "Znajdz zero", "Komunikacja", "Napiecie", "Jazda", "Blue"};
	int max = 12;
	int aktualny = 0;
	int zMin = 0, zMax = 3;
	int cmd;
	int toogle;

	LCD_Clear();
	for(int i = 0; i < 4; i++)
	{
		LCD_GoTo(0,i);
		if(i+zMin == aktualny) LCD_WriteText("->");
		LCD_WriteText(tab[zMin+i]);
	}	
	
	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return -1;
		if(cmd == KEY_OK) return aktualny;
		if(cmd > KEY_1 && cmd <= max) return cmd - 1;
		if(cmd == KEY_DOWN)
		{
			aktualny++; if(aktualny >= max) {aktualny = 0; zMin = 0; zMax = 3;}
			if(aktualny > zMax) {zMax++; zMin++;}
		}
		else if(cmd == KEY_UP)
		{
			aktualny--; if(aktualny < 0) {aktualny = max - 1; zMin = max - 4; zMax = max - 1;}		
			if(aktualny < zMin) {zMin--; zMax++;}	
		}
		
		LCD_Clear();
		for(int i = 0; i < 4; i++)
		{
			LCD_GoTo(0, i);
			if(i+zMin == aktualny) LCD_WriteText("->");
			LCD_WriteText(tab[zMin+i]);
		}
		
	}
}

void RecznaManipulacja(arm* a)
{
	int cmd, toogle;
	int wybrane = 0;
	int dt = 1;
	LCD_Clear();
	//for (int i = 0; i < 4; i++)
	//{
		//a->tab[i].ang = 0;
	//}
	Move(*a);
	ShowAllAngle(a);
	//Show3Angle(0,0,0,0);
	ShowAngle(dt);
	LCD_GoTo(0,2);
	LCD_WriteText("Wybrane: ");
	LCD_WriteInt(wybrane+1);	
	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return;
		if(cmd == KEY_1) wybrane = 0;
		if(cmd == KEY_2) wybrane = 1;
		if(cmd == KEY_3) wybrane = 2;
		if(cmd == KEY_4) wybrane = 3;
		if(cmd == KEY_5) wybrane = 4;
		if(cmd == KEY_6) wybrane = 5;
		if(cmd == KEY_UP) a->tab[wybrane].ang -= dt;
		if(cmd == KEY_DOWN) a->tab[wybrane].ang += dt;
		if(cmd == KEY_LEFT) { Krok(1);}
		if(cmd == KEY_RIGHT) { Krok(0);}
		//if(cmd == KEY_UP) { dt += 1; ShowAngle(dt);}
		//if(cmd == KEY_DOWN) { dt -= 1; ShowAngle(dt);}
		//if(a->tab[0].ang > 60) {a->tab[0].ang = 60;}
		//if(a->tab[0].ang < -60) {a->tab[0].ang = -60;}
		
		Set(a);
		Move(*a);
		LCD_GoTo(0,2);
		LCD_WriteText("Wybrane: ");
		LCD_WriteInt(wybrane+1);
		//Show3Angle(a->tab[0].ang, a->tab[1].ang, a->tab[2].ang,a->tab[3].ang);
		ShowAllAngle(a);
		_delay_ms(50);
	}
}

#define PS2_DATA_PORT PORTE
#define PS2_DATA_DIR DDRE
#define PS2_DATA_PIN _BV(2)

#define PS2_CLOCK_PORT PORTE
#define PS2_CLOCK_DIR DDRE
#define PS2_CLOCK_PIN _BV(6)

void NalejWodki(Memory* m, arm* a)
{
	PS2_DATA_DIR &= ~PS2_DATA_PIN;
	PS2_DATA_PORT |= ~PS2_DATA_PIN;
	PS2_CLOCK_DIR &= ~PS2_CLOCK_PIN;
	PS2_CLOCK_PORT |= ~PS2_CLOCK_PIN;
	uint8_t data = 0;
	int start = 0;
	int count = 0;
	LCD_Clear();
	while(1)
	{
		if(!(PINE & PS2_CLOCK_PIN))
		{
			_delay_us(20);
			if(!start)
			{
				if(!(PINE & PS2_DATA_PIN))
				{
					start = 1;
					count = 0;
				}
			}
			else
			{
				count ++;
				// odczytaj bit danych
				if(count <= 8)
				{
					data = data << 1;
					if(PINE & PS2_DATA_PIN)
						data |= 1;
					else
						data &= ~1;
				}
				else
				{
					count = 0;
					LCD_WriteInt(data);
					LCD_WriteText("|");
					if(data == 110)
						LCD_Clear();
					//ShowByte(data);
					start = 0;
				}
			}
			_delay_us(40);
		}
	}
	
	
	
	LoadFromMemory(a, m->tab[2]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[3]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[5]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[5]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[6]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	
	/*SetAllAngle(a,0,0,0,0,5);
	ZerujKrokowy();
	//Do kieliszka wyprostowany
	KroczDo(164,0);
	//Ugjecie nad kieliszkiem
	SetSlowAngle(a,48,-86,-73,34,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,150,150,150,75, -80, 5);
	Set(a);
	Move(*a);	
	//Pierwsza pozycja
	KroczDo(173,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a); 
	Move(*a);
	_delay_ms(200);
	//dostawienie
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,35,150,150,150,-40,5);
	Set(a);
	Move(*a);
	KroczDo(164,0);
	_delay_ms(200);
	//ugiecie sie do 2 kieliszka
	SetSlowAngle(a,30,-85,-81,58,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,150,150,150,93, -80, 5);
	Set(a);
	Move(*a);
	//druga pozycja
	KroczDo(180,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a);
	Move(*a);
	_delay_ms(200);
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);	
	//podniesienie
	SetSlowAngle(a,35,150,150,150,-40,5);
	Set(a);
	Move(*a);
	KroczDo(164,0);
	_delay_ms(200);
	//ugiecie sie do 3 kieliszka
	SetSlowAngle(a,15,-85,-81,68,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,50,-85,-81,70, -80, 5);
	Set(a);
	Move(*a);  
	//druga pozycja
	KroczDo(184,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a);
	Move(*a);
	_delay_ms(200);
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);
	SetSlowAngle(a,0,0,0,0,-55,5);
	Set(a);
	Move(*a);*/
}

void Zapamietane(Memory* m, arm* a)
{
	int max = m->ile;
	int aktualny = 0;
	int zMin = 0, zMax = 3;
	int cmd;
	int toogle;

	LCD_Clear();
	for(int i = 0; i < 4; i++)
	{
		LCD_GoTo(0,i);
		if(i+zMin == aktualny) LCD_WriteText("->");
		LCD_WriteText(m->tab[zMin+i]->nazwa);
	}
	
	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return -1;
		if(cmd == KEY_OK) 
		{
			LoadFromMemory(a, m->tab[aktualny]);
			Move(*a);		
		}
		if(cmd == KEY_DOWN)
		{
			aktualny++; if(aktualny >= max) {aktualny = 0; zMin = 0; zMax = 3;}
			if(aktualny > zMax) {zMax++; zMin++;}
		}
		else if(cmd == KEY_UP)
		{
			aktualny--; if(aktualny < 0) {aktualny = max - 1; zMin = max - 4; zMax = max - 1;}
		}
		
		LCD_Clear();
		for(int i = 0; i < 4; i++)
		{
			LCD_GoTo(0, i);
			if(i+zMin == aktualny) LCD_WriteText("->");
			LCD_WriteText(m->tab[zMin+i]->nazwa);
		}
		
	}
}

void KalibracjaServ()
{
	int cmd, toogle;
	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return;
		
		
	}
}

void KalibracjaPilota()
{
	int cmd, toogle;
	while (1)
	{
		// Wykrywa i dekoduje polecenie pilota RC5
		cmd = detect();
		toogle = cmd & 2048;
		cmd &= ~2048;
		LCD_Clear();
		LCD_WriteInt(cmd);
		if(toogle) { LCD_GoTo(0,1); LCD_WriteText("Toogle"); }
		
		_delay_ms(500);
		if (cmd == KEY_ESC)
			break;
	}	
}

int PetMenu()
{
	char* tab[] = {"Niuchacz", "Waz", "Panda", "Inne", "Koniec"};
	int max = 5;
	int aktualny = 0;
	int zMin = 0, zMax = 3;
	int cmd;
	int toogle;

	LCD_Clear();
	for(int i = 0; i < 4; i++)
	{
		LCD_GoTo(0,i);
		if(i+zMin == aktualny) LCD_WriteText("->");
		LCD_WriteText(tab[zMin+i]);
	}

	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return -1;
		if(cmd == KEY_OK) return aktualny;
		if(cmd == KEY_DOWN)
		{
			aktualny++; if(aktualny >= max) {aktualny = 0; zMin = 0; zMax = 3;}
			if(aktualny > zMax) {zMax++; zMin++;}
		}		
		else if(cmd == KEY_UP)
		{
			aktualny--; if(aktualny < 0) {aktualny = max - 1; zMin = max - 4; zMax = max - 1;}
		}
		
		LCD_Clear();
		for(int i = 0; i < 4; i++)
		{
			LCD_GoTo(0, i);
			if(i+zMin == aktualny) LCD_WriteText("->");
			LCD_WriteText(tab[zMin+i]);
		}
		
	}	
}

void Zwierzeta(arm* a)
{
	int cmd;
	while(1)
	{
		_delay_ms(100);
		int pet = PetMenu();
		_delay_ms(100);
		if(pet == -1 || pet == 4) return;
		if(pet == 0)//Niuchacz
		{
			SetSlowAngle(a,-22,-39,-43,29,-55,5);
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[3].ang = (rand() % 30) - 15 + 29;
				a->tab[4].ang = (rand() % 30) - 15 - 55;
				Move(*a);
				_delay_ms(50);
			}
		}
		else if(pet == 1) //Waz
		{
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[0].ang = 40;
				a->tab[1].ang = -40;
				a->tab[2].ang = 40;
				a->tab[3].ang = 40;
				Move(*a);
				_delay_ms(200);
				a->tab[0].ang = -40;
				a->tab[1].ang = 40;
				a->tab[2].ang = -40;
				a->tab[3].ang = -40;
				Move(*a);
				_delay_ms(200);
			}			
		}
		else if(pet == 2)//panda
		{
			LCD_Clear();
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,1);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,2);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,3);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,4);
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[0].ang = 30;
				a->tab[1].ang = 80;
				a->tab[2].ang = -80;
				a->tab[3].ang = -45 ;
				Move(*a);
				_delay_ms(200);
				a->tab[0].ang = 35;
				a->tab[1].ang = 75;
				a->tab[2].ang = -70;
				a->tab[3].ang = -40;
				_delay_ms(200);
				Move(*a);
			}	
		}
	}
}

void Pompka()
{
	int cmd;
	LCD_Clear();
	while(1)
	{
		while((cmd = detect()) < 0);
		cmd &= ~2048;
		if(cmd == KEY_ESC) return;
		if(cmd == KEY_LEFT) { PompkaWypelnienieMod(-1);}
		if(cmd == KEY_RIGHT) { PompkaWypelnienieMod(1);}
		if(cmd == KEY_8) {PompkaWypelnienie(0);}
		if(cmd == KEY_9) {PompkaWypelnienie(255);}
		LCD_GoTo(0,0);
		LCD_WriteInt(wypelnienie);
		LCD_WriteText("    ");
		_delay_ms(100);
	}
}

void Komunikacja(arm *a)
{
	while(1);
	//while(1)
	//{
		//if(flag)
		//{
			//if(znak != 0 && znak != 60)
				//ShowByte(znak);
			//flag = 0;
			//if(znak == 48)
			//{
				//LCD_Clear();
			//}
		//}
	//}
	//LCD_Clear();
	//int cmd,toogle;
	//while(1)
	//{
		//cmd = detect();
		//if(cmd >= 0)
		//{
			//toogle = cmd & 2048;
			//cmd &= ~2048;
			//if(cmd == KEY_ESC) break;
			////if(cmd == KEY_OK) USART_SendText("JoBaBaMB ");
		//}
		//if(flag == 1)
		//{
			//LCD_Clear();
			//while(znak != 126);
			//int polecenie = buffer[0];
			//LCD_WriteText("Polecenie: ");
			//LCD_WriteInt(polecenie);
			//LCD_GoTo(0,1);
			//if(polecenie == 1)
			//{
				//LCD_GoTo(0,1);
				//int a1 = buffer[1]*(buffer[2] ? -1 : 1);
				//int a2 = buffer[3]*(buffer[4] ? -1 : 1);
				//int a3 = buffer[5]*(buffer[6] ? -1 : 1);
				//int a4 = buffer[7]*(buffer[8] ? -1 : 1);
				//int a5 = buffer[9]*(buffer[10] ? -1 : 1);
				//LCD_WriteInt(a1); LCD_WriteText(" ");
				//LCD_WriteInt(a2); LCD_WriteText(" ");
				//LCD_WriteInt(a3); LCD_WriteText(" ");
				//LCD_WriteInt(a4); LCD_WriteText(" ");
				//LCD_WriteInt(a5); LCD_WriteText(" ");
				//
				//int miliseconds = buffer[11] * 1 +  buffer[12] * 10 +  buffer[13] * 100 +  buffer[14] * 1000;
				//SetSlowAngle(a,a1,a2,a3,a4,a5,miliseconds);
			//}
			//else if(polecenie == 2)
			//{
								//
			//}
			//else if(polecenie == 3)
			//{
				//Krocz(buffer[1],buffer[2]);	
				//LCD_WriteInt(buffer[1]); LCD_WriteText(" "); LCD_WriteInt(buffer[2]);
			//}
			//else if(polecenie == 4)
			//{
				//KrokowySetCoils(buffer[1],buffer[2],buffer[3],buffer[4]);
				//LCD_WriteInt(buffer[1]); LCD_WriteText(" "); LCD_WriteInt(buffer[2]);
				//LCD_WriteText(" "); LCD_WriteInt(buffer[3]);
				//LCD_WriteText(" "); LCD_WriteInt(buffer[4]);
			//}
			//else if(polecenie == 5)
			//{
				//int servo = buffer[1];
				//int useconds = buffer[2] * 1 +  buffer[3] * 10 +  buffer[4] * 100 +  buffer[5] * 1000;
				//if(servo >= 0 && servo <= N_SERVOS && useconds > 200 && useconds < 3000)
					//servoSet(servo, useconds);
				//LCD_WriteInt(servo);
				//LCD_WriteText(" ");
				//LCD_WriteInt(useconds);
			//}
			//else if(polecenie == 6)
			//{
				//int miliseconds = buffer[1] * 1 +  buffer[2] * 10 +  buffer[3] * 100 +  buffer[4] * 1000;
				//for(int i = 0; i < miliseconds; i++)
				 //_delay_ms(1);
				//LCD_WriteInt(miliseconds);
			//}
			//else if(polecenie == 7)
			//{
				//int w = buffer[1] * 1 +  buffer[2] * 10;
				//int mili = buffer[3] * 1 +  buffer[4] * 10 +  buffer[5] * 100;
				//LCD_WriteInt(w); LCD_WriteText("  "); LCD_WriteInt(mili);
				//PompkaWypelnienie(w);
				//for(int i = 0; i < mili; i++)
					//_delay_ms(1);
				//PompkaWypelnienie(0);
				//
			//}
			//else if(polecenie == 8)
			//{
				//ZerujKrokowy();				
			//}
			//flag = 0;
			//bufferlocation = 0;
		//}
		//
	//}
}	

void Napiecie()
{
	int cmd;
	LCD_Clear();
	//int ile = 0;
	while(1)
	{
		cmd = detect();
		if(cmd > 0)
		{
			cmd &= ~2048;
			if(cmd == KEY_ESC) return;
			//if(cmd == KEY_RIGHT)
			//{
				//for(int i = 0; i < 64; i++)
				//{
					//Krok(1);
				//}
				//ile++;
			//}
			//if(cmd == KEY_LEFT)
			//{
				//for(int i = 0; i < 64; i++)
				//{
					//Krok(0);
				//}
				//ile--;
			//}			
		}
		LCD_GoTo(0,0);
		LCD_WriteInt((int)(V*100));
		_delay_ms(100);
	}
}

void MJazda()
{
	int cmd;
	LCD_Clear();
	//int ile = 0;
	while(1)
	{
		cmd = detect();
		if(cmd > 0)
		{
			cmd &= ~2048;
			if(cmd == KEY_ESC) 
			{
				Jazda(0,0);
				return;
			}
			if(cmd == KEY_LEFT)
			{
				JazdaP(0.9f,-0.1f);
			}
			if(cmd == KEY_RIGHT)
			{
				JazdaP(-0.1f,0.9f);
			}
			if(cmd == KEY_UP)
			{
				JazdaP(0.8f,0.8f);
			}
			if(cmd == KEY_DOWN)
			{
				JazdaP(-0.8f,-0.8f);
			}		
			if(cmd == KEY_1)
			{
				JazdaP(-0.8f,0);
			}
			if(cmd == KEY_2)
			{
				JazdaP(0,-0.8f);
			}	
			if(cmd == KEY_3)
			{
				JazdaP(0.8f,0);
			}
			if(cmd == KEY_4)
			{
				JazdaP(0,0.8f);
			}					
		}
		_delay_ms(100);
	}
}
//unsigned long int EEMEM seed = 0xA5F39AC0;

int main(void) 
{
	srandom(56189741);
	
	
	LCD_Initalize();
	
	
	_delay_ms(1000);
	
    uint8_t i; 
    for(i = 0; i < N_SERVOS; i++) { 
        servoTime[i] = US2TIMER1(SERVO0_MID); 
    }
    

	
	init_rc5();
    servoStart();
	InitKrokowy();
	//ADCInit();
	//USART1Init();
	USART0Init();
	InitPompka();
	InitJazda();
    sei();
	

	arm a; //manipulator
	Init(&a);
	Move(a);
	
	Memory m;
	InitMemory(&m);
	LCD_Clear();
	
	a.tab[0].ang = 0;
	a.tab[1].ang = 0;
	a.tab[2].ang = 0;
	a.tab[3].ang = 0;
	a.tab[4].ang = 0;
	a.tab[5].ang = 0;


  
	//playMusic();
  
	while(1)
	{
		int wynik = MainMenu();
		
		if (wynik == 0)
		{
			NalejWodki(&m, &a);
		}
		else if (wynik == 1)
			Zapamietane(&m, &a);
		else if (wynik == 2)
			KalibracjaServ();
		else if (wynik == 3)
			RecznaManipulacja(&a);
		else if (wynik == 4)
			KalibracjaPilota();
		else if (wynik == 5)
			Zwierzeta(&a);
		else if(wynik == 6)
			Pompka();
		else if(wynik == 7)
			ZerujKrokowy();
		else if(wynik == 8)
			Komunikacja(&a);
		else if(wynik == 9)
			Napiecie();
		else if(wynik == 10)
			MJazda();
		else if(wynik == 11)
			BluetoothComunication(&a);
	
	}
}

//ISR(TIMER2_COMP_vect) 
//{ 
    //static uint16_t nextStart; 
    //static uint8_t servo; 
    //static bool outputHigh = true; 
    //uint16_t currentTime = OCR2; 
    //uint8_t mask = servoOutMask[servo]; 
    //
    //if (outputHigh) { 
        //SERVO_PORT |= mask; 
        //// Set the end time for the servo pulse 
        //OCR2 = currentTime + servoTime[servo]; 
        //nextStart = currentTime + US2TIMER1(SERVO_TIME_DIV); 
    //} else { 
        //SERVO_PORT &= ~mask; 
        //if (++servo == N_SERVOS) { 
            //servo = 0; 
        //} 
        //OCR2 = nextStart; 
    //} 
    //outputHigh = !outputHigh; 
//}

 ISR(TIMER2_COMP_vect) 
 {
     static uint16_t nextStart; 
     static uint8_t servo; 
     static bool outputHigh = true; 
	 static uint16_t timeBuffer = 0;
	
	 if(timeBuffer > 0)
	 {
		 OCR2 = 255;
		 timeBuffer -= 255;
	 }
	 else
	 {	
		 uint16_t currentTime = OCR2; 
		 uint8_t mask = servoOutMask[servo]; 
		
		 if (outputHigh) 
		 { 
			 SERVO_PORT |= mask; 
			 // Set the end time for the servo pulse 
			 timeBuffer = currentTime + servoTime[servo]; 
			 nextStart = currentTime + US2TIMER1(SERVO_TIME_DIV); 
			
			 if(timeBuffer / 255 <= 1)
			 {
				 OCR2 = timeBuffer;
				 timeBuffer = 0;				 
			 }
			 else
			 {
				 OCR2 = timeBuffer % 255;
				 timeBuffer = timeBuffer - timeBuffer % 255 - 255;
			 }
				
		 } else { 
			 SERVO_PORT &= ~mask; 
			 if (++servo == N_SERVOS) { 
				 servo = 0; 
			 }
			 timeBuffer = nextStart;
			 if(timeBuffer / 255 <= 1)
			 {
				 OCR2 = timeBuffer;
				 timeBuffer = 0;
			 }
			 else
			 {
				 OCR2 = timeBuffer % 255;
				 timeBuffer = timeBuffer - timeBuffer % 255 - 255;
			 }	
		 } 
		 outputHigh = !outputHigh; 
	 }
 }

//---------------------------------------------------------------
// Procedura obsługi przerwania  Timer0 Overflow"
//---------------------------------------------------------------
ISR(TIMER0_COMP_vect)
{
	volatile  static u8 inttemp;

	// zmienna timerL zwiększa się co 32us
	timerL++;

	// zmienna timerH  zwiększa się co 8.192ms (32us*256)
	inttemp++;
	if(!inttemp ) timerH++;
}



