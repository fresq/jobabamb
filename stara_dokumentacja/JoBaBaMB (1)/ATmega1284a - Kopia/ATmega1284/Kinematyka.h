﻿/*
 * kinematyka->h
 *
 * Created: 2012-05-10 21:22:07
 *  Author: Paweł Joniak
 */ 

#include <math.h>
#include <stdlib.h>
#include "Praktyczne.h"

#ifndef KINEMATYKA_H_
#define KINEMATYKA_H_

#define RAD 3.141592f/180.0f

#define SECONDS(s) (long)((long)(s[0]*10+s[1])*3600 + ((long)s[3]*10+s[4])*60 + (long)(s[6]*10+s[7])*1 - (long)(11*3661*'0'))

#define COUNT 6
#define LIMIT 85


typedef struct
{
 float len;
 float ang,arg;
 float x,y;
} joint;


typedef struct
{
 joint tab[COUNT]; 
 float ex,ey;
 int tryb;
 
} arm;


void Set(arm* a)
{
	a->tab[0].arg = a->tab[0].ang;
	for(int i=0; i<COUNT-1; i++) //mogloby byc do COUNT a nie do COUNT-1
	{
		a->tab[i+1].arg = a->tab[i].arg + a->tab[i+1].ang;
		a->tab[i+1].x = a->tab[i].x + a->tab[i].len * cosf(a->tab[i].arg * RAD);
		a->tab[i+1].y = a->tab[i].y + a->tab[i].len * sinf(a->tab[i].arg * RAD);
	}
	a->ex = a->tab[COUNT-1].x + a->tab[COUNT-1].len * cosf(a->tab[COUNT-1].arg * RAD);
	a->ey = a->tab[COUNT-1].y + a->tab[COUNT-1].len * sinf(a->tab[COUNT-1].arg * RAD);
}

void SetAllAngle(arm* a, int a1, int a2, int a3, int a4, int a5)
{
	if(a1 != 150) a->tab[0].ang = a1;
	if(a2 != 150) a->tab[1].ang = a2;
	if(a3 != 150) a->tab[2].ang = a3;
	if(a4 != 150) a->tab[3].ang = a4;
	if(a5 != 150) a->tab[4].ang = a5;
}

void SetSlowAngle(arm* a, int a1, int a2, int a3, int a4, int a5, int ms)
{
	int i;
	float a12 = 0,a22 = 0, a32 = 0, a42 = 0, a52 = 0;
	
	int steps = ms / 50;
	if(a1 != 150) a12 = (a1 - a->tab[0].ang) / steps;
	if(a2 != 150) a22 = (a2 - a->tab[1].ang) / steps;
	if(a3 != 150) a32 = (a3 - a->tab[2].ang) / steps;
	if(a4 != 150) a42 = (a4 - a->tab[3].ang) / steps;
	if(a5 != 150) a52 = (a5 - a->tab[4].ang) / steps;
	for(i = 0; i < steps; i++)
	{
		a->tab[0].ang += a12;
		a->tab[1].ang += a22;
		a->tab[2].ang += a32;
		a->tab[3].ang += a42;
		a->tab[4].ang += a52;
		Set(a);
		Move(*a);
		_delay_ms(50);
	}
}


void SetSlowOneAngle(arm* a, int nr, int angle, int ms)
{
	int i;
	float a12 = 0;
	
	int steps = ms / 50;
	a12 = (angle - a->tab[nr].ang) / steps;
	for(i = 0; i < steps; i++)
	{
		a->tab[nr].ang += a12;
		Set(a);
		Move(*a);
		_delay_ms(50);
	}
}

void Init(arm* a)
{
	a->tab[0].len = 12;
	a->tab[0].ang = 0;
	a->tab[0].arg = 0;
	
	a->tab[1].len = 12.5;
	a->tab[1].ang = 0;
	a->tab[1].arg = 0;
	
	a->tab[2].len = 13.5;
	a->tab[2].ang = 0;
	a->tab[2].arg = 0;
	
	a->tab[3].len = 9.7;
	a->tab[3].ang = 0;
	a->tab[3].arg = 0;
	
	a->tab[4].len = 9.7;
	a->tab[4].ang = -55;
	a->tab[4].arg = 0;

	
	
	a->tryb = 0;

	Set(a);
}

void Move(arm a)
{
	servoAngle4(a.tab[0].ang-82);
	servoAngle5(-a.tab[1].ang-2);
	servoAngle6(a.tab[2].ang-13);
	servoAngle1(a.tab[3].ang-18);
	servoAngle0(a.tab[4].ang);
	servoAngle7(a.tab[5].ang);
	//if(a.tryb == 2) servoAngle3(a.tab[1].arg + 100); else servoAngle3(a.tab[4].ang);
}

void LoadFromMemory(arm* a, elem* e)
{
	SetSlowAngle(a,e->a1,e->a2,e->a3,e->a4,e->a5,500);
}

float CheckX(arm* a, int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = a->ey;
	float x = a->ex;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = a->tab[id].x;
	float q1 = a->tab[id].y;

	//Pobieramy punkt pierwszego stawu
	float y0 = a->tab[id - 1].y;
	float x0 = a->tab[id - 1].x;

	//wyliczmy cosinus i sinus
	float c = cosf(alpha * 3.141592f / 180.0f);
	float s = sinf(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1)-(y-q)*(y-q);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		//jesli tak konczymy wyliczanie x
		podpierwiastek = sqrtf(podpierwiastek);
		float x2 = ( fabs(p + podpierwiastek - x) < fabs(p - podpierwiastek - x) ) ? (p + podpierwiastek) : (p - podpierwiastek);
		
		return x2-x;

	}
	else //gdy przsuniecie nie jest mozliwe
	{
		return 0;
	}	
}

float CheckY(arm* a, int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = a->ey;
	float x = a->ex;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = a->tab[id].x;
	float q1 = a->tab[id].y;

	//Pobieramy punkt pierwszego stawu
	float y0 = a->tab[id - 1].y;
	float x0 = a->tab[id - 1].x;

	//wyliczmy cosinus i sinus
	float c = cosf(alpha * 3.141592f / 180.0f);
	float s = sinf(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;
	
	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1) - (x-p)*(x-p);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		podpierwiastek = sqrtf(podpierwiastek);
		float y2 = ( fabs(q + podpierwiastek - y) < fabs(q - podpierwiastek - y) ) ? (q + podpierwiastek) : (q - podpierwiastek);

		return y2-y;
	}
	else
	{
		return 0;
	}	
}

bool GoX(arm* a, int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = a->ey;
	float x = a->ex;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = a->tab[id].x;
	float q1 = a->tab[id].y;

	//Pobieramy punkt pierwszego stawu
	float y0 = a->tab[id - 1].y;
	float x0 = a->tab[id - 1].x;

	//wyliczmy cosinus i sinus
	float c = cosf(alpha * 3.141592f / 180.0f);
	float s = sinf(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	float x2 = (x-x0)*c - (y-y0)*s + x0;
	float y2 = (x-x0)*s + (y-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1)-(y-q)*(y-q);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		//jesli tak konczymy wyliczanie x
		podpierwiastek = sqrtf(podpierwiastek);
		x = ( fabs(p + podpierwiastek - x) < fabs(p - podpierwiastek - x) ) ? (p + podpierwiastek) : (p - podpierwiastek);
			
		//wyliczmy kat zmiany dla drugiego stawu
		float beta = atan2f((y-q),(x-p)) - atan2f(y2-q,x2-p) ;
		beta = (beta*180.0f)/3.14159265f;
		//beta = beta - GetJoint(id).Arg();

		//Modyfikujemy go o wyliczony kąt
		if (fabs(a->tab[id - 1].ang + alpha) < LIMIT) 
		{
			a->tab[id - 1].ang += alpha;
			if (fabs(a->tab[id].ang + beta) < LIMIT) 
			{
				a->tab[id].ang += beta;
				Set(a);
				return true;
			}
			else
			{
				a->tab[id - 1].ang -= alpha;
				return false;
			}				
		}			
		else
		{
			return false;			
		}

	}
	else //gdy przsuniecie nie jest mozliwe
	{
		return false;
	}		
}

bool GoY(arm* a, int id, float alpha)
{
	//Zapamietujemy poziom ktorego mamy sie trzymac(punk receptora)
	float y = a->ey;
	float x = a->ex;

	//Pobieramy punkt pomiedzy dwoma stawami ktore zginamey
	float p1 = a->tab[id].x;
	float q1 = a->tab[id].y;

	//Pobieramy punkt pierwszego stawu
	float y0 = a->tab[id - 1].y;
	float x0 = a->tab[id - 1].x;

	//wyliczmy cosinus i sinus
	float c = cosf(alpha * 3.141592f / 180.0f);
	float s = sinf(alpha * 3.141592f / 180.0f);

	//wyliczamy nowe polozenie punktu pomiedzy(obracamy go o kont w ukladzie wzgletem P0(x0,y0)
	float p = (p1-x0)*c - (q1-y0)*s + x0;
	float q = (p1-x0)*s + (q1-y0)*c + y0;

	float x2 = (x-x0)*c - (y-y0)*s + x0;
	float y2 = (x-x0)*s + (y-y0)*c + y0;

	//Z rownania wyliczmy podpierwiastek
	float podpierwiastek = (x-p1)*(x-p1) + (y-q1)*(y-q1) - (x-p)*(x-p);

	//Kontrolujemy czy takie przemiestrzenie jest wogole mozliwe
	if (podpierwiastek >= 0.0f)
	{
		podpierwiastek = sqrt(podpierwiastek);
		y = ( fabs(q + podpierwiastek - y) < fabs(q - podpierwiastek - y) ) ? (q + podpierwiastek) : (q - podpierwiastek);

		float beta = atan2((y-q),(x-p)) - atan2(y2-q,x2-p);
		beta = (beta*180.0f)/3.14159265f;

		//Modyfikujemy go o wyliczony kąt
		if (fabs(a->tab[id - 1].ang + alpha) < LIMIT) 
		{
			a->tab[id - 1].ang += alpha;
			if (fabs(a->tab[id].ang + beta) < LIMIT) 
			{
				a->tab[id].ang += beta;
				Set(a);
				return true;
			}
			else
			{
				a->tab[id - 1].ang -= alpha;
				return false;
			}				
		}			
		else
		{
			return false;			
		}
	}
	else
	{
		return false;
	}		
}

void MoveY(arm* a, float y)
{
	for(int i = 0; i < 100; i++)
	{
		//Losujemy jedna z par do manipulacji
		int id = (random() % (COUNT-1)) + 1;
		float alpha = ((random() % 1000) - 500)/1000.0f; 
		//Sprawdzamy o ile by nastapilo przesuniecie
		float TryY = CheckY(a,id,alpha);
		//Sprawdzamy czy pasuje nam takie przesuniecie
		if(fabs(y-TryY) < fabs(y)) 
		{
			if(GoY(a,id, alpha))
			{
			//Modyfikujemy pozostala odleglosc
				y = y - TryY;
			//Zapisujemy zmieny
			}
		}
		if(fabs(y) < 0.1) break;
	}	
}

void MoveX(arm* a, float x)
{
	for(int i = 0; i < 100; i++)
	{
		//Losujemy jedna z par do manipulacji
		int id = (random() % (COUNT-1)) + 1;
		float alpha = ((random() % 1000) - 500)/1000.0f; 
		//Sprawdzamy o ile by nastapilo przesuniecie
		float TryX = CheckX(a,id,alpha);
		//Sprawdzamy czy pasuje nam takie przesuniecie
		if(fabs(x-TryX) < fabs(x)) 
		{
			//Zapisujemy zmieny
			if(GoX(a,id, alpha))
			{
			//Modyfikujemy pozostala odleglosc
				x = x -TryX;
			}
		}
		if(fabs(x) < 0.1) break;
	}

}



#endif /* KINEMATYKA_H_ */