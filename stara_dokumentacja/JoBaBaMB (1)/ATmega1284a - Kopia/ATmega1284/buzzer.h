/*
 * buzzer.h
 *
 * Created: 2013-11-30 22:49:10
 *  Author: Admin
 */ 


#ifndef BUZZER_H_
#define BUZZER_H_


#define p   0   //pause = silence
#define b3  (253 * 95)
#define c4  (239 * 95)
#define cx4 (225 * 95)
#define d4  (213 * 95)
#define dx4 (201 * 95)
#define e4  (190 * 95)
#define f4  (179 * 95)
#define fx4 (169 * 95)
#define g4  (159 * 95)
#define gx4 (150 * 95)
#define a4  (142 * 95)
#define ax4 (134 * 95)
#define b4  (127 * 95)
#define c5  (119 * 95)
#define cx5 (113 * 95)
#define d5  (106 * 95)
#define dx5 (100 * 95)
#define e5  (95 * 95)
#define f5  (89 * 95)
#define fx5 (84 * 95)
#define g5  (80 * 95)
#define gx5 (75 * 95)
#define a5  (71 * 95)
#define ax5 (67 * 95)
#define b5  (63 * 95)
#define c6  (60 * 95)
#define cx6 (56 * 95)
#define d6  (53 * 95)
#define dx6 (50 * 95)
#define e6  (47 * 95)
#define f6  (45 * 95)
#define fx6 (42 * 95)
#define g6  (40 * 95)
#define gx6 (38 * 95)
#define a6  (36 * 95)
#define b6  (32 * 95)
#define c7  (30 * 95)

/*
	unsigned char fuer_elise[]= //modified from AVR Butterfly code example. Thanks, ATMEL Norway!

	{		(120>>3),
		8,e5, 8,dx5, 8,e5, 8,dx5, 8,e5, 8,b4, 8,d5, 8,c5, 4,a4, 8,p,
		8,c4, 8,e4, 8,a4, 4,b4, 8,p, 8,e4, 8,gx4, 8,b4, 4,c5, 8,p, 8,e4,
		8,e5, 8,dx5, 8,e5, 8,dx5, 8,e5, 8,b4, 8,d5, 8,c5, 4,a4, 8,p, 8,c4,
		8,e4, 8,a4, 4,b4, 8,p, 8,e4, 8,c5, 8,b4, 4,a4,
		8,p,8,p,  //up one octave for test!
		8,e6, 8,dx6, 8,e6, 8,dx6, 8,e6, 8,b5, 8,d6, 8,c6, 4,a5, 8,p,
		8,c5, 8,e5, 8,a5, 4,b5, 8,p, 8,e5, 8,gx5, 8,b5, 4,c6, 8,p, 8,e5,
		8,e6, 8,dx6, 8,e6, 8,dx6, 8,e6, 8,b5, 8,d6, 8,c6, 4,a5, 8,p, 8,c5,
		8,e5, 8,a5, 4,b5, 8,p, 8,e5, 8,c6, 8,b5, 4,a5,
		0, 0
	};
	
	
	unsigned char chirp[]=  //bird chirp
	{ (18>>3),1,100,1,95,1,90,1,85,1,80,1,75,1,70,1,65,1,60,1,55,1,50,1,45,1,40,
	1,35,2,32,2,30,2,28,2,26,2,24,2,22,2,20,2,18,2,16, 0, 0};
	*/
	
	unsigned int sorcerer[]= //Sorcerer's Apprentice by Paul Dukas. 3/8 time, stored by bars #1-28
	{
		(210>>3),
		6, c4, 6, c4, 3, d4, 3, c4, 3, f4, 3, e4,
		6, c4, 6, c4, 3, d4, 3, c4, 3, g4, 3, f4,
		6, c4, 6, c4, 3, c5, 3, a4, 3, f4, 3, e4, 3, d4,
		6, b4, 6, b4, 3, a4, 3, f4, 3, g4, 3, f4, 
		0, 0
	};

	
	
	/*unsigned int sorcerer[]= //Sorcerer's Apprentice by Paul Dukas. 3/8 time, stored by bars #1-28
	{
	(210>>3),
	4,d5,4,p,4,p,
	4,a5,4,p,4,p,
	4,a4,4,b4,4,cx5,
	4,d5,4,p,4,f5,
	4,d5,4,p,4,f5,
	4,e5,4,d5,4,cx5,
	4,d5,4,p,4,f5,
	4,d5,4,p,4,f5,
	4,e5,4,d5,4,cx5,
	4,d5,4,p,4,f5,
	4,d5,4,f5,4,e5,
	4,d5,4,e5,4,f5,
	4,e5,4,g5,4,f5,
	4,e5,4,p,4,gx5,
	4,d5,4,p,4,f5,
	4,e5,4,g5,4,f5,
	4,e5,4,p,4,gx5,
	4,d5,4,p,4,f5,
	4,e5,4,f5,4,g5,
	3,a5,1,p,3,a5,1,p,3,a5,1,p,
	3,a5,1,p,3,a5,1,p,3,a5,1,p,
	4,a5,4,g5,4,f5,
	4,e5,4,g5,4,f5,
	4,e5,4,d5,4,c5,
	1,d5,3,c5,4,b4,4,a4,
	4,g5,4,p,4,p,
	10,g5,2,p,
	2,a5,0,0
	};*/

unsigned int play_tune,*tune_array;
unsigned int tempo;
unsigned int ms_tick=0;  //general purpose millisecond timer tick, incremented by TIMER2

void playMusic()
{
	tempo=sorcerer[0];			//set tempo = ms per whole note, divided by 8 for note duration scaling
	tune_array=&sorcerer[1];	//point to current tune
	play_tune=1;			//tell timer interrupt routine to play tune
	unsigned int duration=0;	//internal timer for current note
	volatile unsigned int dur,note;	
		
	while(play_tune)
	{	
		ms_tick++;								//increment timer tick at 1 ms intervals
						//are we playing a tune?
		if(duration>0)	
			duration--;				//yes, count down this note's timer
		else 
		{								//done with this note, get next [duration,note] pair
			dur=*tune_array++;
			//duration = tempo*dur; 			//calculate time in ms to play this note
			duration = 1000 / dur;
			if(dur==0) 
				play_tune=0;		//duration entry=0, so signal done playing tune
			note=*tune_array++;
			if (note>0) 
			{ 				//check for note or silence to play
				OCR3A = note;			//set timer interrupt period and point to next entry in tune table
				
			}
			else 
			{
				OCR3A = 0;				//silence, reduce interrupt load
			}
		}
		_delay_ms(2);
	}
}

#endif /* BUZZER_H_ */