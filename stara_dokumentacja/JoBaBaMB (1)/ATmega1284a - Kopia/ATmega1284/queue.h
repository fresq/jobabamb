/*
 * queue.h
 *
 * Created: 2013-11-22 21:47:17
 *  Author: Admin
 */ 


#ifndef QUEUE_H_
#define QUEUE_H_

#define QSIZE 32

typedef struct Queue
{
	int8_t tab[QSIZE];
	int head;
	int tail;
} Queue;


Queue droidQ;

void qAdd(Queue* q, int8_t data)
{
	if((q->tail + 1) % 8 != q->head)
	{
		q->tab[q->tail] = data;
		q->tail++;
		q->tail = q->tail % QSIZE;
	}
}

int8_t qPeek(Queue* q)
{
	if(q->tail != q->head)
		return q->tab[q->head];
}


int8_t qGet(Queue* q)
{
	if(q->tail != q->head)
	{
		int8_t a = q->tab[q->head];
		q->head++;
		q->head = q->head % QSIZE;
		return a;
	}
}

int qEmpty(Queue* q)
{
	return q->tail == q->head;
}


#endif /* QUEUE_H_ */