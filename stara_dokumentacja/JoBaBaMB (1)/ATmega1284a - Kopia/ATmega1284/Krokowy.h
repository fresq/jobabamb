/*
 * Krokowy.h
 *
 * Created: 2013-01-02 20:02:11
 *  Author: Admin
 */ 


#ifndef KROKOWY_H_
#define KROKOWY_H_

#define KROK_DIR_1 DDRB
#define KROK_PORT_1 PORTB
#define KROK_DIR_2 DDRE
#define KROK_PORT_2 PORTE
#define KROK_DIR_3 DDRB
#define KROK_PORT_3 PORTB
#define KROK_DIR_4 DDRB
#define KROK_PORT_4 PORTB

#define ZERO_PORT PORTD
#define ZERO_DIR DDRD

#define PWM_DIR_1 DDRE
#define PWM_PORT_1 PORTE
#define PWM_1 _BV(5)
#define PWM_DIR_2 DDRB
#define PWM_PORT_2 PORTB
#define PWM_2 _BV(5)

#define KROK_1 _BV(0)
#define KROK_2 _BV(7)
#define KROK_3 _BV(2)
#define KROK_4 _BV(3)

#define ZERO_PIN _BV(0)
#define  ZEROWA bit_is_set(PIND,0)

#define MICRO_COUNT 20
#define MAX_VALUE 0x5FFE


volatile int krok = 0;
volatile int krok_poz = 180;

volatile int mKrok = 0;
volatile int mKrokPoz = 180;
volatile int mMicroKrok = 0;

//int16_t values[MICRO_COUNT] = {0,3844,7594,11156,14444,17377,19881,21896,23372,24272};
int16_t values[MICRO_COUNT] = {0,2730,5461,8191,10922,13652,16383,19113,21844,24574};
//int16_t values[MICRO_COUNT] = {0,1293,2586,3880,5173,6467,7760,9053,10347,11640,12934,14227,15521,16814,18107,19401,20694,21988,23281,24575};
//int16_t values[MICRO_COUNT] = {0,68,272,612,1089,1701,2450,3335,4356,5514,6807,8237,9802,11504,13342,15316,17427,19673,22056,24575};
//int16_t values[MICRO_COUNT] = {0,501,1003,1504,2006,2507,3009,3510,4012,4513,5015,5516,6018,6519,7021,7522,8024,8526,9027,9529,10030,10532,11033,11535,12036,12538,13039,13541,14042,14544,15045,15547,16048,16550,17052,17553,18055,18556,19058,19559,20061,20562,21064,21565,22067,22568,23070,23571,24073,24575};

void InitKrokowy()
{
	ZERO_DIR &= ~ZERO_PIN;
	ZERO_PORT &= ~ZERO_PIN;
	
	KROK_DIR_1 |= KROK_1;
	KROK_DIR_2 |= KROK_2;
	KROK_DIR_3 |= KROK_3;
	KROK_DIR_4 |= KROK_4;
	
	KROK_PORT_1 &= ~KROK_1;
	KROK_PORT_2 &= ~KROK_2;
	KROK_PORT_3 &= ~KROK_3;
	KROK_PORT_4 &= ~KROK_4;
	
	PWM_DIR_1 |= PWM_1;
	PWM_DIR_2 |= PWM_2;
	
	DDRE |= _BV(3) | _BV(4) | _BV(5);
	DDRB |= _BV(5) | _BV(6) | _BV(7);	
	
	
	OCR3C = 0;
	OCR3B = 0;
	OCR3A = 0;
	OCR1A = 0;
	OCR1B = 0;
	OCR1C = 0;
		
	//intit PWM-s
	ICR1 = 0x5FFF;
	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<COM1C1);
	TCCR1B = (1<<WGM13)|(1<<CS10);
	ICR3 = 0x5FFF;
	TCCR3A = (1<<COM3A1)|(1<<COM3B1)|(1<<COM3C1);;
	TCCR3B = (1<<WGM33)|(1<<CS30);
	
	OCR3C = MAX_VALUE; //krokowy pwm1
	OCR3B = MAX_VALUE; //Motor pwm2
	//OCR3A = MAX_VALUE / 3; //buzerek
	OCR1A = MAX_VALUE; //krokowy pwm2
	OCR1B = MAX_VALUE; //motor pwm1
	OCR1C = MAX_VALUE; //pompka
	

}

void Krok(int lewo);

volatile int zerowa;

void ZerujKrokowy()
{
	zerowa = 0;
	while(!zerowa)
	{
		Krok(1);
	}
	krok_poz = 180;
	//while(1)
	//{
		//if(ZEROWA) {LCD_GoTo(0,0); LCD_WriteText("Zerowa");}
		//else {LCD_GoTo(0,0); LCD_WriteText("Nie");} 
	//}
}

ISR(PCINT1_vect)
{
	zerowa = 1;
	krok_poz = 180;
	//LCD_WriteText("przerwanie");
}

void KroczDo(int cel, int lewo)
{
	while(krok_poz != cel)
		Krok(lewo);
}


void Krocz(int ile, int lewo)
{
	for (int i = 0; i < ile; i++)
	{
		Krok(lewo);
	}
}

void KrokowyStop()
{
	KROK_PORT_1 &= ~KROK_1;
	KROK_PORT_2 &= ~KROK_2;
	KROK_PORT_3 &= ~KROK_3;
	KROK_PORT_4 &= ~KROK_4;
}

void KrokowySetCoils(int c1, int c2, int c3, int c4)
{
	if(c1) 	KROK_PORT_1 |= KROK_1; else KROK_PORT_1 &= ~KROK_1;
	if(c2) 	KROK_PORT_2 |= KROK_2; else KROK_PORT_2 &= ~KROK_2;
	if(c3) 	KROK_PORT_3 |= KROK_3; else KROK_PORT_3 &= ~KROK_3;
	if(c4) 	KROK_PORT_4 |= KROK_4; else KROK_PORT_4 &= ~KROK_4;
}

void PolKrok(int lewo)
{
	if(lewo)
	{
		krok++;
		krok_poz++;
	}		
	else
	{
		krok--;
		krok_poz--;
	}		
		
	if(krok == -1) krok = 7;
	if(krok == 8) krok = 0;
						
	if(krok == 0)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 &= ~KROK_4;
	}
	else if(krok == 1)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
	}
	else if(krok == 2)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
	}
	else if(krok == 3)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
	}
	else if(krok == 4)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 &= ~KROK_4;
	}
	else if(krok == 5)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
	}	
	else if(krok == 6)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
	}	
	else if(krok == 7)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
	}	
}

void Krok(int lewo)
{
	//MikroKrok(lewo);
	PolKrok(lewo);
	//MikroKrok2(lewo);
}

void MikroKrok(int lewo)
{
	if(lewo)
	{
		mMicroKrok++;
		mKrokPoz++;
		if(mMicroKrok == MICRO_COUNT)
		{
			mKrok++;
			mMicroKrok = 0;
			if(mKrok == 8)
			{
				mKrok = 0;
			}
		}
	}		
	else
	{
		mMicroKrok--;
		mKrokPoz--;
		if(mMicroKrok == -1)
		{
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1;
			if(mKrok == -1)
			{
				mKrok = 7;
			}
		}
	}
						
	if(mKrok == 0)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = MAX_VALUE;
		OCR1A = values[mMicroKrok];
	}
	else if(mKrok == 1)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = values[MICRO_COUNT - mMicroKrok - 1];
		OCR1A = MAX_VALUE;
	}
	else if(mKrok == 2)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = values[mMicroKrok];
		OCR1A = MAX_VALUE;
	}
	else if(mKrok == 3)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = MAX_VALUE;
		OCR1A = values[MICRO_COUNT - mMicroKrok - 1];
	}
	else if(mKrok == 4)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = MAX_VALUE;
		OCR1A = values[mMicroKrok];
	}
	else if(mKrok == 5)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = values[MICRO_COUNT - mMicroKrok - 1];
		OCR1A = MAX_VALUE;
	}	
	else if(mKrok == 6)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = values[mMicroKrok];
		OCR1A = MAX_VALUE;
	}	
	else if(mKrok == 7)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = MAX_VALUE;
		OCR1A = values[MICRO_COUNT - mMicroKrok - 1];
	}	
	//_delay_ms(10);
}


void MikroKrok2(int lewo)
{
	if(lewo)
	{
		mMicroKrok++;
		mKrokPoz++;
		if(mMicroKrok == MICRO_COUNT - 1)
		{
			mKrok++;
			mMicroKrok = 0;
			if(mKrok == 4)
			{
				mKrok = 0;
			}
		}
	}
	else
	{
		mMicroKrok--;
		mKrokPoz--;
		if(mMicroKrok == -1)
		{
			mKrok--;
			mMicroKrok = MICRO_COUNT - 1 - 1;
			if(mKrok == -1)
			{
				mKrok = 3;
			}
		}
	}
	
	if(mKrok == 0)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = values[MICRO_COUNT - mMicroKrok - 1];
		OCR1A = values[mMicroKrok];
	}
	else if(mKrok == 1)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 |= KROK_3;
		KROK_PORT_4 &= ~KROK_4;
		OCR3C = values[mMicroKrok];
		OCR1A = values[MICRO_COUNT - mMicroKrok - 1];
	}
	else if(mKrok == 2)
	{
		KROK_PORT_1 &= ~KROK_1;
		KROK_PORT_2 |= KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = values[MICRO_COUNT - mMicroKrok - 1];
		OCR1A = values[mMicroKrok];
	}
	else if(mKrok == 3)
	{
		KROK_PORT_1 |= KROK_1;
		KROK_PORT_2 &= ~KROK_2;
		KROK_PORT_3 &= ~KROK_3;
		KROK_PORT_4 |= KROK_4;
		OCR3C = values[mMicroKrok];
		OCR1A = values[MICRO_COUNT - mMicroKrok - 1];
	}
	//_delay_ms(10);
}
#endif /* KROKOWY_H_ */