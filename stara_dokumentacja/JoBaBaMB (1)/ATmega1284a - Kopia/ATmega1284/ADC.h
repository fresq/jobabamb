/*
 * ADC.h
 *
 * Created: 2013-08-31 13:25:37
 *  Author: Bartek
 */ 


#ifndef ADC_H_
#define ADC_H_

#define V_Ref 2.56f

volatile float V = 6.6f;

ISR(ADC_vect)
{
	V = ((ADC / 1023.0f) * V_Ref) * 4.71361f;
	ADCSRA|=_BV(ADSC);
}

void ADCInit()
{
	ADCSRA |= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0) | _BV(ADEN) | _BV(ADIE);
	ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX0);
	ADCSRA|=_BV(ADSC);
}

#endif /* ADC_H_ */