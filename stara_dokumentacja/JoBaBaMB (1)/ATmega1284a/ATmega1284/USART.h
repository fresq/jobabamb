﻿/*
 * USART.h
 *
 * Created: 2012-05-04 18:44:10
 *  Author: Paweł Joniak
 */ 


#ifndef USART_H0_
#define USART_H0_
#include "queue.h"


#define      PREDKOSC_USART0     9600                      //Prędkość transmisji i wyłączona 

#define      BAUD0   ((F_CPU / (PREDKOSC_USART0 * 16UL)) - 1)      //Wzór na wyliczenie "BAUD" 
//#endif                                                //Koniec warunku 

#define      W_UBRRL0            (unsigned char)(BAUD0)                  //Wyłuskaj niższa część "BAUD" 
#define      W_UBRRH0           (unsigned char)(BAUD0 >> 8)               //Wyłuskaj wyższa część "BAUD" 

//Declare the location of the stream for the output
void USART0_Transmite(char c){
	// Wait until UDR ready
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = c;    // send character
}

unsigned char USART0_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer */
	return UDR0;
}
void USART0_SendText(char* s)
{
	do
	{
		while ( !( UCSR0A & (1<<UDRE0)) );
		UDR0 = *s;
	}while(*s++!='\0');
}


//USART RX interrupt this code is executed when we recieve a character
ISR(USART0_RX_vect)
{
	qAdd(&droidQ, UDR0);
}
 //
void USART0Init()
{
   UBRR0H = W_UBRRH0;                                    //Ustaw rejestr "UBRR0H" 
   UBRR0L = W_UBRRL0;                                    //Ustaw rejestr "UBRR0L" 

   //UCSR1A |= (W_U2X0 << U2X0);                           //W zależności od wybranej opcji 
   //UCSR1B = 0b10010000; 
   //UCSR1C = 0b00100110; 
  
   UCSR0B = (1 << TXEN0) | (1 << RXEN0);
   //// Enable receive complete interrupt
   UCSR0B |= (1 << RXCIE0);
   //// Asynch, 8 data, no parity, 2 stop
   //UCSR1C = (1 << USBS1)  UCSR1C |= (1 << UCSZ11);
  // UCSR0C = (1<<USBS0)|(3<<UCSZ00);
  UCSR0C = (3<<UCSZ00);
}




#endif /* USART_H_ */