/*
 * ADC.h
 *
 * Created: 2013-08-31 13:25:37
 *  Author: Bartek
 */ 


#ifndef ADC_H_
#define ADC_H_

#define V_Ref 2.56f

volatile float V = 6.6f;
volatile float TEMP = 1.1f;
volatile int adc_port = 0;

ISR(ADC_vect)
{
	if(adc_port ==  1) V = ((ADC / 1023.0f) * V_Ref) * 4.71361f;
	else if(adc_port == 0) TEMP = ADC;//((ADC / 1023.0f) * V_Ref) * 4.71361f;
}

void ADCInit()
{
	ADCSRA |= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0) | _BV(ADEN) | _BV(ADIE);
	ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX0);
	ADCSRA|=_BV(ADSC);
}

#endif /* ADC_H_ */