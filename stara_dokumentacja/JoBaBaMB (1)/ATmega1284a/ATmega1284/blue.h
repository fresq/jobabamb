/*
 * IncFile1.h
 *
 * Created: 2013-11-22 22:02:35
 *  Author: Admin
 */ 


#ifndef INCFILE1_H_
#define INCFILE1_H_

#include "queue.h"
#include "buzzer.h"

#define RIGHT_PAD 0x7e //126
#define GYRO 0x7d //125
#define POMPKA 0x7c
#define PODSTAWA 0x7b
#define S1 0x7a
#define S2 0x79
#define S3 0x78
#define S4 0x77
#define CHW_OBR 0x76
#define CHW 0x75
#define LEFT_PAD 0x74
#define VBAR 0x73
#define DEFAULT 0x72

int8_t GetNextData()
{
	while(qEmpty(&droidQ)) {_delay_ms(10);}
	return qGet(&droidQ);
}


void VBar(int v)
{
	
}

void RightPad(int x, int y)
{
	float l = 0;
	float r = 0;
	l += (-y / 100.0f) * 0.8f;
	r += (-y / 100.0f) * 0.8f;
	int znak = (y < 0) ? -1 : 1;

	if(y < 38)
	{
		l += (x / 100.0f) * 0.5f;
		r -= (x / 100.0f) * 0.5f;
	}
	else
	{
		l -= (x / 100.0f) * 0.5f;
		r += (x / 100.0f) * 0.5f;
	}
	if(l <= -1) l = -0.99;
	if(l >= 1) l = 0.99;
	if(r <= -1) r = -0.99;
	if(r >= 1) r = 0.99;
	LCD_Clear();
	LCD_WriteText("RP: ");
	LCD_WriteInt(l*100);
	LCD_WriteText(", ");
	LCD_WriteInt(r*100);
	JazdaP(l, r);
}

void LeftPad(int x, int y)
{
	//float xx = x / 33.333f;
	//float yy = y / 3.333f;
	//LCD_Clear();
	//LCD_WriteText("LP: ");
	//LCD_WriteInt(xx*100);
	//LCD_WriteText(", ");
	//LCD_WriteInt(yy*100);
	//rkMoveArm(a, xx, yy);
}

void Gyro(int x, int y, int z)
{
	LCD_Clear();
	LCD_WriteText("Zyro: ");
	LCD_WriteInt(x);
	LCD_WriteText(", ");
	LCD_WriteInt(y);
	LCD_WriteText(", ");
	LCD_WriteInt(z);
}

void DelayMs(int ms)
{
	for(int i = 0; i < ms; i++)
		_delay_ms(1);
}

void Delay100Us(int us)
{
	for(int i = 0; i < us; i++)
		_delay_us(100);
}

void BluetoothComunication(arm* a)
{
	droidQ.head = 0;
	droidQ.tail = 0;
	int baseCount = 0;
	int baseDir = 0;
	int baseMax = 50;
	
	//for(int i = 0; i < 24000; i++)
	//{
		//OCR3A = i;
		//_delay_ms(5);
	//}
	
	//playMusic();
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_ms(3);	}
	//for(int i = 0; i < 40; i++){Krocz(1,1); _delay_ms(2);	}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1800);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1700);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1600);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1500);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1400);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1300);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1200);}
	//for(int i = 0; i < 30; i++){Krocz(1,1); _delay_us(1100);}
	//for(int i = 0; i < 10000; i++){Krocz(1,1); _delay_us(1000);}
		
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(900);}
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(850);}
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(800);}
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(750);}
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(700);}
	
	//for(int i = 0; i < 3; i++){Krocz(1,1); _delay_ms(30);	}
	//for(int i = 0; i < 4; i++){Krocz(1,1); _delay_ms(20);	}
	//for(int i = 0; i < 3; i++){Krocz(1,1); _delay_ms(15);}
	//for(int i = 0; i < 5; i++){Krocz(1,1); _delay_ms(10);}
	//for(int i = 0; i < 8; i++){Krocz(1,1); _delay_ms(7);}
	//for(int i = 0; i < 10; i++){Krocz(1,1); _delay_us(6000);}
	//for(int i = 0; i < 15; i++){Krocz(1,1); _delay_us(5000);}
	//for(int i = 0; i < 25; i++){Krocz(1,1); _delay_us(4000);}
	//for(int i = 0; i < 1000; i++){Krocz(1,1); _delay_us(3000);}
		
		
	//for(int i = 0; i < 1000; i++)
	//{
		//Krocz(1,1); 
		//_delay_ms(1000);	
		//LCD_Clear();
		//LCD_WriteInt(mKrok);
		//LCD_WriteText(", ");
		//LCD_WriteInt(mMicroKrok);
	//}
	
	
	LCD_Clear();
	LCD_WriteText("Blue1!");
	KrokowyStop();
	
	while(1)
	{
		if(!qEmpty(&droidQ))
		{
			int8_t data = qGet(&droidQ);
			if(data == RIGHT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				RightPad(x, y);
			}
			else if(data == DEFAULT)
			{
				SetSlowAngle(a, 0, 30, 30, 30, 0, 500);
			}
			else if(data == LEFT_PAD)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				float yy = y / 33.333f;
				LCD_Clear();
				
				if(abs(yy)>0.5)
					rkMoveArm(a, yy, 0);
				
				baseDir = (x < 0) ? 0 : 1;
				baseMax = 50 - abs(x / 2);
				
				LCD_WriteText("LP: ");
				LCD_WriteInt(x);
				LCD_WriteText(", ");
				LCD_WriteInt(yy*100);
			}
			else if(data == VBAR)
			{
				int8_t h = GetNextData();
				float step_down = h / 16.666f;
				rkMoveArm(a, 0.0f, step_down);
				
				LCD_Clear();
				LCD_WriteText("VBar: ");
				LCD_WriteInt(step_down*100);				
			}
			else if(data == GYRO)
			{
				int8_t x = GetNextData();
				int8_t y = GetNextData();
				int8_t z = GetNextData();
				Gyro(x, y, z);
			}
			else if(data == S1)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 0, angle,300);
			}
			else if(data == S2)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 1, angle,300);
			}
			else if(data == S3)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 2, angle,300);
			}
			else if(data == S4)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 3, angle,300);
			}
			else if(data == CHW)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 5, angle,300);
			}
			else if(data == CHW_OBR)
			{
				int8_t angle = GetNextData();
				SetSlowOneAngle(a, 4, angle,300);
			}
			else if(data == POMPKA)
			{
				int8_t v = GetNextData();
				PompkaWypelnienieP( v / 100.0f);		
				LCD_Clear();
				LCD_WriteText("Pompka: ");
				LCD_WriteInt(v);
				LCD_WriteText("%");
			}			
			else if(data == PODSTAWA)
			{
				int8_t v = GetNextData();
				baseDir = (v < 0) ? 0 : 1;
				baseMax = 50 - abs(v);
				//LCD_Clear();
				//LCD_WriteText("ob: ");
				//LCD_WriteInt(baseSpeed);
				//LCD_WriteText(", ");
				//LCD_WriteInt(baseDir);
			}
		}
		if(baseMax != 50) baseCount++;
		if(baseCount >= baseMax)
		{
			Krocz(1, baseDir);
			baseCount = 0;
		}
		_delay_ms(10);
	}
}


#endif /* INCFILE1_H_ */