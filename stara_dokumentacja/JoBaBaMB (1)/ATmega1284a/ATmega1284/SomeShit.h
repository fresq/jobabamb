/*
 * SomeShit.h
 *
 * Created: 2013-12-10 23:26:42
 *  Author: Admin
 */ 


#ifndef SOMESHIT_H_
#define SOMESHIT_H_

#define PS2_DATA_PORT PORTE
#define PS2_DATA_DIR DDRE
#define PS2_DATA_PIN _BV(2)

#define PS2_CLOCK_PORT PORTE
#define PS2_CLOCK_DIR DDRE
#define PS2_CLOCK_PIN _BV(6)

void NalejWodki(Memory* m, arm* a)
{
	PS2_DATA_DIR &= ~PS2_DATA_PIN;
	PS2_DATA_PORT |= ~PS2_DATA_PIN;
	PS2_CLOCK_DIR &= ~PS2_CLOCK_PIN;
	PS2_CLOCK_PORT |= ~PS2_CLOCK_PIN;
	uint8_t data = 0;
	int start = 0;
	int count = 0;
	LCD_Clear();
	while(1)
	{
		if(!(PINE & PS2_CLOCK_PIN))
		{
			_delay_us(20);
			if(!start)
			{
				if(!(PINE & PS2_DATA_PIN))
				{
					start = 1;
					count = 0;
				}
			}
			else
			{
				count ++;
				// odczytaj bit danych
				if(count <= 8)
				{
					data = data << 1;
					if(PINE & PS2_DATA_PIN)
						data |= 1;
					else
						data &= ~1;
				}
				else
				{
					count = 0;
					LCD_WriteInt(data);
					LCD_WriteText("|");
					if(data == 110)
						LCD_Clear();
					//ShowByte(data);
					start = 0;
				}
			}
			_delay_us(40);
		}
	}
	
	
	
	LoadFromMemory(a, m->tab[2]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[3]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[5]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[5]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[6]);
	_delay_ms(1000);
	LoadFromMemory(a, m->tab[4]);
	_delay_ms(1000);
	
	/*SetAllAngle(a,0,0,0,0,5);
	ZerujKrokowy();
	//Do kieliszka wyprostowany
	KroczDo(164,0);
	//Ugjecie nad kieliszkiem
	SetSlowAngle(a,48,-86,-73,34,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,150,150,150,75, -80, 5);
	Set(a);
	Move(*a);	
	//Pierwsza pozycja
	KroczDo(173,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a); 
	Move(*a);
	_delay_ms(200);
	//dostawienie
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,35,150,150,150,-40,5);
	Set(a);
	Move(*a);
	KroczDo(164,0);
	_delay_ms(200);
	//ugiecie sie do 2 kieliszka
	SetSlowAngle(a,30,-85,-81,58,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,150,150,150,93, -80, 5);
	Set(a);
	Move(*a);
	//druga pozycja
	KroczDo(180,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a);
	Move(*a);
	_delay_ms(200);
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);	
	//podniesienie
	SetSlowAngle(a,35,150,150,150,-40,5);
	Set(a);
	Move(*a);
	KroczDo(164,0);
	_delay_ms(200);
	//ugiecie sie do 3 kieliszka
	SetSlowAngle(a,15,-85,-81,68,-18,10);
	Set(a);
	Move(*a);
	_delay_ms(500);
	//Zlapanie
	SetSlowAngle(a,150,150,150,150, -80,5);
	Set(a);
	Move(*a);
	//podniesienie
	SetSlowAngle(a,50,-85,-81,70, -80, 5);
	Set(a);
	Move(*a);  
	//druga pozycja
	KroczDo(184,1);
	_delay_ms(1000);
	//opuszczenie
	SetSlowAngle(a,5,-86,-76, 90, 150,5);
	Set(a);
	Move(*a);
	_delay_ms(200);
	//otwarcie
	SetSlowAngle(a,150,150,150,150,-40,5);
	Set(a);
	Move(*a);
	SetSlowAngle(a,0,0,0,0,-55,5);
	Set(a);
	Move(*a);*/
}


int PetMenu()
{
	char* tab[] = {"Niuchacz", "Waz", "Panda", "Inne", "Koniec"};
	int max = 5;
	int aktualny = 0;
	int zMin = 0, zMax = 3;
	int cmd;
	int toogle;

	LCD_Clear();
	for(int i = 0; i < 4; i++)
	{
		LCD_GoTo(0,i);
		if(i+zMin == aktualny) LCD_WriteText("->");
		LCD_WriteText(tab[zMin+i]);
	}

	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return -1;
		if(cmd == KEY_OK) return aktualny;
		if(cmd == KEY_DOWN)
		{
			aktualny++; if(aktualny >= max) {aktualny = 0; zMin = 0; zMax = 3;}
			if(aktualny > zMax) {zMax++; zMin++;}
		}
		else if(cmd == KEY_UP)
		{
			aktualny--; if(aktualny < 0) {aktualny = max - 1; zMin = max - 4; zMax = max - 1;}
		}
		
		LCD_Clear();
		for(int i = 0; i < 4; i++)
		{
			LCD_GoTo(0, i);
			if(i+zMin == aktualny) LCD_WriteText("->");
			LCD_WriteText(tab[zMin+i]);
		}
		
	}
}

void Zwierzeta(arm* a)
{
	int cmd;
	while(1)
	{
		_delay_ms(100);
		int pet = PetMenu();
		_delay_ms(100);
		if(pet == -1 || pet == 4) return;
		if(pet == 0)//Niuchacz
		{
			SetSlowAngle(a,-22,-39,-43,29,-55,5);
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[3].ang = (rand() % 30) - 15 + 29;
				a->tab[4].ang = (rand() % 30) - 15 - 55;
				Move(*a);
				_delay_ms(50);
			}
		}
		else if(pet == 1) //Waz
		{
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[0].ang = 40;
				a->tab[1].ang = -40;
				a->tab[2].ang = 40;
				a->tab[3].ang = 40;
				Move(*a);
				_delay_ms(200);
				a->tab[0].ang = -40;
				a->tab[1].ang = 40;
				a->tab[2].ang = -40;
				a->tab[3].ang = -40;
				Move(*a);
				_delay_ms(200);
			}
		}
		else if(pet == 2)//panda
		{
			LCD_Clear();
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,1);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,2);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,3);
			LCD_WriteText("Pan da 5.5!!");
			LCD_GoTo(0,4);
			while(1)
			{
				cmd = detect();
				cmd &= ~2048;
				if(cmd == KEY_ESC) break;
				a->tab[0].ang = 30;
				a->tab[1].ang = 80;
				a->tab[2].ang = -80;
				a->tab[3].ang = -45 ;
				Move(*a);
				_delay_ms(200);
				a->tab[0].ang = 35;
				a->tab[1].ang = 75;
				a->tab[2].ang = -70;
				a->tab[3].ang = -40;
				_delay_ms(200);
				Move(*a);
			}
		}
	}
}


#endif /* SOMESHIT_H_ */