#ifndef __RC5
#define __RC5

//---------------------------------------------------------------
//   Plik "main.c"
//
//   KURS AVR-GCC (abxyz.bplaced.net)
// 
//   Dekoder  RC5
// 
//   (schemat i opis dzia�ania w artykule)
//   testowanie na atmega8 (8MHz)
//---------------------------------------------------------------

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


#define KEY_ESC 12
#define KEY_LEFT 17
#define KEY_RIGHT 16
#define KEY_DOWN 33
#define KEY_UP 32
#define KEY_OK 59
#define KEY_1 1
#define KEY_2 2
#define KEY_3 3
#define KEY_4 4
#define KEY_5 5
#define KEY_6 6
#define KEY_7 7
#define KEY_8 8
#define KEY_9 9

// Odbiornik podczerwieni SFH5110  przy��czona do portu  PD6 
#define RC5_IN   (PIND & _BV(1))

//
typedef unsigned char u8;
typedef unsigned int  uint;

// Zmienne globalne pe�ni� rol�  programowych zegar�w
// nap�dzanych przerwaniem TIMER0_OVF
volatile u8 timerL; 
volatile u8 timerH; 

//---------------------------------------------------------------
// Funkcja konfiguruje i uruchamia Timer0 
// oraz w��cza przerwanie od przepe�nienia timera,
// przerwanie powinno wyst�powa� co 32us.  
//---------------------------------------------------------------
void init_rc5()
{
  ////atmega8
  OCR0 = 80;
  DDRD &= ~_BV(1);
  PORTD |= _BV(1);
  TCCR0 = (1<<CS01);
  TIMSK = (1<<OCIE0);

  //TCNT3 = 0;
  //TCCR0 = (1<<CS00);  // w��cza Timer0  
 // TIMSK = (1<<TOIE0); // w��cza przerwanie "Timer0 Overflow"

/*
  //atmega88
  TCCR0B = (1<<CS00);
  TIMSK0 = (1<<TOIE0);
  */
}



//---------------------------------------------------------------
// Funkcja wykrywa i dekoduje  komend� pilota RC5                                             
//---------------------------------------------------------------
 int detect()
 {
    u8 temp;
    u8 ref1;
    u8 ref2;
    u8 bitcnt;
    int command;

    timerH  = 0;
    timerL  = 0;

    // Czeka na okres ciszy na linii wej�cia uC trwaj�cy  3.5ms
    // Je�li nie wykryje takiego okresu ciszy w ci�gu 131ms,
    // to ko�czy dzia�anie funkcji z b��dem
    while( timerL<110)
    {
       if(timerH>=16)  return  command = -2;

       if(!RC5_IN) timerL = 0;
    }

    // Czeka na  pierwszy bit startowy. 
    // Je�li nie wykryje bitu startowego w ci�gu 131ms,
    // to ko�czy dzia�anie funkcji z b��dem
    while(RC5_IN)  
         if(timerH>=16)  return command = -3 ;


    // Pomiar czasu trwani niskiego poziom sygan�u 
    // w pierwszym bicie startowym.
    // Je�li nie wykryje rosn�cego zbocza sygna�u w ci�gu  
    // 1ms, to ko�czy dzia�anie funkcji z b��dem 
    timerL = 0;
    while(!RC5_IN)
         if(timerL>34) return command = -4;

    //
    temp = timerL;
    timerL = 0;

    // ref1 - oblicza  3/4 czasu trwania bitu
    ref1 =temp+(temp>>1);

    // ref2 - oblicza 5/4 czasu trwania bitu
    ref2 =(temp<<1)+(temp>>1);

 
    // Oczekuje na zbocze opadaj�ce sygna�u w �rodku drugiego
    // bitu startowego.
    // Je�li nie wykryje zbocza w ci�gu 3/4 czasu trwania 
    // bitu, to ko�czy dzia�anie funkcji z b��dem 
    while(RC5_IN)
         if(timerL > ref1) return command = -5;

    // W momencie wykrycia zbocza sygna�u, synchronizuje
    // zmien� timerL dla pr�bkowania  bitu toggle
    timerL = 0;

    // Odczytuje dekoduje pozosta�e 12 bit�w polecenia rc5
    for(bitcnt=0, command = 0; bitcnt <12; bitcnt++)
    {
       // Czeka 3/4 czasu trwania bitu od momentu wykrycia
       // zbocza sygna�u w po�owie poprzedniego bitu 
       while(timerL < ref1) {};
 
       // Pr�bkuje - odczytuje port we  uC
       if(!RC5_IN)
       {
          // Je�li odczytano 0, zapami�tuje w zmiennej 
          // "command" bit o warto�ci 0         
          command <<= 1 ;

          // Oczekuje na zbocze rosn�ce sygna�u w �rodku bitu.
          // Je�li nie wykryje zbocza w ci�gu 5/4 czasu trwania 
          // bitu, to ko�czy dzia�anie funkcji z b��dem    
          while(!RC5_IN)
             if(timerL > ref2) return command = -6;
       }
       else
       {
          // Je�li odczytano 1, zapami�tuje w zmiennej 
          // "command" bit o warto�ci 1  
          command = (command <<1 ) | 0x01;

          // Oczekuje na zbocze opadaj�ce sygna�u w �rodku bitu.
          // Je�li nie wykryje zbocza w ci�gu 5/4 czasu trwania 
          // bitu, to ko�czy dzia�anie funkcji z b��dem 
          while(RC5_IN)
             if(timerL > ref2) return command = -7;
       }

       // W momencie wykrycia zbocza sygna�u, synchronizuje
       // zmien� timerL dla pr�bkowania kolejnego bitu
       timerL = 0;
   }

   // Zwraca kod polecenia rc5
   // bity 0..5 numer przycisku
   // bity 6..10  kod systemu(urz�dzenia)
   // bit 11 toggle bit
   return command;
 }

//---------------------------------------------------------------
// GL�WNA FUNKCJA PROGRAMU                                                   
//---------------------------------------------------------------
//int main(void)
//{
  ////
  //uint cmd;
  //u8 out;
//
  //// Porty PD0..PD6  wyj�ciami - diody LED
  //DDRD  = 0x7f;
  //PORTD = 0x00;
//
  //// uruchamia Timer0 i przerwanie
  //init_rc5();
//
  //while (1)
  //{
     //// Wykrywa i dekoduje polecenie pilota RC5 
     //cmd = detect();
//
     //// Je�li odebrano komend� 
     //if(cmd != -1)
     //{  
        //// Na sze�ciu diodach LED poka�e numer polecenia rc5,
        //// a na si�dmej LED - toggle bit
        //out = (cmd & (1<<11)) >> 5;
        //out |= cmd & 0x3f;
        //PORTD = (out);
     //}
  //}
//
  //return 0;
//}

#endif