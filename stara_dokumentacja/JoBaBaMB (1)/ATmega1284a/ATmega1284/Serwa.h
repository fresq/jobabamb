﻿
#define SERVO_PORT  PORTA 
#define SERVO_DDR   DDRA 

// Up to 8 servos (since pulses are generated in 
// sequence + only one port is used). 
#define N_SERVOS    6 

// Servo times (this is Futaba timing). 
#define SERVO0_MIN  1010// microseconds 
#define SERVO0_MAX   1710 // microseconds 
#define SERVO0_MID   1310
#define SERVO0_ANGLESPAN 90.0f

#define SERVO1_MIN    700 // microseconds 
#define SERVO1_MAX   2540 // microseconds 
#define SERVO1_MID   1610
#define SERVO1_ANGLESPAN 180.0f

#define SERVO2_MIN    590 // microseconds 
#define SERVO2_MAX   2540 // microseconds 
#define SERVO2_MID   1580
#define SERVO2_ANGLESPAN 180.0f

#define SERVO3_MIN    700 // microseconds
#define SERVO3_MAX   2710 // microseconds
#define SERVO3_MID   1670
#define SERVO3_ANGLESPAN 180.0f

#define SERVO4_MIN    660 // microseconds
#define SERVO4_MAX   2700 // microseconds
#define SERVO4_MID   1700
#define SERVO4_ANGLESPAN 180.0f


#define SERVO5_MIN    700 // microseconds
#define SERVO5_MAX   1840 // microseconds
#define SERVO5_MID   1140
#define SERVO5_ANGLESPAN 180.0f

// Time between servo pulses.  
#define SERVO_FRAME 20000 // microseconds (50Hz) 

// Time slot available for each servo. 
#define SERVO_TIME_DIV (SERVO_FRAME / N_SERVOS) 

#if (SERVO_TIME_DIV < SERVO0_MAX + 50) 
#warning "Output fewer servo signals or increase SERVO_FRAME" 
#endif 
#if ((SERVO_TIME_DIV * (F_CPU / 1000000UL))/256 >= 0xFF) 
#warning "Output more servo signals or decrease SERVO_FRAME (or use the prescaler)" 
#endif 

// Computing timer ticks given microseconds. 
// Note, this version works out ok with even MHz F_CPU (e.g., 1, 2, 4, 8, 16 MHz). 
// (Not a good idea to have this end up as a floating point operation) 
#define US2TIMER1(us) (((us) * (uint16_t)(F_CPU / 1E6))/256)

// Servo times - to be entered as timer1 ticks (using US2TIMER1). 
// This must be updated with interrupts disabled. 
volatile uint16_t servoTime[N_SERVOS]; 

// Servo output allocation (on a single port currently). 
const static uint8_t servoOutMask[N_SERVOS] = { 
    0b00000001, // PX0 
    0b00000010, // PX1 
    0b00000100, // PX2 
    0b00001000, // PX3 
    0b00010000, // PX4 
    0b00100000, // PX5 
    0b01000000, // PX6 
    0b10000000, // PX7 
}; 
// Servo mask is just the above masks ored. 
#define SERVO_MASK 0xff 

void servoStart(void) 
{ 
    // Outputs 
    SERVO_DDR |= SERVO_MASK;
    // Setupt a first compare match 
    OCR2 = TCNT2 + US2TIMER1(100);
    // start timer 1 with 1024 prescaler 
    TCCR2 = (1 << CS22);// | (1 << CS20);
    // Enable interrupt 
    TIMSK |= (1 << OCIE2);
} 

void servoSet(uint8_t servo, uint16_t time /* microseconds */)
{
    uint16_t ticks = US2TIMER1(time);
    cli();
    servoTime[servo] = ticks;
    sei();
}
void servoAngle0(float angle)
{
	float dz = (SERVO0_MAX - SERVO0_MIN) / SERVO0_ANGLESPAN;

	servoSet(0, SERVO0_MID + (uint16_t)(angle * dz));
}

void servoAngle1(float angle)
{
	float dz = (SERVO1_MAX - SERVO1_MIN) / SERVO1_ANGLESPAN;

	servoSet(1, SERVO1_MID + (uint16_t)(angle * dz));
}

void servoAngle2(float angle)
{
	float dz = (SERVO2_MAX - SERVO2_MIN) / SERVO2_ANGLESPAN;

	servoSet(2, SERVO2_MID + (uint16_t)(angle * dz));
}

void servoAngle3(float angle)
{
	float dz = (SERVO3_MAX - SERVO3_MIN) / SERVO3_ANGLESPAN;
	servoSet(3, SERVO3_MID + (uint16_t)(angle * dz));
}

void servoAngle4(float angle)
{
	float dz = (SERVO4_MAX - SERVO4_MIN) / SERVO4_ANGLESPAN;
	servoSet(4, SERVO4_MID + (uint16_t)(angle * dz));
}

void servoAngle5(float angle)
{
	float dz = (SERVO5_MAX - SERVO5_MIN) / SERVO5_ANGLESPAN;
	servoSet(5, SERVO5_MID + (uint16_t)(angle * dz));
}
