/*
 * Pompka.h
 *
 * Created: 2013-04-05 18:56:09
 *  Author: Admin
 */ 


#ifndef POMPKA_H_
#define POMPKA_H_

uint16_t wypelnienie;

#define POMPKA_DDR DDRB
#define POMPKA_NR 7

#ifndef MAX_VALUE
#define MAX_VALUE 0x5FFE
#endif
#define POMPKA_MAX_VALUE MAX_VALUE

void InitPompka()
{
	wypelnienie = 0;
	OCR1C = 0;
}

void PompkaWypelnienieP(float p)
{
	OCR1C = (int)(p * MAX_VALUE);
}

void PompkaWypelnienie(int w)
{
	OCR1C = w;
	wypelnienie = w;
}

void PompkaWypelnienieMod(uint8_t w)
{
	wypelnienie += w;
	OCR1C = wypelnienie;
}



#endif /* POMPKA_H_ */