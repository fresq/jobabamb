﻿/*
 * kinematyka->h
 *
 * Created: 2012-05-10 21:22:07
 *  Author: Paweł Joniak
 */ 

#include <math.h>
#include <stdlib.h>
#include "Praktyczne.h"

#ifndef KINEMATYKA_H_
#define KINEMATYKA_H_

#define RAD 3.141592f/180.0f

#define SECONDS(s) (long)((long)(s[0]*10+s[1])*3600 + ((long)s[3]*10+s[4])*60 + (long)(s[6]*10+s[7])*1 - (long)(11*3661*'0'))

#define COUNT 6
#define LIMIT 85


typedef struct
{
 float len;
 float ang,arg;
 float x,y;
} joint;


typedef struct
{
 joint tab[COUNT]; 
 float ex,ey;
 int tryb;
 
} arm;


void Set(arm* a)
{
	a->tab[0].arg = a->tab[0].ang;
	for(int i=0; i<COUNT-1; i++) //mogloby byc do COUNT a nie do COUNT-1
	{
		a->tab[i+1].arg = a->tab[i].arg + a->tab[i+1].ang;
		a->tab[i+1].x = a->tab[i].x + a->tab[i].len * cosf(a->tab[i].arg * RAD);
		a->tab[i+1].y = a->tab[i].y + a->tab[i].len * sinf(a->tab[i].arg * RAD);
	}
	a->ex = a->tab[COUNT-1].x + a->tab[COUNT-1].len * cosf(a->tab[COUNT-1].arg * RAD);
	a->ey = a->tab[COUNT-1].y + a->tab[COUNT-1].len * sinf(a->tab[COUNT-1].arg * RAD);
}

void SetAllAngle(arm* a, float a1, float a2, float a3, float a4, int a5)
{
	if(a1 != 150) a->tab[0].ang = a1;
	if(a2 != 150) a->tab[1].ang = a2;
	if(a3 != 150) a->tab[2].ang = a3;
	if(a4 != 150) a->tab[3].ang = a4;
	if(a5 != 150) a->tab[4].ang = a5;
}

void SetSlowAngle(arm* a, float a1, float a2, float a3, float a4, float a5, int ms)
{
	int i;
	float a12 = 0,a22 = 0, a32 = 0, a42 = 0, a52 = 0;
	
	int steps = ms / 50;
	if(a1 != 150.0f) a12 = (a1 - a->tab[0].ang) / steps;
	if(a2 != 150.0f) a22 = (a2 - a->tab[1].ang) / steps;
	if(a3 != 150.0f) a32 = (a3 - a->tab[2].ang) / steps;
	if(a4 != 150.0f) a42 = (a4 - a->tab[3].ang) / steps;
	if(a5 != 150.0f) a52 = (a5 - a->tab[4].ang) / steps;
	for(i = 0; i < steps; i++)
	{
		a->tab[0].ang += a12;
		a->tab[1].ang += a22;
		a->tab[2].ang += a32;
		a->tab[3].ang += a42;
		a->tab[4].ang += a52;
		Set(a);
		Move(*a);
		_delay_ms(50);
	}
}


void SetSlowOneAngle(arm* a, int nr, float angle, int ms)
{
	int i;
	float a12 = 0;
	
	int steps = ms / 50;
	a12 = (angle - a->tab[nr].ang) / steps;
	for(i = 0; i < steps; i++)
	{
		a->tab[nr].ang += a12;
		Set(a);
		Move(*a);
		_delay_ms(50);
	}
}


void Init(arm* a)
{
	a->tab[0].len = 12;
	a->tab[0].ang = 0;
	a->tab[0].arg = 0;
	
	a->tab[1].len = 12.5;
	a->tab[1].ang = 0;
	a->tab[1].arg = 0;
	
	a->tab[2].len = 13.5;
	a->tab[2].ang = 0;
	a->tab[2].arg = 0;
	
	a->tab[3].len = 9.7;
	a->tab[3].ang = 0;
	a->tab[3].arg = 0;
	
	a->tab[4].len = 9.7;
	a->tab[4].ang = 0;
	a->tab[4].arg = 0;

	a->tab[4].len = 9.7;
	a->tab[4].ang = -55;
	a->tab[4].arg = 0;
	
	a->tryb = 0;

	Set(a);
}

void Move(arm a)
{
	servoAngle0(a.tab[0].ang);
	servoAngle1(a.tab[1].ang);
	servoAngle2(a.tab[2].ang);
	servoAngle3(a.tab[3].ang);
	servoAngle4(a.tab[4].ang);
	servoAngle5(a.tab[5].ang);
}

void LoadFromMemory(arm* a, elem* e)
{
	SetSlowAngle(a,e->a1,e->a2,e->a3,e->a4,e->a5,500);
}



#endif /* KINEMATYKA_H_ */