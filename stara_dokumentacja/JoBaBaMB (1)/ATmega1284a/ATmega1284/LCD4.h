/*
**	Project:	4-Bit LCD
**	Purpose:	Interface a Hitachi-compatible LCD screen to an 8-bit
**				microcontroller making use of 7 bits of any port.  Timer
**				delay functions are used as the ready flag polling
**				is not needed in some applications.
**
**	Author:		Steven Pickles
**	Date:		Friday, April 22, 2005
**
**	Notes:		(1)	The LCD pinout is as follows:
**
**					01 - Ground (Vss)
**					02 - 5V Power (Vcc)
**					03 - Contrast (Vo)
**					04 - Register Select (RS)
**					05 - Read/Write_L (R/W_L)
**					06 - Enable (E)
**					07 - Data 0 (DB0)
**					08 - Data 1 (DB1)
**					09 - Data 2 (DB2)
**					10 - Data 3 (DB3)
**					11 - Data 4 (DB4)
**					12 - Data 5 (DB5)
**					13 - Data 6 (DB6)
**					14 - Data 7 (DB7)
**					15 - Backlight 5V Power (use 10 ohm resistor)
**					16 - Ground (Vss)
**
**				(2)	The port pinout is considered to be the following:
**
**					P7 - No connect (NC)
**					P6 - Enable (E)
**					P5 - Read/Write_L (R/W_L)
**					P4 - Register Select (RS)
**					P3 - Data 7 (DB7)
**					P2 - Data 6 (DB6)
**					P1 - Data 5 (DB5)
**					P0 - Data 4 (DB4)
*/

/*
**	Header File Information
*/
#ifndef __lcd_4bit_h__
#define __lcd_4bit_h__

/*
**	Compiler Include Directives
*/
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>



/*
**	Compiler Define Directives
*/

//	LCD Port Information (from microcontroller)
// #define	LCD_OUT		PORTA
// #define	LCD_IN		PINA
// #define	LCD_DDR		DDRA
// #define	ENABLE		6
// #define	RW			5
// #define	RS			4
// #define	D7			3
// #define	D6			2
// #define	D5			1
// #define	D4			0

//	Register Select Constants
#define	DATA_REGISTER		0
#define	COMMAND_REGISTER	1


#define LCD_RS_DIR		DDRD
#define LCD_RS_PORT 	PORTD
#define LCD_RS			(1 << PD7)

#define LCD_RW_DIR		DDRD
#define LCD_RW_PORT		PORTD
#define LCD_RW			(1 << PD6)

#define LCD_E_DIR		DDRD
#define LCD_E_PORT		PORTD
#define LCD_E			(1 << PD5)



#define LCD_DATA_DIR	DDRC
#define LCD_DATA_PORT	PORTC
#define LCD_DATA_PIN	PINC



/*
**	Function Declarations
*/

/*
**	Function:		initializeLCD
**	Parameters:		<none>
**	Purpose:		Initializes the LCD into the following mode:
**					<some mode>
**	Returns:		<none>
*/
void LCD_Initalize(void);

/*
**	Function:		waitForLCD
**	Parameters:		<none>
**	Purpose:		Polls the ready bit on the LCD to ensure that the
**					LCD is able to receive a new command or new data, or
**					delays for about 50 microseconds.
**	Returns:		<none>
*/
void waitForLCD(void);

/*
**	Function:		writeCharacterToLCD
**	Parameters:		character - unsigned character to be written to the LCD
**	Purpose:		Writes an 8-bit unsigned character to the LCD screen.
**	Returns:		<none>
*/
void writeCharacterToLCD(uint8_t character);

/*
**	Function:		writeNibbleToLCD
**	Parameters:		selectedRegister - either the command or data register
**									   that will be written to
**					nibble - the four bits to be written to the LCD
**	Purpose:		Writes a nibble to the LCD.
**	Returns:		<none>
*/
void writeNibbleToLCD(uint8_t selectedRegister, uint8_t nibble);

/*
**	Function:		writeByteToLCD
**	Parameters:		selectedRegister - either the command or data register
**									   that will be written to
**					byte - the eight bits to be written to the LCD
**	Purpose:		Writes and 8-bit value to the LCD screen.
**	Returns:		<none>
*/
void writeByteToLCD(uint8_t selectedRegister, uint8_t byte);

/*
**	Function:		writeStringToLCD
**	Parameters:		stringPointer - a pointer to the array of characters
**									that will be written to the LCD
**	Purpose:		Writes a series of bytes to the LCD data register so
**					that a character array may be given and displayed.
**	Returns:		<none>
*/
void LCD_WriteText(char* stringPointer);

/*
**	Function:		writeIntegerToLCD
**	Parameters:		integer - the integer that will be written to the LCD
**	Purpose:		Coverts a standard 16-bit int into the ASCII
**					representation of the number and writes that number
**					to the LCD.
**					*** Note:	The maximum value that can be written is
**								9999.  This is because there is no
**								ten thousands place support.
**	Returns		<none>
*/
void LCD_WriteInt(int integer);

/*
**	Function:		clearLCD
**	Parameters:		<none>
**	Purpose:		Writes the "clear and go home" command to the LCD.
**	Returns:		<none>
*/
void LCD_Clear(void);


/*
**	Global Variables
*/

volatile uint8_t position;

/*
**	Function Declarations
*/

/*
**	Function:		initializeLCD
**	Parameters:		<none>
**	Purpose:		Initializes the LCD into the following mode:
**					<some mode>
**	Returns:		<none>
*/
void LCD_Initalize(void)
{	
	//	Set the position counter to 0
	position = 0;
	
	//	Set the proper data direction for output
	//LCD_DDR = 0x7f;


	LCD_E_DIR 	|= LCD_E;
	LCD_RS_DIR 	|= LCD_RS;
	LCD_RW_DIR	|= LCD_RW;
	LCD_DATA_DIR = 0xFF;		

	//	First, delay50us for at least 15ms after power on
	_delay_ms(15);

_delay_ms(30); // oczekiwanie na ustalibizowanie si� napiecia zasilajacego
LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E
LCD_RW_PORT  &= ~LCD_RW;

for(int i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
{
	LCD_E_PORT |= LCD_E;
	LCD_DATA_PORT = 0x3F;
	LCD_E_PORT &= ~LCD_E;
	_delay_ms(10); // czekaj 10ms
}	
	
	//	Set for 4-bit LCD mode
	writeNibbleToLCD(COMMAND_REGISTER, 0x03);
	_delay_ms(5);
	writeNibbleToLCD(COMMAND_REGISTER, 0x03);
	_delay_ms(5);
	writeNibbleToLCD(COMMAND_REGISTER, 0x03);
	_delay_ms(5);
	writeNibbleToLCD(COMMAND_REGISTER, 0x02);
	_delay_ms(5);
	
	//	Function set
	writeByteToLCD(COMMAND_REGISTER, 0x28);
	_delay_ms(15);
	
	//	Turn display off
	writeByteToLCD(COMMAND_REGISTER, 0x08);
	_delay_ms(15);
	
	//	Clear LCD and return home
	writeByteToLCD(COMMAND_REGISTER, 0x01);
	_delay_ms(15);
	
	//	Turn on display, turn off cursor and blink
	//writeByteToLCD(COMMAND_REGISTER, 0x0c);
	writeByteToLCD(COMMAND_REGISTER, 0x0f);   // blinking cursor
	_delay_ms(15);
}


/*
**	Function:		waitForLCD
**	Parameters:		<none>
**	Purpose:		Polls the ready bit on the LCD to ensure that the
**					LCD is able to receive a new command or new data, or
**					delays for about 50 microseconds.
**	Returns:		<none>
*/
void waitForLCD(void)
{
	//	Poll the busy flag until it goes clear -- which means
	//	that new instructions may be given to the LCD screen
	//while((readByteFromLCD(COMMAND_REGISTER) & 0x80) == 0x80);
	
	//	Or.... delay 50us
	_delay_us(50);
}

/*
**	Function:		writeNibbleToLCD
**	Parameters:		selectedRegister - either the command or data register
**									   that will be written to
**					nibble - the four bits to be written to the LCD
**	Purpose:		Writes a nibble to the LCD.
**	Returns:		<none>
*/
void writeNibbleToLCD(uint8_t selectedRegister, uint8_t nibble)
{
	//	Pull the enable line high
	LCD_E_PORT |= LCD_E;
	//LCD_OUT = BV(ENABLE);

	//	Output the nibble to the LCD
	//LCD_OUT |= nibble;
	
	//	Determine if the register select bit needs to be set
	if(selectedRegister == DATA_REGISTER)
	{
		//	If so, then set it
		//LCD_OUT |= BV(RS);
		LCD_RS_PORT |= LCD_RS;
	}
	else
	{
		//	Otherwise, clear the register select bit
		//LCD_OUT &= ~BV(RS);
		LCD_RS_PORT &= ~LCD_RS;
		
	}
	LCD_DATA_PORT = ((nibble << 4) & 0xf0);
	//LCD_DATA_PORT = nibble;
	asm volatile("nop");
	//	Toggle the enable line to latch the nibble
	LCD_E_PORT &= ~LCD_E;
	asm volatile("nop");
}

/*
**	Function:		writeByteToLCD
**	Parameters:		selectedRegister - either the command or data register
**									   that will be written to
**					byte - the eight bits to be written to the LCD
**	Purpose:		Writes and 8-bit value to the LCD screen.
**	Returns:		<none>
*/
void writeByteToLCD(uint8_t selectedRegister, uint8_t byte)
{
	//	Generate the high and low part of the bytes that will be
	//	written to the LCD
	uint8_t upperNibble = byte >> 4;
	uint8_t lowerNibble = byte & 0x0f;
	
	//	Assuming a 4 line by 20 character display, ensure that
	//	everything goes where it is supposed to go:
	if(selectedRegister == DATA_REGISTER && position == 20)
	{
		writeByteToLCD(COMMAND_REGISTER, 0xC0);
		_delay_us(1250);
	}
	else if(selectedRegister == DATA_REGISTER && position == 40)
	{
		writeByteToLCD(COMMAND_REGISTER, 0x94);
		_delay_us(1250);
	}
	else if(selectedRegister == DATA_REGISTER && position == 60)
	{
		writeByteToLCD(COMMAND_REGISTER, 0xd4);
		_delay_us(1250);
	}
	
	//	Wait for the LCD to become ready
	waitForLCD();
	
	//	First, write the upper four bits to the LCD
	writeNibbleToLCD(selectedRegister, upperNibble);
	
	//	Finally, write the lower four bits to the LCD
	writeNibbleToLCD(selectedRegister, lowerNibble);
	
	//	Reset the position count if it is equal to 80
	if(selectedRegister == DATA_REGISTER && ++position == 80)
	position = 0;
}


/*
**	Function:		writeStringToLCD
**	Parameters:		stringPointer - a pointer to the array of characters
**									that will be written to the LCD
**	Purpose:		Writes a series of bytes to the LCD data register so
**					that a character array may be given and displayed.
**	Returns:		<none>
*/
void LCD_WriteText(char* stringPointer)
{
	//	So long as the data that is pointed to by the string pointer
	//	is valid (ie, not the null pointer) then the data will be written
	//	to the LCD
	while (*stringPointer)
	writeByteToLCD(DATA_REGISTER, *stringPointer++);
}

/*
**	Function:		writeIntegerToLCD
**	Parameters:		integer - the integer that will be written to the LCD
**	Purpose:		Coverts a standard 16-bit int into the ASCII
**					representation of the number and writes that number
**					to the LCD.
**					*** Note:	The maximum value that can be written is
**								9999.  This is because there is no
**								ten thousands place support.
**	Returns		<none>
*/
void LCD_WriteInt(int integer)
{
	char text[10];
	sprintf(text,"%d",integer);
	LCD_WriteText(text);
}

/*
**	Function:		clearLCD
**	Parameters:		<none>
**	Purpose:		Writes the "clear and go home" command to the LCD.
**	Returns:		<none>
*/
void LCD_Clear(void)
{
	//	Send command to LCD (0x01)
	writeByteToLCD(COMMAND_REGISTER, 0x01);
	
	//	Set the current position to 0
	position = 0;
	_delay_ms(1.64);
}

/*
**	Function:		moveToXY
**	Parameters:		row - the row to be moved to
**					column - the column to be moved to
**	Purpose:		Moves the cursor to the requested (row, column) position
**					on the LCD.  This function was originally written by
**					William Dubel (dubel@ufl.edu).
**	Returns:		<none>
*/
void LCD_GoTo(uint8_t column, uint8_t row)
{
	//	Determine the new position
	position = (row * 20) + column;
	
	//	Send the correct commands to the command register of the LCD
	if(position < 20)
	writeByteToLCD(COMMAND_REGISTER, 0x80 | position);
	else if(position >= 20 && position < 40)
	writeByteToLCD(COMMAND_REGISTER, 0x80 | ((position % 20) + 0x40));
	else if(position >= 41 && position < 60)
	writeByteToLCD(COMMAND_REGISTER, 0x80 | ((position % 40) + 0x14));
	else if(position >= 60 && position < 80)
	writeByteToLCD(COMMAND_REGISTER, 0x80 | ((position % 60) + 0x54));
	_delay_ms(2);
}

#endif