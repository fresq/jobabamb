/*
 * FastMatrix.h
 *
 * Created: 2013-12-10 23:35:55
 *  Author: Admin
 */ 


#ifndef FASTMATRIX_H_
#define FASTMATRIX_H_


#include <math.h>

typedef struct 
{
	float* tab;
	int r, c;
} FastMatrix;

inline void fmCreate(FastMatrix * m, int r, int c)
{
	m->tab = (float*)malloc(sizeof(float) * r * c);
	m->r = r;
	m->c = c;
}

inline void fmCopy(FastMatrix* from, FastMatrix* to)
{
	for(int i = 0; i < from->c * from->r; i ++)
	to->tab[i] = from->tab[i];
}

inline void fmDestroy(FastMatrix* m)
{
	free(m->tab);
}

inline void fmAdd(FastMatrix* m1, FastMatrix* m2, FastMatrix* r)
{
	for(int i = 0; i < m1->r * m1->c; i++)
	r->tab[i] = m1->tab[i] + m2->tab[i];
}

inline void fmSub(FastMatrix* m1, FastMatrix* m2, FastMatrix* r)
{
	for(int i = 0; i < m1->r * m1->c; i++)
	r->tab[i] = m1->tab[i] - m2->tab[i];
}

inline void fmMulf(FastMatrix* m1, float f, FastMatrix* r)
{
	for(int i = 0; i < m1->r * m1->c; i++)
	r->tab[i] = m1->tab[i] *  f;
}

inline void fmMulfS(FastMatrix* m1, float f)
{
	for(int i = 0; i < m1->r * m1->c; i++)
	m1->tab[i] = m1->tab[i] *  f;
}

inline void fmMul(FastMatrix* m1, FastMatrix* m2, FastMatrix* r)
{
	for(int i = 0; i < m1->r; i++)
	for(int j = 0; j < m2->c; j++)
	{
		float a = 0;
		//sumujemy iloczyny odpowiednich elementow
		for(int k=0; k < m1->c; k++)
		a = a + m1->tab[i * m1->c + k] * m2->tab[k * m2->c + j];
		r->tab[i * r->c + j] = a;
	}
}

inline void fmTrans(FastMatrix* m1, FastMatrix* r)
{
	for(int i=0; i < m1->r; i++)             //i teraz kopijemy dane
	for(int j=0; j < m1->c; j++)      //sprowadza sie to do tego
	r->tab[j *r->c + i] = m1->tab[i * m1->c + j]; //ze zmaieniamy kolejnosc indeksow
}

inline float fmNorm(FastMatrix* m)
{
	float w = 0;
	for(int i = 0; i < m->r*m->c; i++)
	w += m->tab[i] * m->tab[i];
	return sqrt(w);
}

//Zamiana dwoch wierszy
void fmSwitchR(FastMatrix* m, int r1, int r2)
{
	for(int i = 0; i < m->c; i++)
	{
		float a = m->tab[r1 * m->c + i];
		m->tab[r1 * m->c + i] = m->tab[r2 * m->c + i];
		m->tab[r2 * m->c + i] = a;
	}
}

inline void fmPrint(FastMatrix* m)
{
	for(int i = 0; i < m->r; i++)
	{
		for(int j  = 0; j < m->c; j++)
		{
			//out << setw(4) << m->tab[i * m->c + j] << ((j != m->c - 1) ? "|" : "\n");
		}
	}
}

void fmMulR(FastMatrix* m, int r, float a)
{
	for(int i = 0; i < m->c; i++)
	m->tab[r * m->c + i] = a * m->tab[r * m->c + i];
}

//Dodanie do wiersza r1 wiersza r2 pomnozonego przez stala;
void fmAddMulR(FastMatrix* m, int r1, int r2, float a)
{
	for(int i = 0; i < m->c; i++)
	m->tab[r1 * m->c + i] = m->tab[r1 * m->c + i] + a * m->tab[r2 * m->c + i];
}

inline float fmDetEvenFaster(FastMatrix* m, FastMatrix* tmp)
{
	fmCopy(m, tmp);

	float wynik = 1; //zmienna przechowujaca wynik
	for(int i = 0; i < m->c; i++)
	{
		if(tmp->tab[i * m->c + i] < 0.001)
		{
			for(int j = i + 1; j < m->r; j++)
			if(tmp->tab[i * m->c + j] > 0) {fmSwitchR(tmp,i,j); break;}
		}
		for(int j = i + 1; j < m->r; j++)
		fmAddMulR(tmp, j, i, - tmp->tab[j * m->c + i]/tmp->tab[i * m->c + i]);
	}
	for(int i = 0; i < m->c; i++)
	{
		wynik = wynik * tmp->tab[i * m->c + i];
	}

	//zwracamy wyniki
	return wynik;
}

inline void fmInvertEvenFaster(FastMatrix* m, FastMatrix* r, FastMatrix* tmp)
{
	int n = m->c;
	int n2 = 2 * n;
	for(int i = 0; i < n; i++)
	for(int j = 0; j < n; j++)
	tmp->tab[i * 2 * n + j] = m->tab[i * n + j];
	for(int i = 0; i < n; i++)
	for(int j = n; j < 2 * n; j++)
	tmp->tab[i * 2 * n + j] = ((j - n == i) ? 1 : 0);

	for(int i = 0; i < n; i++)
	{
		if(tmp->tab[i * 2  *n + i] < 0.001)
		for(int j = i + 1; j < n; j++)
		if(tmp->tab[i * 2 * n + j] > 0) {fmSwitchR(tmp,i,j); break;}
		for(int j = i + 1; j < n; j++)
		fmAddMulR(tmp, j, i, - tmp->tab[j * n2 + i]/tmp->tab[i * n2 + i]);
	}

	for(int i = n - 1; i >= 0; i--)
	{
		for(int j = i - 1; j >= 0; j--)
		fmAddMulR(tmp, j, i, - tmp->tab[j * n2 + i]/tmp->tab[i * n2 + i]);
	}

	for(int i = 0; i < n; i++)
	{
		fmMulR(tmp, i, 1 / tmp->tab[i * n2 + i]);
	}

	for(int i = 0; i < n; i++)
	for(int j = n; j < n2; j++)
	r->tab[i * n + j - n] = tmp->tab[i * n2 + j];
}

inline float fmGet(FastMatrix* m, int r, int c)
{
	return m->tab[r * m->c + c];
}

inline void fmSet(FastMatrix* m, int r, int c, float f)
{
	m->tab[r * m->c + c] = f;
}



#endif /* FASTMATRIX_H_ */