/*
 * Jazda.h
 *
 * Created: 2013-10-13 15:34:56
 *  Author: Admin
 */ 


#ifndef JAZDA_H_
#define JAZDA_H_

#define MOTOR1_DDR DDRD
#define MOTOR2_DDR DDRD
#define MOTOR3_DDR DDRD
#define MOTOR4_DDR DDRD
#define MOTOR1_PORT PORTF
#define MOTOR2_PORT PORTF
#define MOTOR3_PORT PORTF
#define MOTOR4_PORT PORTF
#define  MOTOR1 _BV(4)
#define  MOTOR2 _BV(5)
#define  MOTOR3 _BV(3)
#define  MOTOR4 _BV(2)

#define M1SET MOTOR1_PORT |= MOTOR1
#define M1CLR MOTOR1_PORT &= ~MOTOR1
#define M2SET MOTOR2_PORT |= MOTOR2
#define M2CLR MOTOR2_PORT &= ~MOTOR2
#define M3SET MOTOR3_PORT |= MOTOR3
#define M3CLR MOTOR3_PORT &= ~MOTOR3
#define M4SET MOTOR4_PORT |= MOTOR4
#define M4CLR MOTOR4_PORT &= ~MOTOR4

#define MOTOR_PWM1 OCR1B
#define MOTOR_PWM2 OCR3B
#define MOTOR_MAX 0x5FFE

void InitJazda()
{
	MOTOR1_DDR |= MOTOR1;
	MOTOR2_DDR |= MOTOR2;
	MOTOR3_DDR |= MOTOR3;
	MOTOR4_DDR |= MOTOR4;
	M1CLR;
	M2CLR;
	M3CLR;
	M4CLR;
}

int lastLewy = 0, lastPrawy = 0;

void Jazda(int lewy, int prawy)
{
	if(lewy >= 0)
	{
		if(lewy>MOTOR_MAX)
		lewy = MOTOR_MAX;
		M1SET;
		M2CLR;
	}
	else
	{
		if(lewy<-MOTOR_MAX)
		lewy = -MOTOR_MAX;
		M1CLR;
		M2SET;
	}
	
	if(prawy >= 0)
	{
		if(prawy>MOTOR_MAX)
		prawy = MOTOR_MAX;
		M3SET;
		M4CLR;
	}
	else
	{
		if(prawy<-MOTOR_MAX)
		prawy = -MOTOR_MAX;
		M3CLR;
		M4SET;
	}
	
	MOTOR_PWM1 = abs(lewy);
	MOTOR_PWM2 = abs(prawy);
}

int ileKrokow = 4;

void JazdaP(float lewy, float prawy)
{
	int deltaLewy =  (MOTOR_MAX * lewy - lastLewy) / ileKrokow;
	int deltaPrawy =  (MOTOR_MAX * prawy - lastPrawy) / ileKrokow;
	for(int i = 0; i < ileKrokow; i++)
	{
		lastLewy += deltaLewy;
		lastPrawy += deltaPrawy;
		Jazda(lastLewy, lastPrawy);
		_delay_ms(20);
	}
	lastLewy = MOTOR_MAX * lewy;
	lastPrawy = MOTOR_MAX * prawy;
	LCD_Clear();
	LCD_WriteInt(100*lewy);
	LCD_GoTo(0,1);
	LCD_WriteInt(100*prawy);
}


void MJazda()
{
	int cmd;
	LCD_Clear();
	//int ile = 0;
	while(1)
	{
		cmd = detect();
		if(cmd > 0)
		{
			cmd &= ~2048;
			if(cmd == KEY_ESC)
			{
				Jazda(0,0);
				return;
			}
			if(cmd == KEY_LEFT)
			{
				JazdaP(0.9f,-0.1f);
			}
			if(cmd == KEY_RIGHT)
			{
				JazdaP(-0.1f,0.9f);
			}
			if(cmd == KEY_UP)
			{
				JazdaP(0.8f,0.8f);
			}
			if(cmd == KEY_DOWN)
			{
				JazdaP(-0.8f,-0.8f);
			}
			if(cmd == KEY_1)
			{
				JazdaP(-0.8f,0);
			}
			if(cmd == KEY_2)
			{
				JazdaP(0,-0.8f);
			}
			if(cmd == KEY_3)
			{
				JazdaP(0.8f,0);
			}
			if(cmd == KEY_4)
			{
				JazdaP(0,0.8f);
			}
		}
		_delay_ms(100);
	}
}

#endif /* JAZDA_H_ */