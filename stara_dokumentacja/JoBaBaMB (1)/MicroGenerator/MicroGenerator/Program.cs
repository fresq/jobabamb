﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication1
{
	class Program
	{
		static int MICRO_COUNT = 50;
		static int MAX_VALUE = 10000;

		public static double f(double x)
		{
			//return MAX_VALUE * Math.Sin(x);
			return MAX_VALUE * x;
		}

		static void Main(string[] args)
		{
			List<string> lines = new List<string>();
			string line = "{";
			for (int i = 0; i < MICRO_COUNT; i++)
			{
				//double x = (0.5 * Math.PI * (double)i) / ((double)MICRO_COUNT - 1.0);
				double x = (i) / ((double)MICRO_COUNT - 1.0);
				double y = f(x);
				line += ((int)y).ToString() + ",";
				lines.Add(((int)y).ToString());
			}
			line = line.Substring(0, line.Length - 1);
			line += "}";
			Console.Write(line);
			Console.ReadKey();
			File.WriteAllText("dane.txt", line);
			File.WriteAllLines("dane2.txt", lines);
		}
	}
}
