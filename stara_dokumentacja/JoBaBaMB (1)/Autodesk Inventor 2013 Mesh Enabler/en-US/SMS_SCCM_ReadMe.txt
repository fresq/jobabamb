SMS and SCCM Integration ReadMe
Improvements in Autodesk® deployment technology allow many administrators to streamline their process of integrating Autodesk deployments on Microsoft® Systems Management Server (SMS) and System Center Configuration Manager (SCCM). In most cases, Autodesk deployments can now be advertised using a single program line instead of individual program lines for each component in the deployment. 
To advertise a deployment with a single program line, use the following syntax:
.\AdminImage\Setup.exe /W /Q /I .\AdminImage\<deployment name>.ini /language en-us
This sample command line assumes that the package contents are defined at the root folder of your Autodesk product deployment folder, and that the product language is US English (en-us).

Software Requirements
Before creating an SCCM advertisement, ensure that all target computers have Microsoft .NET 4.0 Framework installed. Windows XP computers need Direct X installed prior to receiving a 2011 or later Autodesk product advertisement.

Component-level Advertisement
Special circumstances such as execution time limits may require you to advertise an Autodesk deployment at the component level. Please refer to <deployment name>_SCCM.txt for a listing of required program lines in the order they must occur on target computers. These commands will vary by deployment. For long command lines, use a batch file.
For more information about Autodesk deployment technology and how it integrates with SMS and SCCM, see the Network Administrator’s Guide, which is included with your Autodesk product.
