﻿
#define SERVO_PORT  PORTA 
#define SERVO_DDR   DDRA 

// Up to 8 servos (since pulses are generated in 
// sequence + only one port is used). 
#define N_SERVOS    5 

// Servo times (this is Futaba timing). 
#define SERVO0_MIN    480 // microseconds 
#define SERVO0_MAX   2320 // microseconds 
#define SERVO0_MID   1400 

#define SERVO1_MIN    460 // microseconds 
#define SERVO1_MAX   2100 // microseconds 
#define SERVO1_MID   1280 

#define SERVO2_MIN    440 // microseconds 
#define SERVO2_MAX   2220 // microseconds 
#define SERVO2_MID   1330 

// Time between servo pulses.  
#define SERVO_FRAME 20000 // microseconds (50Hz) 

// Time slot available for each servo. 
#define SERVO_TIME_DIV (SERVO_FRAME / N_SERVOS) 

#if (SERVO_TIME_DIV < SERVO0_MAX + 50) 
#warning "Output fewer servo signals or increase SERVO_FRAME" 
#endif 
#if ((SERVO_TIME_DIV * (F_CPU / 1000000UL)) >= 0xFF00) 
#warning "Output more servo signals or decrease SERVO_FRAME (or use the prescaler)" 
#endif 

// Computing timer ticks given microseconds. 
// Note, this version works out ok with even MHz F_CPU (e.g., 1, 2, 4, 8, 16 MHz). 
// (Not a good idea to have this end up as a floating point operation) 
#define US2TIMER1(us) ((us) * (uint16_t)(F_CPU / 1E6)) 

// Servo times - to be entered as timer1 ticks (using US2TIMER1). 
// This must be updated with interrupts disabled. 
volatile uint16_t servoTime[N_SERVOS]; 

// Servo output allocation (on a single port currently). 
const static uint8_t servoOutMask[N_SERVOS] = { 
    0b00000001, // PX0 
    0b00000010, // PX1 
    0b00000100, // PX2 
    0b00001000, // PX3 
    0b00010000, // PX4 
    0b00100000, // PX5 
    0b01000000, // PX6 
    0b10000000, // PX7 
}; 
// Servo mask is just the above masks ored. 
#define SERVO_MASK 0xff 

void servoStart(void) 
{ 
    // Outputs 
    SERVO_DDR |= SERVO_MASK; 
    // Setupt a first compare match 
    OCR1A = TCNT1 + US2TIMER1(100); 
    // start timer 1 with no prescaler 
    TCCR1B = (1 << CS10);       
    // Enable interrupt 
    TIMSK1 |= (1 << OCIE1A); 
} 

void servoSet(uint8_t servo, uint16_t time /* microseconds */)
{
    uint16_t ticks = US2TIMER1(time);
    cli();
    servoTime[servo] = ticks;
    sei();
}

//void servoAngle0(float angle)
//{
	//float dz = (SERVO0_MAX - SERVO0_MIN) / 180.0f;
//
	//servoSet(0, SERVO0_MID + (uint16_t)(angle * dz));
//}

void servoAngle1(float angle)
{
	float dz = (SERVO1_MAX - SERVO1_MIN) / 180.0f;

	servoSet(3, SERVO1_MID + (uint16_t)(angle * dz));
}

void servoAngle2(float angle)
{
	float dz = (SERVO2_MAX - SERVO2_MIN) / 180.0f;

	servoSet(3, SERVO2_MID + (uint16_t)(angle * dz));
}

void servoAngle3(float angle)
{
	float dz = (SERVO2_MAX - SERVO2_MIN) / 180.0f;

	servoSet(2, SERVO2_MID + (uint16_t)(angle * dz));
}

void servoAngle4(float angle)
{
	uint16_t time = (uint16_t)(11*angle) + 1780;

	servoSet(1, time);
}

void servoAngle0(float angle)
{
	uint16_t time = (uint16_t)(11*angle) + 1780;

	servoSet(4, time);
}

void servoAngle5(float angle)
{
	uint16_t time = (uint16_t)(32/3*angle) + 1680;

	servoSet(0, time);
}

void servoAngle6(float angle)
{
	uint16_t time = (uint16_t)(94/9*angle) + 1650;

	servoSet(2, time);
}