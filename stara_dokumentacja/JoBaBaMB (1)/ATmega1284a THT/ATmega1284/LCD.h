﻿//SEKCJA: LCD I/O
#define LCD_RS_DIR		DDRB
#define LCD_RS_PORT 	PORTB
#define LCD_RS			(1 << PB0)

#define LCD_RW_DIR		DDRB
#define LCD_RW_PORT		PORTB
#define LCD_RW			(1 << PB1)

#define LCD_E_DIR		DDRB
#define LCD_E_PORT		PORTB
#define LCD_E			(1 << PB2)

#define LCD_DATA_DIR	DDRC
#define LCD_DATA_PORT	PORTC
#define LCD_DATA_PIN	PINC
//EOS: LCD I/O

//SEKCJA HD44780
#define HD44780_CLEAR					0x01

#define HD44780_HOME					0x02

#define HD44780_ENTRY_MODE				0x04
	#define HD44780_EM_SHIFT_CURSOR		0
	#define HD44780_EM_SHIFT_DISPLAY	1
	#define HD44780_EM_DECREMENT		0
	#define HD44780_EM_INCREMENT		2

#define HD44780_DISPLAY_ONOFF			0x08
	#define HD44780_DISPLAY_OFF			0
	#define HD44780_DISPLAY_ON			4
	#define HD44780_CURSOR_OFF			0
	#define HD44780_CURSOR_ON			2
	#define HD44780_CURSOR_NOBLINK		0
	#define HD44780_CURSOR_BLINK		1

#define HD44780_DISPLAY_CURSOR_SHIFT	0x10
	#define HD44780_SHIFT_CURSOR		0
	#define HD44780_SHIFT_DISPLAY		8
	#define HD44780_SHIFT_LEFT			0
	#define HD44780_SHIFT_RIGHT			4

#define HD44780_FUNCTION_SET			0x20
	#define HD44780_FONT5x7				0
	#define HD44780_FONT5x10			4
	#define HD44780_ONE_LINE			0
	#define HD44780_TWO_LINE			8
	#define HD44780_4_BIT				0
	#define HD44780_8_BIT				16

#define HD44780_CGRAM_SET				0x40

#define HD44780_DDRAM_SET				0x80
//EOS: HD44780


//SEKCJA: LCD FUNC
unsigned char _LCD_Read(void)
{
unsigned char tmp = 0;

LCD_DATA_DIR = 0x00;

LCD_RW_PORT |= LCD_RW;
LCD_E_PORT |= LCD_E;
asm("nop");
tmp = LCD_DATA_PIN;
LCD_E_PORT &= ~LCD_E;
return tmp;
}
unsigned char LCD_ReadStatus(void)
{
LCD_RS_PORT &= ~LCD_RS;
return _LCD_Read();
}
void _LCD_Write(unsigned char dataToWrite)
{
LCD_DATA_DIR = 0xFF;

LCD_RW_PORT &= ~LCD_RW;
LCD_E_PORT |= LCD_E;
LCD_DATA_PORT = dataToWrite;
LCD_E_PORT &= ~LCD_E;
_delay_ms(4);
}
void LCD_WriteCommand(unsigned char commandToWrite)
{
LCD_RS_PORT &= ~LCD_RS;
_LCD_Write(commandToWrite);
while(LCD_ReadStatus()&0x80);
}
void LCD_WriteData(unsigned char dataToWrite)
{
LCD_RS_PORT |= LCD_RS;
_LCD_Write(dataToWrite);
while(LCD_ReadStatus()&0x80);
_delay_ms(5);
}
unsigned char LCD_ReadData(void)
{
LCD_RS_PORT |= LCD_RS;
return _LCD_Read();
}
void LCD_WriteText(char * text)
{
while(*text)
  LCD_WriteData(*text++);
  
 _delay_ms(3);
}

void LCD_WriteInt(int a)
{
	char text[10];
	sprintf(text,"%d",a);
	LCD_WriteText(text);
}

void LCD_GoTo(unsigned char x, unsigned char y)
{
LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}
void LCD_Clear(void)
{
LCD_WriteCommand(HD44780_CLEAR);
_delay_ms(3);
}
void LCD_Home(void)
{
LCD_WriteCommand(HD44780_HOME);
_delay_ms(1);
}
void LCD_Initalize(void)
{
unsigned char i;
LCD_DATA_DIR = 0xFF;
LCD_E_DIR 	|= LCD_E;
LCD_RS_DIR 	|= LCD_RS;
LCD_RW_DIR	|= LCD_RW;
_delay_ms(30); // oczekiwanie na ustalibizowanie się napiecia zasilajacego
LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E
LCD_RW_PORT  &= ~LCD_RW;

for(i = 0; i < 3; i++) // trzykrotne powtórzenie bloku instrukcji
{
LCD_E_PORT |= LCD_E;
LCD_DATA_PORT = 0x3F;
LCD_E_PORT &= ~LCD_E;
_delay_ms(10); // czekaj 10ms
}
LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_8_BIT); // interfejs 4-bity, 2-linie, znak 5x7
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wyłączenie wyswietlacza
LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartosći pamieci DDRAM
LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // włącz LCD, bez kursora i mrugania
}
//EOS: LCD FUNC


//TEMPORARY CREDITS
#define d1 63

int w1 = 15;

char tab1[d1+1] = "Pawel Joniak, Michal Burdka, Emil Barczynski, Bartek Witkowski.";

void Credits()
{
	LCD_Home();

	int spacje = (w1<0)?0:w1;

	int u1 = (w1<0)?w1:0;
		
	int max1 =((d1>16-w1)?(16-w1):(d1));

	for(int i = 0; i<spacje; i++)
		LCD_WriteData(' ');
	for(int i = -u1; i<max1; i++)
		LCD_WriteData(tab1[i]);		
	for(int i = 0; i<16 - (w1+max1); i++)
		LCD_WriteData(' ');	
	w1--;
		
	if(w1<-d1+1) w1 = 15;
}

//EOF CREDITS