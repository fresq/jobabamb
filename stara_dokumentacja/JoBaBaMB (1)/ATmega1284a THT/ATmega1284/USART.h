﻿/*
 * USART.h
 *
 * Created: 2012-05-04 18:44:10
 *  Author: Paweł Joniak
 */ 


#ifndef USART_H_
#define USART_H_


#define      PREDKOSC_USART     2400                      //Prędkość transmisji 
#define      W_U2X0            0                           //Prędkości transmisji: normalna/podwojona 
//
//#if(W_U2X0)                                             //Jeżeli opcja podwojenia częstotkiwości załączona 
   //#define      BAUD   ((F_CPU / (PREDKOSC_USART * 8UL)) - 1)      //Wzór na wyliczenie "BAUD" 
//#else                                                //Jeżeli opcja podwojenia częstotkiwości wyłączona 
   #define      BAUD   ((F_CPU / (PREDKOSC_USART * 16UL)) - 1)      //Wzór na wyliczenie "BAUD" 
//#endif                                                //Koniec warunku 

#define      W_UBRRL            (unsigned char)(BAUD)                  //Wyłuskaj niższa część "BAUD" 
#define      W_UBRRH            (unsigned char)(BAUD >> 8)               //Wyłuskaj wyższa część "BAUD" 

//Four variables for buffering of received messages
volatile int8_t znak; //The temp place to dump the character we just received
volatile int8_t buffer[50]; // The array holding the characters that have been sent
volatile int bufferlocation; //The current location in the buffer
volatile int flag;

//Declare the location of the stream for the output
void USART_Transmite(char c){
	// Wait until UDR ready
	while(!(UCSR1A & (1 << UDRE1)));
	UDR1 = c;    // send character
}

unsigned char USART_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSR1A & (1<<RXC1)) );
	/* Get and return received data from buffer */
	return UDR1;
}
void USART_SendText(char* s)
{
	do
	{
		while ( !( UCSR1A & (1<<UDRE1)) );
		UDR1 = *s;
	}while(*s++!='\0');
}

//USART RX interrupt this code is executed when we recieve a character
ISR(USART1_RX_vect)
{
	flag = 1;
	znak = UDR1; //Read the value out of the UART buffer
	buffer[bufferlocation] = znak; //Dump that value out into the buffer array
	bufferlocation++;	//Increment the bufferlocation so its ready to write next time
}
 //
void USARTInit()
{
   UBRR1H = W_UBRRH;                                    //Ustaw rejestr "UBRR0H" 
   UBRR1L = W_UBRRL;                                    //Ustaw rejestr "UBRR0L" 

   //UCSR1A |= (W_U2X0 << U2X0);                           //W zależności od wybranej opcji 
   //UCSR1B = 0b10010000; 
   //UCSR1C = 0b00100110; 
  
   UCSR1B = (1 << TXEN1) | (1 << RXEN1);
   //// Enable receive complete interrupt
   UCSR1B |= (1 << RXCIE1);
   //// Asynch, 8 data, no parity, 2 stop
   //UCSR1C = (1 << USBS1)  UCSR1C |= (1 << UCSZ11);
   UCSR1C = (1<<USBS1)|(3<<UCSZ10);
   
}




#endif /* USART_H_ */