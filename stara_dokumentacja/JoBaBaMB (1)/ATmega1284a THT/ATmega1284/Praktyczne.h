/*
 * Praktyczne.h
 *
 * Created: 2013-01-02 17:45:41
 *  Author: Admin
 */ 


#ifndef PRAKTYCZNE_H_
#define PRAKTYCZNE_H_

typedef struct  
{
	char* nazwa;
	int a1, a2, a3, a4, a5;
} elem;

elem* NewElem(char* n, int a1, int a2, int a3, int a4, int a5)
{
	elem* e = malloc(sizeof(elem));
	e->nazwa = n;
	e->a1 = a1;
	e->a2 = a2;
	e->a3 = a3;
	e->a4 = a4;
	e->a5 = a5;
	return e;
}

typedef struct
{
	int ile;
	elem* tab[5] ;	
} Memory;



void InitMemory(Memory* m)
{
	m->ile = 5;
	m->tab[0] = NewElem("Viagra",0,0,0,0, -55);//-55
	m->tab[1] = NewElem("Ustalona",0,-67,91,82, -55);
	m->tab[2] = NewElem("Samochwyt",-57,66,90,82, -30);//-40
	m->tab[3] = NewElem("Nazwa3",0,0,0,0,-55);
	m->tab[4] = NewElem("Nazwa4",0,0,0,0,-55);
}



#endif /* PRAKTYCZNE_H_ */