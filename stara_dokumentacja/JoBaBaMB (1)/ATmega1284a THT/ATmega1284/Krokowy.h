/*
 * Krokowy.h
 *
 * Created: 2013-01-02 20:02:11
 *  Author: Admin
 */ 


#ifndef KROKOWY_H_
#define KROKOWY_H_

#define KROK_DIR DDRD
#define KROK_PORT PORTD

#define ZERO_PORT PORTB
#define ZERO_DIR DDRB

#define KROK_1 _BV(0)
#define KROK_2 _BV(1)
#define KROK_3 _BV(4)
#define KROK_4 _BV(5)

#define ZERO_PIN _BV(3)
#define  ZEROWA bit_is_set(PINB,3)




volatile int krok = 0;
volatile int krok_poz = 180;

void InitKrokowy()
{
	ZERO_DIR &= ~ZERO_PIN;
	ZERO_PORT &= ~ZERO_PIN;
	
	KROK_DIR |= KROK_1;
	KROK_DIR |= KROK_2;
	KROK_DIR |= KROK_3;
	KROK_DIR |= KROK_4;
	
	KROK_PORT &= ~KROK_1;
	KROK_PORT &= ~KROK_2;
	KROK_PORT &= ~KROK_3;
	KROK_PORT &= ~KROK_4;
	
	//znalezienie pozycj zero
	PCICR |= _BV(PCIE1);
	PCMSK1 |= _BV(PCINT11);
	
}



void Krok(int lewo);

volatile int zerowa;

void ZerujKrokowy()
{
	zerowa = 0;
	while(!zerowa)
	{
		Krok(1);
	}
	krok_poz = 180;
	//while(1)
	//{
		//if(ZEROWA) {LCD_GoTo(0,0); LCD_WriteText("Zerowa");}
		//else {LCD_GoTo(0,0); LCD_WriteText("Nie");} 
	//}
}

ISR(PCINT1_vect)
{
	zerowa = 1;
	krok_poz = 180;
	//LCD_WriteText("przerwanie");
}

void KroczDo(int cel, int lewo)
{
	while(krok_poz != cel)
		Krok(lewo);
}


void Krocz(int ile, int lewo)
{
	for (int i = 0; i < ile; i++)
	{
		Krok(lewo);
	}
}

void KrokowySetCoils(int c1, int c2, int c3, int c4)
{
	if(c1) 	KROK_PORT |= KROK_1; else KROK_PORT &= ~KROK_1;
	if(c2) 	KROK_PORT |= KROK_2; else KROK_PORT &= ~KROK_2;
	if(c3) 	KROK_PORT |= KROK_3; else KROK_PORT &= ~KROK_3;
	if(c4) 	KROK_PORT |= KROK_4; else KROK_PORT &= ~KROK_4;
}

void Krok(int lewo)
{
	if(lewo)
	{
		krok++;
		krok_poz++;
	}		
	else
	{
		krok--;
		krok_poz--;
	}		
		
	if(krok == -1) krok = 7;
	if(krok == 8) krok = 0;
						
	if(krok == 0)
	{
		KROK_PORT |= KROK_1;
		KROK_PORT &= ~KROK_2;
		KROK_PORT &= ~KROK_3;
		KROK_PORT &= ~KROK_4;
	}
	else if(krok == 1)
	{
		KROK_PORT |= KROK_1;
		KROK_PORT &= ~KROK_2;
		KROK_PORT |= KROK_3;
		KROK_PORT &= ~KROK_4;
	}
	else if(krok == 2)
	{
		KROK_PORT &= ~KROK_1;
		KROK_PORT &= ~KROK_2;
		KROK_PORT |= KROK_3;
		KROK_PORT &= ~KROK_4;
	}
	else if(krok == 3)
	{
		KROK_PORT &= ~KROK_1;
		KROK_PORT |= KROK_2;
		KROK_PORT |= KROK_3;
		KROK_PORT &= ~KROK_4;
	}
	else if(krok == 4)
	{
		KROK_PORT &= ~KROK_1;
		KROK_PORT |= KROK_2;
		KROK_PORT &= ~KROK_3;
		KROK_PORT &= ~KROK_4;
	}
	else if(krok == 5)
	{
		KROK_PORT &= ~KROK_1;
		KROK_PORT |= KROK_2;
		KROK_PORT &= ~KROK_3;
		KROK_PORT |= KROK_4;
	}	
	else if(krok == 6)
	{
		KROK_PORT &= ~KROK_1;
		KROK_PORT &= ~KROK_2;
		KROK_PORT &= ~KROK_3;
		KROK_PORT |= KROK_4;
	}	
	else if(krok == 7)
	{
		KROK_PORT |= KROK_1;
		KROK_PORT &= ~KROK_2;
		KROK_PORT &= ~KROK_3;
		KROK_PORT |= KROK_4;
	}	
	_delay_ms(200);
}

void SpecialnyKrok(int k)
{
		if(krok == 0)
		{
			KROK_PORT |= KROK_1;
			KROK_PORT &= ~KROK_2;
			KROK_PORT &= ~KROK_1;
		}
		else if(krok == 1)
		{
			KROK_PORT |= KROK_3;
			KROK_PORT &= ~KROK_4;
			_delay_ms(200);
			KROK_PORT &= ~KROK_3;
		}
		else if(krok == 2)
		{
			KROK_PORT |= KROK_3;
			KROK_PORT &= ~KROK_4;
			_delay_ms(200);
			KROK_PORT &= ~KROK_3;
		}
		else
		{
			KROK_PORT |= KROK_2;
			KROK_PORT &= ~KROK_1;
			_delay_ms(200);
			KROK_PORT &= ~KROK_2;
		}
}



#endif /* KROKOWY_H_ */