/*
 * Pompka.h
 *
 * Created: 2013-04-05 18:56:09
 *  Author: Admin
 */ 


#ifndef POMPKA_H_
#define POMPKA_H_

uint8_t wypelnienie;

#define POMPKA_DDR DDRD
#define POMPKA_NR 7

void InitPompka()
{
	POMPKA_DDR |= _BV(POMPKA_NR);
	TCCR2A |= _BV(WGM20) | _BV(WGM21) | _BV(COM2A1);
	TCCR2B |= _BV(CS22) | _BV(CS20);
	OCR2A = 0;
	wypelnienie = 0;
}

void PompkaWypelnienie(int w)
{
		OCR2A = w;
		wypelnienie = w;
}

void PompkaWypelnienieMod(uint8_t w)
{
		wypelnienie += w;
		OCR2A = wypelnienie;
}



#endif /* POMPKA_H_ */