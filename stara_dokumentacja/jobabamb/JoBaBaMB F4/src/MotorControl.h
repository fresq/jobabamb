/**
  * @file MotorControl.h
  * @brief   Plik zawiera funkcje obslugi motorow
  */

#pragma once

#include "TimeControl.h"
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>


#define MOTOR_MAX 10000

#define MOTOR1_GPIO GPIOD
#define MOTOR2_GPIO GPIOB
#define MOTOR3_GPIO GPIOD
#define MOTOR4_GPIO GPIOB
#define MOTOR_PWM_GPIO GPIOB

#define MOTOR1_PIN GPIO_Pin_7
#define MOTOR2_PIN GPIO_Pin_5
#define MOTOR3_PIN GPIO_Pin_6
#define MOTOR4_PIN GPIO_Pin_7
#define MOTOR1_PWM_PIN GPIO_Pin_8
#define MOTOR2_PWM_PIN GPIO_Pin_9

#define M1SET GPIO_SetBits(MOTOR1_GPIO, MOTOR1_PIN);
#define M1CLR GPIO_ResetBits(MOTOR1_GPIO, MOTOR1_PIN);
#define M2SET GPIO_SetBits(MOTOR2_GPIO, MOTOR2_PIN);
#define M2CLR GPIO_ResetBits(MOTOR2_GPIO, MOTOR2_PIN);
#define M3SET GPIO_SetBits(MOTOR3_GPIO, MOTOR3_PIN);
#define M3CLR GPIO_ResetBits(MOTOR3_GPIO, MOTOR3_PIN);
#define M4SET GPIO_SetBits(MOTOR4_GPIO, MOTOR4_PIN);
#define M4CLR GPIO_ResetBits(MOTOR4_GPIO, MOTOR4_PIN);


/**
  * @brief   Inicjalizacja silnikow(timery i wyjscia sterujace)
  */
void MOTORS_Config()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = MOTOR1_PIN | MOTOR3_PIN ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;

	GPIO_Init(MOTOR1_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR2_PIN | MOTOR4_PIN;
	GPIO_Init(MOTOR2_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR1_PWM_PIN | MOTOR2_PWM_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(MOTOR_PWM_GPIO, &GPIO_InitStructure);

	GPIO_PinAFConfig(MOTOR_PWM_GPIO, GPIO_PinSource8, GPIO_AF_TIM4);
	GPIO_PinAFConfig(MOTOR_PWM_GPIO, GPIO_PinSource9, GPIO_AF_TIM4);

////////////////////////
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 10000;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 34;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStructure);

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 0;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	outputChannelInit.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outputChannelInit.TIM_OCIdleState = TIM_OCIdleState_Set;
	outputChannelInit.TIM_OCNIdleState = TIM_OCIdleState_Reset;

	TIM_OC4Init(TIM4, &outputChannelInit);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_OC3Init(TIM4, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);

	TIM_Cmd(TIM4, ENABLE);

	TIM_CtrlPWMOutputs(TIM4, ENABLE);
}

int lastLewy = 0, lastPrawy = 0;

/**
  * @brief   Ustawia kierunek i predkosc jazdy
  */
void Jazda(int lewy, int prawy)
{
	if(lewy >= 0)
	{
		if(lewy>MOTOR_MAX)
		lewy = MOTOR_MAX;
		M1SET;
		M2CLR;
	}
	else
	{
		if(lewy<-MOTOR_MAX)
		lewy = -MOTOR_MAX;
		M1CLR;
		M2SET;
	}

	if(prawy >= 0)
	{
		if(prawy>MOTOR_MAX)
		prawy = MOTOR_MAX;
		M3SET;
		M4CLR;
	}
	else
	{
		if(prawy<-MOTOR_MAX)
		prawy = -MOTOR_MAX;
		M3CLR;
		M4SET;
	}

	TIM4->CCR3 = abs(lewy);
	TIM4->CCR4 = abs(prawy);
}

int ileKrokow = 4;

/**
  * @brief   Ustawia kierunek i predkosc jazdy procentowo bez zrywow
  */
void JazdaP(float lewy, float prawy)
{
	//LCDOutString("jazda:  ");
	//LCDOutUFloat(lewy,1,5);
	//LCDOutUFloat(prawy,1,5);
	int deltaLewy =  (MOTOR_MAX * lewy - lastLewy) / ileKrokow;
	int deltaPrawy =  (MOTOR_MAX * prawy - lastPrawy) / ileKrokow;
	for(int i = 0; i < ileKrokow; i++)
	{
		lastLewy += deltaLewy;
		lastPrawy += deltaPrawy;
		Jazda(lastLewy, lastPrawy);
		_delay_ms(20);
	}
	lastLewy = MOTOR_MAX * lewy;
	lastPrawy = MOTOR_MAX * prawy;

}

/**
  * @brief   Petla umozliwiajaca jazde sterowana z pilota
  */
void MenuJazda()
{
	int cmd;
	LCDClear();
	//int ile = 0;
	while(1)
	{
		while((cmd = detect()) < 0);
		if(cmd > 0)
		{
			cmd &= ~2048;
			//LCDOutint16(cmd,5);
			if(cmd == KEY_ESC)
			{
				Jazda(0,0);
				return;
			}
			if(cmd == KEY_LEFT)
			{
				JazdaP(0.9f,-0.1f);
			}
			if(cmd == KEY_RIGHT)
			{
				JazdaP(-0.1f,0.9f);
			}
			if(cmd == KEY_UP)
			{
				JazdaP(0.8f,0.8f);
			}
			if(cmd == KEY_DOWN)
			{
				JazdaP(-0.8f,-0.8f);
			}
			if(cmd == KEY_1)
			{
				JazdaP(-0.8f,0);
			}
			if(cmd == KEY_2)
			{
				JazdaP(0,-0.8f);
			}
			if(cmd == KEY_3)
			{
				JazdaP(0.8f,0);
			}
			if(cmd == KEY_4)
			{
				JazdaP(0,0.8f);
			}
		}
		//_delay_ms(100);
	}
}
