/**
 * @file    main.c
 * @date    07.04.2014
 * @brief   Plik zawiera glowna funkcje programu
 * @mainpage Projekt Platformy mobilnej z ramieniem o 6 stopniach swobody.
 */

#include <stdio.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_flash.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_exti.h>
#include <stm32f4xx_syscfg.h>
#include <stm32f4xx_usart.h>
#include <stm32f4xx_adc.h>
#include <stm32f4xx_dma.h>
#include <misc.h>

#include "hd44780.h"
#include "RC5.h"
#include "menu.h"
#include "Pompka.h"
#include "ServoControl.h"
#include "Blue.h"
#include "SteperControl.h"
#include "TimeControl.h"
#include "MotorControl.h"
#include "Queue.h"
#include "buzzer.h"


#define LED_GPIO GPIOD
#define LED_GREEN GPIO_Pin_12
#define LED_ORANGE GPIO_Pin_13
#define LED_RED GPIO_Pin_14
#define LED_BLUE GPIO_Pin_15

#define BLUE_GPIO GPIOA
#define BLUE_RX_PIN GPIO_Pin_3
#define BLUE_TX_PIN GPIO_Pin_2


/**
  * @brief funkcja inicjalizuje szyny takujace
  */
void RCC_Config()
{
	ErrorStatus HSEStartUpStatus;

	RCC_DeInit();
	RCC_HSEConfig(RCC_HSE_ON);
	HSEStartUpStatus = RCC_WaitForHSEStartUp();

	if(HSEStartUpStatus == SUCCESS)
	{
		FLASH_PrefetchBufferCmd(ENABLE);
		FLASH_SetLatency(FLASH_Latency_7);
		RCC_HCLKConfig(RCC_SYSCLK_Div1);
		RCC_PCLK2Config(RCC_HCLK_Div1);
		RCC_PCLK1Config(RCC_HCLK_Div2);
		RCC_PLLConfig(RCC_PLLSource_HSE, 4, 168, 2, 4);
		RCC_PLLCmd(ENABLE);
		while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
		while(RCC_GetSYSCLKSource() != 0x08);
	}
}

/**
  * @brief funkcja inicjalizuje przerwania
  */
void NVIC_Config()
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;

	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;

	//NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief funkcja inicjalizuje polaczeni bluetooth przez interfejs UART
  */
void BLUE_Config()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = BLUE_RX_PIN | BLUE_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(BLUE_GPIO, &GPIO_InitStructure);

	GPIO_PinAFConfig(BLUE_GPIO, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(BLUE_GPIO, GPIO_PinSource3, GPIO_AF_USART2);

    USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART2, &USART_InitStructure);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART2, ENABLE);
}

#define SERVO1_PIN GPIO_Pin_6
#define SERVO1_GPIO GPIOC
#define SERVO2_PIN GPIO_Pin_7
#define SERVO2_GPIO GPIOC
#define SERVO3_PIN GPIO_Pin_8
#define SERVO3_GPIO GPIOC
#define SERVO4_PIN GPIO_Pin_11
#define SERVO4_GPIO GPIOE
#define SERVO5_PIN GPIO_Pin_14
#define SERVO5_GPIO GPIOB
#define SERVO6_PIN GPIO_Pin_11
#define SERVO6_GPIO GPIOB
#define SERVO7_PIN GPIO_Pin_10
#define SERVO7_GPIO GPIOB

/**
  * @brief funkcja inicjalizuje serwomechanizmy
  */
void SERVO_Config()
{

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOE, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM12, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 10000;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 335;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);
	TIM_TimeBaseInit(TIM12, &TIM_TimeBaseInitStructure);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = SERVO1_PIN | SERVO2_PIN | SERVO3_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = SERVO4_PIN;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = SERVO5_PIN | SERVO6_PIN | SERVO7_PIN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_PinAFConfig(SERVO1_GPIO, GPIO_PinSource6, GPIO_AF_TIM3);
	GPIO_PinAFConfig(SERVO2_GPIO, GPIO_PinSource7, GPIO_AF_TIM3);
	GPIO_PinAFConfig(SERVO3_GPIO, GPIO_PinSource8, GPIO_AF_TIM3);
	GPIO_PinAFConfig(SERVO4_GPIO, GPIO_PinSource11, GPIO_AF_TIM1);
	GPIO_PinAFConfig(SERVO5_GPIO, GPIO_PinSource14, GPIO_AF_TIM12);
	GPIO_PinAFConfig(SERVO6_GPIO, GPIO_PinSource11, GPIO_AF_TIM2);
	GPIO_PinAFConfig(SERVO7_GPIO, GPIO_PinSource10, GPIO_AF_TIM2);

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 1000;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	outputChannelInit.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outputChannelInit.TIM_OCIdleState = TIM_OCIdleState_Set;
	outputChannelInit.TIM_OCNIdleState = TIM_OCIdleState_Reset;

	TIM_OC1Init(TIM3, &outputChannelInit);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_OC2Init(TIM3, &outputChannelInit);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_OC3Init(TIM3, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_OC2Init(TIM1, &outputChannelInit);
	TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);

	TIM_OC1Init(TIM12, &outputChannelInit);
	TIM_OC1PreloadConfig(TIM12, TIM_OCPreload_Enable);

	TIM_OC4Init(TIM2, &outputChannelInit);
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);

	TIM_OC3Init(TIM2, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);

	TIM_Cmd(TIM1, ENABLE);
	TIM_Cmd(TIM2, ENABLE);
	TIM_Cmd(TIM3, ENABLE);
	TIM_Cmd(TIM12, ENABLE);


	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_CtrlPWMOutputs(TIM2, ENABLE);
	TIM_CtrlPWMOutputs(TIM3, ENABLE);
	TIM_CtrlPWMOutputs(TIM12, ENABLE);

}

/**
  * @brief funkcja inicjalizuje zielonego led-a
  */
void GPIO_Config()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = LED_GREEN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;

	GPIO_Init(LED_GPIO, &GPIO_InitStructure);

//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
//	GPIO_Init(GPIOA, &GPIO_InitStructure);

}

/**
  * @brief funkcja inicjalizuje UART do komunikacji z komputerem
  */
void USB_Config()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(BLUE_GPIO, GPIO_PinSource0, GPIO_AF_UART4);
	GPIO_PinAFConfig(BLUE_GPIO, GPIO_PinSource1, GPIO_AF_UART4);

    USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(UART4, &USART_InitStructure);

	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);

	USART_Cmd(UART4, ENABLE);
}

/**
  * @brief przerwanie danych przychodzacych z UART-a z komputera
  */
void UART4_IRQHandler(void)
{
	if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
	{

	}
}

/**
  * @brief inicjalizacja ADC do odczytu napiecia na bateri
  */
void ADC_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;//The channel 10 is connected to PC0
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN; //The PC0 pin is configured in analog mode
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL; //We don't need any pull up or pull down
	GPIO_Init(GPIOC,&GPIO_InitStructure);//Affecting the port with the initialization structure configuration


	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	/* ADC Common Init **********************************************************/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_10Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	 ADC_DeInit();
	 ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;//data converted will be shifted to right
	 ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;//Input voltage is converted into a 12bit number giving a maximum value of 4096
	 ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; //the conversion is continuous, the input data is converted more than once
	 ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;// conversion is synchronous with TIM1 and CC1 (actually I'm not sure about this one :/)
	 ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;//no trigger for conversion
	 ADC_InitStructure.ADC_NbrOfConversion = 1;//I think this one is clear :p
	 ADC_InitStructure.ADC_ScanConvMode = DISABLE;//The scan is configured in one channel
	 ADC_Init(ADC1,&ADC_InitStructure);//Initialize ADC with the previous configuration
	 //Enable ADC conversion
	 ADC_Cmd(ADC1,ENABLE);
	 //Select the channel to be read from
	 ADC_RegularChannelConfig(ADC1,ADC_Channel_10,1,ADC_SampleTime_144Cycles);

	ADC_TempSensorVrefintCmd(ENABLE);

	ADC_Cmd(ADC1, ENABLE);


//PRZERWANIE ADC
//	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);
}

/**
  * @brief Wyswietla w petli napiecie baterii na wyswietlaczu
  */
void Napiecie()
{
	int cmd;
	LCD_Clear();
	while(1)
	{
		cmd = detect();
		if(cmd > 0)
		{
			cmd &= ~2048;
			if(cmd == KEY_ESC) return;
		}

		ADC_SoftwareStartConv(ADC1);
		int adc = ADC_GetConversionValue(ADC1);
		float V = (3.99) * adc * 3.31 / 4095;

		LCD_GoTo(0,0);
		LCD_WriteText("Napiecie :");
		LCDOutFloat(V, 2, 5);
		LCD_GoTo(0,1);
		LCD_WriteText("Raw :");
		LCDOutint16(adc,5);

	}
}

/**
  * @brief wyswietla katy ramienai na wyswietlaczu
  */
void ShowAllAngle(arm* a)
{
	int i, b;
	LCD_GoTo(0,0);
	for(i = 0;  i < COUNT; i++)
	{
		b = (int)(a->tab[i].ang);
		LCD_WriteInt(b);
		LCD_WriteText(" ");
	}
	LCD_WriteText("K(");
	//LCD_WriteInt(mKrok);
	LCD_WriteText(", ");
	//LCD_WriteInt(mMicroKrok);
	LCD_WriteText(")  ");
}

/**
  * @brief funkcja sterowania ramieniem z pilota
  */
void RecznaManipulacja(arm* a)
{
	int cmd, toogle;
	int wybrane = 0;
	int dt = 1;
	LCD_Clear();
	//for (int i = 0; i < 4; i++)
	//{
		//a->tab[i].ang = 0;
	//}
	a->tab[5].ang = 30;
	ShowAllAngle(a);
	_delay_ms(2000);
	Move(*a);
	a->tab[5].ang = 50;
	ShowAllAngle(a);
	LCD_GoTo(0,2);
	LCD_WriteText("Wybrane: ");
	LCD_WriteInt(wybrane+1);
	int zmiana = 0;
	while(1)
	{
		zmiana = 0;
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		zmiana = 0;
		if(cmd == KEY_ESC) return;
		if(cmd == KEY_1) {wybrane = 0; zmiana = 1;}
		if(cmd == KEY_2) {wybrane = 1; zmiana = 1;}
		if(cmd == KEY_3) {wybrane = 2; zmiana = 1;}
		if(cmd == KEY_4) {wybrane = 3; zmiana = 1;}
		if(cmd == KEY_5) {wybrane = 4; zmiana = 1;}
		if(cmd == KEY_6) {wybrane = 5; zmiana = 1;}
		if(cmd == KEY_UP) {a->tab[wybrane].ang -= dt; zmiana = 1;}
		if(cmd == KEY_DOWN) {a->tab[wybrane].ang += dt; zmiana = 1;}
		if(cmd == KEY_LEFT) { Krocz(1,1); }
		if(cmd == KEY_RIGHT) { Krocz(1,0); }
		//if(cmd == KEY_UP) { dt += 1; ShowAngle(dt);}
		//if(cmd == KEY_DOWN) { dt -= 1; ShowAngle(dt);}
		//if(a->tab[0].ang > 60) {a->tab[0].ang = 60;}
		//if(a->tab[0].ang < -60) {a->tab[0].ang = -60;}

		//Set(a);
		if(zmiana)
		{
			Move(*a);
			LCD_GoTo(0,2);
			LCD_WriteText("Wybrane: ");
			LCD_WriteInt(wybrane+1);
			//Show3Angle(a->tab[0].ang, a->tab[1].ang, a->tab[2].ang,a->tab[3].ang);
			ShowAllAngle(a);
		}
	}
}

/**
  * @brief funkcja umozliwia precyzyjne zmiany wypelnienia na serwomechanizmach
  */
void KalibracjaServ()
{
	int cmd, toogle;
	int wybrane = 0;
	int useconds = 1500;
	int dt = 10;
	while(1)
	{
		while((cmd = detect()) < 0);
		toogle = cmd & 2048;
		cmd &= ~2048;
		if(cmd == KEY_ESC) return;
		if(cmd == KEY_1) wybrane = 0;
		if(cmd == KEY_2) wybrane = 1;
		if(cmd == KEY_3) wybrane = 2;
		if(cmd == KEY_4) wybrane = 3;
		if(cmd == KEY_5) wybrane = 4;
		if(cmd == KEY_6) wybrane = 5;
		if(cmd == KEY_UP) {useconds += dt; servoSet(wybrane, useconds);}
		if(cmd == KEY_DOWN) {useconds -= dt; servoSet(wybrane, useconds);}
		if(cmd == KEY_LEFT) { dt -= 1; }
		if(cmd == KEY_RIGHT) {dt += 1;}

		LCD_Clear();
		LCD_WriteText("Wybrane: ");
		LCD_WriteInt(wybrane+1);
		LCD_WriteText(", dt: ");
		LCD_WriteInt(dt);
		LCD_GoTo(0,1);
		LCD_WriteText("Wartosc: ");
		LCD_WriteInt(useconds);


		servoSet(wybrane, useconds);

		_delay_ms(50);
	}

}


/**
  * @brief funkcja umozliwia sterowanie pompka z pilota
  */
void Pompka()
{
	int cmd;
	LCD_Clear();
	while(1)
	{
		while((cmd = detect()) < 0);
		cmd &= ~2048;
		if(cmd == KEY_ESC) return;
		if(cmd == KEY_LEFT) { PompkaWypelnienieMod(-200);}
		if(cmd == KEY_RIGHT) { PompkaWypelnienieMod(200);}
		if(cmd == KEY_8) {PompkaWypelnienie(0);}
		if(cmd == KEY_9) {PompkaWypelnienie(POMPKA_MAX_VALUE);}
		LCD_GoTo(0,0);
		LCDOutFloat((POMPKA_TIM->POMPKA_CCR) / 10000.0f, 1, 5);
		LCD_WriteText("    ");
		_delay_ms(100);
	}
}


//void DMA_FOR_BLUE_Config()
//{
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);   //wlacz taktowanie DMA
//
//
//	DMA_InitTypeDef DMA_InitStructure;
//	  DMA_DeInit(DMA1_Stream6);
//	  DMA_InitStructure.DMA_Channel = DMA_Channel_4;
//	  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
//	  //DMA_InitStructure.DMA_Memory0BaseAddr = PID_data;
//	  //DMA_InitStructure.DMA_BufferSize = 20;
//	  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
//	  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
//	  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART2->DR;
//	  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
//	  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
//	  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
//	  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
//	  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
//	  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
//	  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
//	  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
//
//
//	  DMA_Init(DMA1_Stream6, &DMA_InitStructure);
//
//
//}
//
//void SendDataDMA(uint32_t buffer, uint32_t size)
//{
//	while(DMA_GetFlagStatus(DMA1_Stream6, DMA_FLAG_TCIF6) == RESET);
//	DMA_ClearFlag(DMA1_Stream6, DMA_FLAG_HTIF6  | DMA_FLAG_TCIF6);
//	DMA1_Stream6->CR &= ~(uint32_t)DMA_SxCR_EN;
//	DMA1_Stream6->M0AR = (uint32_t)buffer;
//	DMA1_Stream6->NDTR = size;
//	DMA1_Stream6->CR |= (uint32_t)DMA_SxCR_EN;
//}

/**
  * @brief glowna funkcja programu.
  */
int main()
{
	RCC_Config();
	GPIO_Config();
	SERVO_Config();
	rkInit();
	arm a; //manipulator
	Init(&a);
	Move(a);
	SetSlowAngle(&a, 0, 0, 0, 0, 0, 0, 200);
	POMPKA_Config();
	LCDInit();
	NVIC_Config();
	RC5_Init();
	BLUE_Config();
	BUZZER_Config();
	//BuzzerSet();
	DELAY_Config();
	STEPER_Config();
	ADC_Config();
	//ZerujKrokowy();
	MOTORS_Config();
	//USB_Config();


	Memory m;
	InitMemory(&m);


	while(1)
	{
		int wynik = MainMenu();

		if (wynik == 0)
		{
			//NalejWodki(&m, &a);
		}
//		else if (wynik == 1)
//			Zapamietane(&m, &a);
		else if (wynik == 2)
			KalibracjaServ();
		else if (wynik == 3)
			RecznaManipulacja(&a);
//		else if (wynik == 4)
//			Odwrotna(&a);
//		else if (wynik == 5)
//			Zwierzeta(&a);
		else if(wynik == 6)
			Pompka();
		else if(wynik == 7)
			ZerujKrokowy();
//		else if(wynik == 8)
//			Komunikacja(&a);
		else if(wynik == 9)
			Napiecie();
		else if(wynik == 10)
			MenuJazda();
		else if(wynik == 11)
			BluetoothComunication(&a);

	}
}

