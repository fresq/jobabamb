#pragma once

/**
  * @file queue.h
  * @brief Plik zawiera funkcje implementacji i obslugi kolejki
  */

#define QSIZE 32

typedef struct Queue
{
	int16_t tab[QSIZE];
	int head;
	int tail;
} Queue;


Queue droidQ;

/**
  * @brief Dadanie elementu do kolejki
  */
void qAdd(Queue* q, int16_t data)
{
	if((q->tail + 1) % 8 != q->head)
	{
		q->tab[q->tail] = data;
		q->tail++;
		q->tail = q->tail % QSIZE;
	}
}

/**
  * @brief  sprawdenie elementu z szczytu kolejki bez zdejmowania
  */
int8_t qPeek(Queue* q)
{
	if(q->tail != q->head)
		return q->tab[q->head];
	return -1;
}


/**
  * @brief zdejmuje element z kolejki
  */
int8_t qGet(Queue* q)
{
	if(q->tail != q->head)
	{
		int8_t a = q->tab[q->head];
		q->head++;
		q->head = q->head % QSIZE;
		return a;
	}
	return 0;
}

/**
  * @brief  sprawdza czy kolejka pusta
  */
int qEmpty(Queue* q)
{
	return q->tail == q->head;
}
