/**
  * @file buzzer.h
  * @brief   Plik zawiera funkcje obslugujace buzzer
  */

#pragma once

#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>

/**
  * @brief   Konfiguracja buzzer-a
  */
void BUZZER_Config()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;

	GPIO_Init(GPIOB, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_TIM8);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 10000;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 335;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseInitStructure);

	TIM_OCInitTypeDef outputChannelInit;
	outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
	outputChannelInit.TIM_Pulse = 0;
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	outputChannelInit.TIM_OCIdleState = TIM_OCIdleState_Set;

	outputChannelInit.TIM_OutputNState = TIM_OutputNState_Enable;
	outputChannelInit.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outputChannelInit.TIM_OCNIdleState = TIM_OCIdleState_Reset;

	TIM_OC3Init(TIM8, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);

	TIM_Cmd(TIM8, ENABLE);

	TIM_CtrlPWMOutputs(TIM8, ENABLE);
}

/**
  * @brief   wlaczenie buzzera
  */
void BuzzerSet()
{
	TIM8->CCR3 = 5000;
}

/**
  * @brief   wylaczenie buzzera
  */
void BuzzerReset()
{
	TIM8->CCR3 = 0;
}
